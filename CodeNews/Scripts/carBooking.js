﻿var commonBlock;
var commonData;
var checkBlockKey = false;
$(document).ready(function () {
    checkBlock(selectedDate);
    NewPage();

    $("#btn_send_request").click(function () {
        if ($("#userId").val()) {
            if ($("#mode").val() == "insert") {
                commonData.startPoint = $("#startPoint").val();
                commonData.finishPoint = $("#finishPoint").val();
                commonData.biztrip = $("#biztrip").val();
                commonData.itinerary = $("#itinerary").val();
                commonData.userId = $("#userId").val();
                commonData.start = $("#from").val();
                commonData.end = $("#to").val();
                commonData.usingFamily = $("#usingFamily").attr("checked");
                $("#loadingmessage").show();
                $.ajax({
                    type: 'POST',
                    url: root + '/CarBooking/CreateActual',
                    data: commonData,
                    dataType: 'json',
                    success: function (jsonData) {
                        $("#loadingmessage").hide();
                        if (jsonData) {
                            commonBlock.attr("id", jsonData);
                            NewPage();
                        } else {
                            alert("Your booking is existed. Please try again!");
                            NewPage();
                        }

                        $("#userId").multiselect('refresh');
                        $(".close-reveal-modal").click();
                    }
                });
            } else {
                var data = {};
                data.id = $("#carBookingActualId").val();
                data.start = parseInt($("#from").val()) + 1;
                data.end = $("#to").val();
                data.startPoint = $("#startPoint").val();
                data.finishPoint = $("#finishPoint").val();
                data.biztrip = $("#biztrip").val();
                data.itinerary = $("#itinerary").val();
                data.userId = $("#userId").val();
                data.usingFamily = $("#usingFamily").attr("checked");
                $("#loadingmessage").show();
                $.ajax({
                    type: 'POST',
                    url: root + '/CarBooking/UpdateActual',
                    data: data,
                    dataType: 'json',
                    success: function (jsonData) {
                        $("#loadingmessage").hide();
                        NewPage();
                        $(".close-reveal-modal").click();
                    }
                });
            }
        } else {
            alert("Your must be choose User to continue book.");
        }
    });

    $("#btn_cancel").click(function () {
        $(".close-reveal-modal").click();
        NewPage();
    });

    $("#departmentId").change(function () {
        $.ajax({
            type: 'POST',
            url: root + '/CarBooking/GetUserList',
            data: { departmentId: $(this).val() },
            dataType: 'json',
            success: function (jsonData) {
                $("#userId").empty();
                $(jsonData).each(function () {
                    $("#userId").append("<option value=" + this.userId + " >" + this.fullName + "</option>");
                });

                $("#userId").multiselect('refresh');
            }
        });
    });

    $("#from").change(function () {
        var curFrom = $("#from").val();
        var curTo = $("#to").val();
        if (parseInt(curFrom) >= parseInt(curTo)) {
            curTo = parseInt(curFrom) + 1;
            $("#to").val(curTo);
        }
    });

    $("#to").change(function () {
        var curFrom = $("#from").val();
        var curTo = $("#to").val();
        if (parseInt(curTo) <= parseInt(curFrom)) {
            curFrom = parseInt(curTo) - 1;
            $("#from").val(curFrom);
        }
    });
});

function NewPage() {
    $.ajax({
        type: 'POST',
        url: root + '/CarBooking/GetUserJsonList',
        data: { selectedDate: selectedDate, carId: carId, actualId: actualId },
        dataType: 'json',
        success: function (jsonData) {
            $("#ganttChart").empty();
            $("#ganttChart").ganttView({
                data: jsonData,
                slideWidth: 720,//672,
                checkBlockKey: checkBlockKey,
                behavior: {
                    onClick: function (data) {
                    },
                    onResize: function (data) {
                        if (confirm("Do you want to change this request?")) {
                            $.ajax({
                                type: 'POST',
                                url: root + '/CarBooking/UpdateActual',
                                data: data,
                                dataType: 'json',
                                success: function (jsonData) {
                                }
                            });
                        } else {
                            NewPage();
                        }
                    },
                    onDrag: function (data) {
                        if (confirm("Do you want to change this request?")) {
                            $.ajax({
                                type: 'POST',
                                url: root + '/CarBooking/UpdateActual',
                                data: data,
                                dataType: 'json',
                                success: function (jsonData) {
                                    if (!jsonData) {
                                        alert("Your booking is existed. Please try again!");
                                    }
                                    NewPage();
                                }
                            });
                        } else {
                            NewPage();
                        }
                    },
                    onSelect: function (data, block) {
                        // Get value to fill dropdown time                               
                        $.ajax({
                            type: 'POST',
                            url: root + '/CarBooking/getTime',
                            data: { bookstart: data.start,
                                bookend: data.end,
                                bookingDate: data.bookingDate,
                                planId: data.planId,
                                carId: data.carId
                            },
                            dataType: 'json',
                            success: function (json) {
                                if (json) {
                                    $("#from option").remove();
                                    $("#to option").remove();
                                    var fStart = 0, fEnd = 0, sStart = 0, sEnd = 0;
                                    var m = 0;
                                    $(json).each(function () {
                                        if (m == 0) {
                                            fStart = this.start;
                                            fEnd = this.end;
                                        } else {
                                            sStart = this.start;
                                            sEnd = this.end;
                                        }
                                        m++;
                                    });
                                    var head = fStart < data.start ? fStart : data.start;
                                    var tail = fEnd > data.end ? fEnd : data.end;
                                    for (var i = head; i < tail; i++) {
                                        var optionItem = '<option value="' + i + '">' + (i % 2 == 0 ? i / 2 + ':00' : (i - 1) / 2 + ':30') + '</option>';
                                        $("#from").append(optionItem);
                                    }
                                    $("#from").val(data.start);
                                    for (var j = parseInt(fStart) + 1; j <= sEnd; j++) {
                                        var optionItem = '<option value="' + j + '">' + (j % 2 == 0 ? j / 2 + ':00' : (j - 1) / 2 + ':30') + '</option>';
                                        $("#to").append(optionItem);
                                    }
                                    $("#to").val(data.end);
                                }
                            }
                        });

                        // Load user list
                        $.ajax({
                            type: 'POST',
                            url: root + '/CarBooking/getUser',
                            dataType: 'json',
                            success: function (json) {
                                if (json) {
                                    $("#userId option").remove();
                                    $.each(json, function (i, item) {
                                        var value = '<option value="' + item.userId + '">' + item.fullName + '</option>';
                                        $("#userId").append(value);
                                    });
                                }
                            }
                        });

                        $("#departmentId").multiselect("uncheckAll");
                        $("#userId").multiselect("uncheckAll");

                        commonBlock = block;
                        commonData = data;

                        $("#startPoint").val("");
                        $("#finishPoint").val("");
                        $("#biztrip").val("");
                        $("#itinerary").val("");
                        $("#usingFamily").attr("checked", false);
                        $("#mode").val("insert");
                        $("#myModal").reveal();
                        $("#biztrip").focus();
                    }
                }
            });
            RefreshRightMenu();
        }
    });    
}


function RefreshRightMenu(newBlock) {
    if (!newBlock) {
        newBlock = $('.ganttview-block');
    }
    if (checkBlockKey == false) {
        newBlock.contextPopup({
            title: 'My Popup Menu',
            items: [
			null,
			{ label: 'Delete',
			    icon: root + '/Scripts/granttview/icons/shopping-basket.png',
			    action: function () {
			        if (confirm("Do you want to cancel this request?")) {
			            $.ajax({
			                type: 'POST',
			                url: root + '/CarBooking/DeleteActual',
			                data: { id: obj.attr("id") },
			                dataType: 'json',
			                success: function (jsonData) {
			                    //obj.remove();
			                    NewPage();
			                }
			            });
			        }
			    }
			},
            { label: 'Update',
                icon: root + '/Scripts/granttview/icons/red-edit-icon-glossy-md.png',
                action: function () {
                    $.ajax({
                        type: 'POST',
                        url: root + '/CarBooking/GetActualById',
                        data: { id: obj.attr("id") },
                        dataType: 'json',
                        success: function (jsonData) {
                            var startTime, endTime, planId, bookingDate, carId;
                            startTime = jsonData.startTime;
                            endTime = jsonData.endTime;
                            planId = jsonData.planId;
                            carId = jsonData.carId;
                            bookingDate = $("#bookingDate").text();

                            $.ajax({
                                type: 'POST',
                                url: root + '/CarBooking/getTime',
                                data: { bookstart: startTime,
                                    bookend: endTime,
                                    bookingDate: bookingDate,
                                    planId: planId,
                                    carId: carId,
                                    label: 'update'
                                },
                                dataType: 'json',
                                success: function (json) {
                                    if (json) {
                                        $("#from option").remove();
                                        $("#to option").remove();
                                        var fStart = 0, fEnd = 0, sStart = 0, sEnd = 0;
                                        var m = 0;
                                        $(json).each(function () {
                                            if (m == 0) {
                                                fStart = this.start;
                                                fEnd = this.end;
                                            } else {
                                                sStart = this.start;
                                                sEnd = this.end;
                                            }
                                            m++;
                                        });
                                        for (var i = fStart; i < fEnd; i++) {
                                            var optionItem = '<option value="' + i + '">' + (i % 2 == 0 ? i / 2 + ':00' : (i - 1) / 2 + ':30') + '</option>';
                                            $("#from").append(optionItem);
                                        }
                                        $("#from").val(startTime);
                                        for (var j = parseInt(fStart) + 1; j <= sEnd; j++) {
                                            var optionItem = '<option value="' + j + '">' + (j % 2 == 0 ? j / 2 + ':00' : (j - 1) / 2 + ':30') + '</option>';
                                            $("#to").append(optionItem);
                                        }
                                        $("#to").val(endTime);
                                    }
                                }
                            });

                            //$("#time").text(ConvertTime(jsonData.startTime) + " ~ " + ConvertTime(jsonData.endTime));

                            $("#carBookingActualId").val(jsonData.carBookingActualId);
                            $("#startPoint").val(jsonData.startPoint);
                            $("#finishPoint").val(jsonData.finishPoint);
                            $("#biztrip").val(jsonData.biztrip);
                            $("#itinerary").val(jsonData.itinerary);
                            $("#userId").val(jsonData.user);
                            if (jsonData.usingFamily == null || jsonData.usingFamily == false) {
                                $("#usingFamily").attr("checked", false);
                            }else {
                                $("#usingFamily").attr("checked", true);
                            }
                            $("#mode").val("update");
                            $("#myModal").reveal();
                            $("#biztrip").focus();
                        }
                    });
                }
            }
		]
        });
    } else {
        newBlock.contextPopup({
            title: 'My Popup Menu',
            items: [
			null,			
            { label: 'View',
                icon: root + '/Scripts/granttview/icons/red-edit-icon-glossy-md.png',
                action: function () {
                    $.ajax({
                        type: 'POST',
                        url: root + '/CarBooking/GetActualById',
                        data: { id: obj.attr("id") },
                        dataType: 'json',
                        success: function (jsonData) {
                            var startTime, endTime, planId, bookingDate, carId;
                            startTime = jsonData.startTime;
                            endTime = jsonData.endTime;
                            planId = jsonData.planId;
                            carId = jsonData.carId;
                            bookingDate = $("#bookingDate").text();

                            $.ajax({
                                type: 'POST',
                                url: root + '/CarBooking/getTime',
                                data: { bookstart: startTime,
                                    bookend: endTime,
                                    bookingDate: bookingDate,
                                    planId: planId,
                                    carId: carId,
                                    label: 'update'
                                },
                                dataType: 'json',
                                success: function (json) {
                                    if (json) {
                                        $("#from option").remove();
                                        $("#to option").remove();
                                        var fStart = 0, fEnd = 0, sStart = 0, sEnd = 0;
                                        var m = 0;
                                        $(json).each(function () {
                                            if (m == 0) {
                                                fStart = this.start;
                                                fEnd = this.end;
                                            } else {
                                                sStart = this.start;
                                                sEnd = this.end;
                                            }
                                            m++;
                                        });
                                        for (var i = fStart; i < fEnd; i++) {
                                            var optionItem = '<option value="' + i + '">' + (i % 2 == 0 ? i / 2 + ':00' : (i - 1) / 2 + ':30') + '</option>';
                                            $("#from").append(optionItem);
                                        }
                                        $("#from").val(startTime);
                                        for (var j = parseInt(fStart) + 1; j <= sEnd; j++) {
                                            var optionItem = '<option value="' + j + '">' + (j % 2 == 0 ? j / 2 + ':00' : (j - 1) / 2 + ':30') + '</option>';
                                            $("#to").append(optionItem);
                                        }
                                        $("#to").val(endTime);
                                    }
                                }
                            });

                            //$("#time").text(ConvertTime(jsonData.startTime) + " ~ " + ConvertTime(jsonData.endTime));

                            $("#carBookingActualId").val(jsonData.carBookingActualId);
                            $("#startPoint").val(jsonData.startPoint);
                            $("#finishPoint").val(jsonData.finishPoint);
                            $("#biztrip").val(jsonData.biztrip);
                            $("#itinerary").val(jsonData.itinerary);
                            $("#userId").val(jsonData.user);
                            if (jsonData.usingFamily == null || jsonData.usingFamily == false) {
                                $("#usingFamily").attr("checked", false);
                            } else {
                                $("#usingFamily").attr("checked", true);
                            }
                            $("#mode").val("update");
                            $("#myModal").reveal();
                            $("#biztrip").focus();
                        }
                    });

                }
            }
		]
        });
        $("#myModal input[type=text]").attr("readonly", "readonly");
        $("#myModal input[type=checkbox]").attr("disabled", "disabled");
        $("#myModal select").attr("disabled", "disabled");        
        $("#myModal textarea").attr("readonly", "readonly");
        $("#btn_send_request").hide();
        $("#btn_cancel").attr('value', 'OK');

    }
}

function checkBlock(date) {
    $.ajax({
        type: 'POST',
        url: root + '/CarBooking/GetMonthBlock',
        data: { currentDate: date },
        dataType: 'json',
        success: function (jsonData) {
            if (jsonData == true) {                                
                checkBlockKey = true;
            } else {                
                checkBlockKey = false;
            }
        }
    });
}

function ApplyBooking() {    
    $.ajax({
        type: 'POST',
        url: root + '/CarBooking/ApplyBooking',
        dataType: 'json',
        success: function (jsonData) {
            NewPage();
        }
    });
}

function ConvertTime(time) {
    var result = parseInt(time / 2) + "h";
    if ((time / 2) == parseInt(time / 2)) {
        result += "00";
    } else {
        result += "30";
    }
    return result;
}