﻿var currentDay = "";
var copyFlg = true;
var checkBlockKey = false;
var commonData = new Object();
var myMode = "week";
var carPlate = "";
var driveName = "";
$(document).ready(function () {
    NewPage("");

    
    if ($("#currentMode").val().length > 0) {
        myMode = $("#currentMode").val();
    }
    
    
    $('#getday').Zebra_DatePicker({
        format: "Y/m/d"
    });

    $("#btn_cancel").click(function () {
        $(".close-reveal-modal").click();
    });
    $("#resource_cancel").click(function () {
        $(".close-reveal-modal").click();
    });
    $("#btn_ok").click(function () {
        var group = $("#hd_group").val();
        var member = $("#hd_member").val();
        var dataRow = $(".ganttview-vtheader-series-name[number=" + group + "/" + member + "]");
        dataRow.attr("driverid", $("#driverDdl").val());
        $.ajax({
            type: 'POST',
            url: root + '/BookingPlan/UpdatePlanDriver',
            data: { bookingDate: dataRow.attr("date"), carId: dataRow.attr("carid"), driverId: dataRow.attr("driverid") },
            dataType: 'json',
            success: function (jsonData) {
                NewPage("");
            }
        });
        $(".close-reveal-modal").click();
    });
    
    $("#driverDdl").live("change", function () {
        $.ajax({
            type: 'POST',
            url: root + '/BookingPlan/GetDriverById',
            data: { id: $(this).val() },
            dataType: 'json',
            success: function (jsonData) {
                $("#driverCompany").html(jsonData.companyname);
                $("#driverBirthday").html(jsonData.birthday);
                $("#driverGender").html(jsonData.gender);
                $("#driverExperience").html(jsonData.experience);
                if (jsonData.image != null) {
                    $("#driverImage").attr("src", "/Images/Items/" + jsonData.image);
                } else {
                    $("#driverImage").attr("src", "/Content/Images/NoPhotoIcon.jpg");
                }
            }
        });
    });

    // Fill data to resource
    $("#resourceUsage_button").click(function () {
        $("#loadingmessage").show();
        $.ajax({
            type: 'POST',
            url: root + '/BookingPlan/GetResourceList',
            data: { bookingDate: $("#currentDate").val() },
            dataType: 'html',
            success: function (jsonData) {
                $('#resource').html(jsonData);
                $("#loadingmessage").hide();
                $("#resource_list").reveal();
            }
        });
    });
    //ganttview-block none-edit ui-resizable ui-draggable
    $(".none-edit").attr("start", "20");

    // Block mouse hover
    $(".ganttview-block").mouseover(function () {
        alert($(this).attr("id"));
    });

    $("#btn_cancel_create").click(function () {
        $("#myCreateModal .close-reveal-modal").click();
    });

    $("#btn_send_request").click(function () {

        if ($("#userId").val()) {
            if ($("#mode_create").val() == "insert") {

                commonData.bookingDate = bookingDate;
                commonData.carId = carId;
                commonData.driverId = driverId;
                commonData.planId = planId;
                commonData.booker = booker;


                commonData.startPoint = $("#startPoint").val();
                commonData.finishPoint = $("#finishPoint").val();
                commonData.biztrip = $("#biztrip").val();
                commonData.itinerary = $("#itinerary").val();
                commonData.userId = $("#userId").val();
                commonData.start = $("#from").val();
                commonData.end = $("#to").val();
                commonData.usingFamily = $("#usingFamily").attr("checked");

                $("#loadingmessage").show();
                $.ajax({
                    type: 'POST',
                    url: root + '/CarBooking/CreateActual',
                    data: commonData,
                    dataType: 'json',
                    success: function (jsonData) {
                        $("#loadingmessage").hide();
                        if (jsonData) {
                            alert("Booking Successful!");
                            NewPage("");
                            $(".close-reveal-modal").click();
                        } else {
                            alert("Your booking time has conflicted with booking other.\nPlease try again!");
                            return false;
                        }
                    }
                });
            }
        } else {
            alert("Your must be choose User to continue book");
        }
    });
});          // End document ready

function checkBlock(date) {    
    $.ajax({
        type: 'POST',
        url: root + '/CarBookingUsing/GetMonthBlock',
        data: { currentDate: date },
        dataType: 'json',
        success: function (jsonData) {
            if (jsonData == true) {
                $("#mode").parent().addClass("disabled");
                checkBlockKey = true;
            } else {
                $("#mode").parent().removeClass("disabled");
                checkBlockKey = false;
            }
        }
    });    
}

function RefreshDriverList() {
    $(".ganttview-vtheader-series").each(function () {
        var allDdl = $(this).find(".driverDdl select");
        for (var i = 0; i < allDdl.length; i++) {
            allDdl.eq(i).find("option").each(function () {
                for (var j = 0; j < allDdl.length; j++) {
                    if (j != i && allDdl.eq(j).val() == $(this).val()) {
                        $(this).remove();
                    }
                }
            });
        }
    });
}

function NewPage(option) {
    if (option == "frompicker") {
        $("#currentDate").val($("#getday").val().length < 10 ? $("#getday").val() + "/01" : $("#getday").val());
    }
    $("#loadingmessage").show();
    myMode = $("#currentMode").val();
    $.ajax({
        type: 'POST',
        url: root + '/BookingPlan/GetJsonList',
        data: { mode: $("#currentMode").val(), option: option, currentDate: $("#currentDate").val() },
        dataType: 'json',
        success: function (jsonData) {
            $("#ganttChart").empty();
            $("#ganttChart").ganttView({
                data: jsonData,
                slideWidth: 720,
                behavior: {
                    onClick: function (data) {
                    },
                    onResize: function (data) {
                        $.ajax({
                            type: 'POST',
                            url: root + '/BookingPlan/CheckWrongActual',
                            data: data,
                            dataType: 'json',
                            success: function (jsonData) {
                                var cfr = true; ;
                                if (jsonData) {
                                    cfr = confirm("If you apply these changes with this plan you will lose some actual informations. Are you sure?");
                                }
                                if (cfr) {
                                    $.ajax({
                                        type: 'POST',
                                        url: root + '/BookingPlan/UpdatePlan',
                                        data: data,
                                        dataType: 'json',
                                        success: function (jsonData) {
                                        }
                                    });
                                } else {
                                    NewPage(option);
                                }
                            }
                        });
                    },
                    onDrag: function (data) {
                        $.ajax({
                            type: 'POST',
                            url: root + '/BookingPlan/CheckWrongActual',
                            data: data,
                            dataType: 'json',
                            success: function (jsonData) {
                                var cfr = true; ;
                                if (jsonData) {
                                    cfr = confirm("If you apply these changes with this plan you will lose some actual informations. Are you sure?");
                                }
                                if (cfr) {
                                    $.ajax({
                                        type: 'POST',
                                        url: root + '/BookingPlan/UpdatePlan',
                                        data: data,
                                        dataType: 'json',
                                        success: function (jsonData) {
                                        }
                                    });
                                } else {
                                    NewPage(option);
                                }
                            }
                        });
                    },
                    onSelect: function (data, block) {
                        $.ajax({
                            type: 'POST',
                            url: root + '/BookingPlan/CreatePlan',
                            data: data,
                            dataType: 'json',
                            success: function (jsonData) {
                                block.attr("id", jsonData);
                                RefreshRightMenu(block);
                            }
                        });
                    }
                }

            });
            RefreshRightMenu();
            $("#currentDate").val(jsonData[0].name);
            currentDay = jsonData[0].name;
            checkBlock(currentDay);
            $("#headTitle").text(jsonData[0].name + " ~ " + jsonData[jsonData.length - 1].name);
            $("#loadingmessage").hide();

            // set mode
            $(".ganttview-vtheader-carplate-name a").each(function () {
                var link = $(this).attr("href");
                link += "&mode=" + myMode;
                $(this).attr("href", link);

                $(this).click(function() {
                    link = $(this).attr("href");
                    link += "&scroll=" + $(window).scrollTop();
                    window.location = link;
                    return false;
                });
            });

            if (myMode == "week") {
                $(".weekMode").addClass("disabled");
            } else if (myMode == "day") {
                $(".dayMode").addClass("disabled");
            } else {
                $(".monthMode").addClass("disabled");
            }

            var querystring = location.search;
            var parram = querystring.split("&");
            if (parram.length > 0) {
                var parramCount = parram.length;
                var lastParram = parram[parramCount - 1];
                var scroll = lastParram.split("=");
                if (scroll[0] == "scroll") {
                    window.scrollTo(0, scroll[1]);
                }
            }
        }
    });
}

// Delete function: processing when user delete actual booking plan
function DeleteAction() {
    var crrDate = $("#currentDate").val();
    var crrMode = $("#mode").attr("class");
    var action = confirm("Are you sure to delete data?");
    if (action) {
        if (crrDate != '' && crrMode != '') {
            $("#loadingmessage").show();
            $.ajax({
                type: 'POST',
                url: root + '/BookingPlan/DeletePlanRemove',
                data: {
                    mode: crrMode,
                    currentDate: crrDate
                },
                dataType: 'json',
                success: function (jsonData) {
                    NewPage("");
                }
            });
        }
    }
}

function ChangeMode(mode) {    
    $(".mode").removeClass("disabled");
    $(".mode").css("pointer-events", "visible");
    $("." + mode + "Mode").addClass("disabled");
    $("." + mode + "Mode").css("pointer-events", "none");
    if (mode == "month") {
        $('#getday').Zebra_DatePicker({
            format: "Y/m"
        });
    } else if (mode == "week") {
        $("#getday").Zebra_DatePicker({
            format: "Y/m/d",
            show_week_number: 'Wk'
        });
    } else {
        $('#getday').Zebra_DatePicker({
            format: "Y/m/d"
        });
    }

    $("#currentMode").val(mode);
    $("#currentDate").val(currentDay);
    $("#mode").removeClass();
    $("#mode").addClass(mode);
    myMode = mode;
    
    NewPage("");
}

function RefreshRightMenu(newBlock) {
    
    if (!newBlock) {
        newBlock = $('.ganttview-block:not(.none-edit)');        
    }
    newBlock.contextPopup({
        title: 'My Popup Menu',
        items: [
		null,
            {
                label: 'Delete',
                icon: root + '/Scripts/granttview/icons/shopping-basket.png',
                action: function () {
                    $.ajax({
                        type: 'POST',
                        url: root + '/BookingPlan/DeletePlan',
                        data: { id: obj.attr("id") },
                        dataType: 'json',
                        success: function (jsonData) {
                            obj.remove();
                        }
                    });
                }
            },
            {
                label: 'Booking',
                icon: root + '/Scripts/granttview/icons/application-table.png',
                action: function () {
                    $("#loadingmessage").show();
                    $.ajax({
                        type: 'POST',
                        url: root + '/BookingPlan/GetCreateInfo',
                        data: { planId: obj.attr("id") },
                        dataType: 'html',//'json',
                        success: function (jsonData) {
                            $("#create_source").html(jsonData);
                            $("#loadingmessage").hide();
                            $("#myCreateModal").reveal();
                        }
                    });
                }
            }
	    ]
    });
    
}


     function ChangeDriver(id, group, member) {
    $.ajax({
        type: 'POST',
        url: root + '/BookingPlan/GetDriverList',
        dataType: 'json',
        success: function (jsonData) {
            $("#driverDdl").empty();
            var others = $(".ganttview-vtheader-series-name[group=" + group + "]");
            var flg = true;
            var currentDriver = $(".ganttview-vtheader-series-name[number=" + group + "/" + member + "]").attr("driverid");
            var currentIndex = 0;
            for (var i = 0; i < jsonData.length; i++) {
                for (var j = 0; j < others.length; j++) {
                    if (jsonData[i].driverid == others.eq(j).attr("driverid") && others.eq(j).attr("number") != group + "/" + member) {
                        flg = false;
                        break;
                    }
                }
                if (flg) {
                    $("#driverDdl").append("<option value=" + jsonData[i].driverid + ">" + jsonData[i].name + "</option>");
                }
                flg = true;
                if (jsonData[i].driverid == currentDriver) {
                    currentIndex = i;
                }
            }
            $("#driverDdl").val(currentDriver);
            $("#hd_group").val(group);
            $("#hd_member").val(member);

            $("#driverCompany").html(jsonData[currentIndex].companyname);
            $("#driverBirthday").html(jsonData[currentIndex].birthday);
            $("#driverGender").html(jsonData[currentIndex].gender);
            $("#driverExperience").html(jsonData[currentIndex].experience);
            if (jsonData[currentIndex].image != null && jsonData[currentIndex].image != "") {
                $("#driverImage").attr("src", root + "/Images/Items/" + jsonData[currentIndex].image);
            } else {
                $("#driverImage").attr("src", root + "/Content/Images/NoPhotoIcon.jpg");
            }

            $("#myModal").reveal();
        }
    });
}

function Block() {
    $.ajax({
        type: 'POST',
        url: root + '/BookingPlan/Block',
        dataType: 'json',
        success: function (jsonData) {
            if (jsonData == "0") {
                alert("Booking system is unblocked successful!");
                $("#link_block").text("Block");
            } else {                
                alert("Booking system is blocked successful!");
                $("#link_block").text("Unblock");
            }
        }
    });
}

function Copy() {
    if (copyFlg) {
        $("#hd_startDateCopy").val($("#currentDate").val());    
        $("#link_copy").text("Past");
    } else {
        if ($("#hd_startDateCopy").val() != $("#currentDate").val()) {
            $.ajax({
                type: 'POST',
                url: root + '/BookingPlan/CopyPlanData',
                data: { oldStart: $("#hd_startDateCopy").val(), newStart: $("#currentDate").val() },
                dataType: 'json',
                success: function (jsonData) {
                    NewPage("");
                    $("#link_copy").text("Copy");
                }
            });
        } else {
            $("#link_copy").text("Copy");
        }
    }
    copyFlg = !copyFlg;
}