﻿$(document).ready(function () {

    // Set read only for userForm
    $('#userForm').readOnlyForm(true);
    ChangeButtonStatus(false);


    // Set target row when mouse hover
    $("#tableId tbody tr").live("mouseenter", function () {
        $(this).css("background-color", "#AED5E1");
    });

    // Remove target row when mouse leave
    $("#tableId tbody tr").live("mouseleave", function () {
        $(this).removeAttr('style');
    });


    // Ajax to load list user
    $.ajax({
        type: 'POST',
        url: root + '/Users/UserList',
        dataType: 'html',
        success: function (data) {
            $('#listContent').html(data);
            $("#tableId tbody tr:odd").attr("class", 'odd');
        }
    });

    // Function processing when click on delete button
    $("#btnDelete").click(function () {
        if ($("#userId").val() == "") {
            alert("Please choose an item!");
        }
        else {
            var action = confirm("Are you sure to delete data?");
            if (action) {
                $.ajax(
                {
                    url: root + '/Users/Delete',
                    type: "POST",
                    dataType: "html",
                    data: (getUser()),
                    success: function (data) {
                        $('#listContent').html(data);
                        $("#tableId tbody tr:odd").attr("class", 'odd');                        
                        if (message != "") {
                            $('#userId').val('');
                            alert(message);
                            DeleteButton('userForm');
                            CancelButtonUser();
                        }
                    }
                });
            }
        }
    });

    // Function processing when click on save button
    $("#btnSave").click(function () {
        if ($("#userForm").validationEngine('validate') && isPass) {
            $("#loadingmessage").show();
            $.ajax(
            {
                url: root + '/Users/Save',
                type: "POST",
                dataType: "html",
                data: (getUser()),
                success: function (data) {
                    $('#listContent').html(data);
                    $("#tableId tbody tr:odd").attr("class", 'odd');
                    SaveButton('userForm');
                    $('select#departmentId').multiselect('disable');
                    if (message != "") {
                        $('#userId').val('');
                        isOn = false;
                        $("#loadingmessage").hide();
                        alert(message);
                    }
                }
            });
        } else {
            if (!isPass) {
                var caption = '<div class="userNameformError caption parentFormuserForm formError" style="opacity: 0.87; position: absolute; top: 136px; left: 408px; margin-top: -48px;"><div class="formErrorContent">Username has exists<br></div><div class="formErrorArrow"><div class="line10"><!-- --></div><div class="line9"><!-- --></div><div class="line8"><!-- --></div><div class="line7"><!-- --></div><div class="line6"><!-- --></div><div class="line5"><!-- --></div><div class="line4"><!-- --></div><div class="line3"><!-- --></div><div class="line2"><!-- --></div><div class="line1"><!-- --></div></div></div>';
                $("#userForm").append(caption);
            }
            if (!isEmail) {
                var caption = '<div class="userNameformError emailcaption parentFormuserForm formError" style="opacity: 0.87; position: absolute; top: 168.8px; left: 408px; margin-top: -34px;"><div class="formErrorContent">Username has exists<br></div><div class="formErrorArrow"><div class="line10"><!-- --></div><div class="line9"><!-- --></div><div class="line8"><!-- --></div><div class="line7"><!-- --></div><div class="line6"><!-- --></div><div class="line5"><!-- --></div><div class="line4"><!-- --></div><div class="line3"><!-- --></div><div class="line2"><!-- --></div><div class="line1"><!-- --></div></div></div>';
                $("#userForm").append(caption);
            }
        }
    });
});

function newPage(page, sortField, sortDirection) {
    $.ajax({
        type: 'GET',
        url: root + '/Users/UserList?page=' + page + '&sortField=' + sortField + '&sortDirection=' + sortDirection,
        dataType: 'html',
        success: function (data) {
            $('#listContent').html(data);
            $("#tableId tbody tr:odd").attr("class", 'odd');
        }
    });
}

// Function sort page
function sort(page, sortField, sortDirection, newSortField) {
    if (sortField == newSortField) {
        if (sortDirection == "asc") {
            sortDirection = "desc";
        } else if (sortDirection == "desc") {
            sortField = "userId";
        } else {
            sortDirection = "asc";
        }
        
    } else {
        sortField = newSortField;
        sortDirection = "asc";
    }

    $.ajax({
        type: 'GET',
        url: root + '/Users/UserList?page=' + page + '&sortField=' + sortField + '&sortDirection=' + sortDirection,
        dataType: 'html',
        success: function (data) {
            $('#listContent').html(data);
            $("#tableId tbody tr:odd").attr("class", 'odd');
        }
    });
}

// Function get user and set infoation to form
function SelectUser(obj, userId) {
    function printObject(o) {
        var out = '';
        for (var p in o) {
            out += p + ': ' + o[p] + '\n';
        }
        alert(out);
    }
    if ($("#mode").val() == '') {
        $.ajax({
            type: 'POST',
            url: root + '/Users/GetUserById',
            data: { id: userId },
            dataType: 'json',
            success: function (json) {

                // When get new item we need clear all caption
                $('.formError').remove();
                isOn = false;
                action = "none";

                uName = json.userName;
                uEmail = json.email;

                $('.active').attr("class", '');
                $("#tableId tbody tr:odd").attr("class", 'odd');

                $("#userId").val(json.userId);
                $("#userName").val(json.userName);
                $("#fullName").val(ReplaceNull(json.fullName));
                $("#email").val(ReplaceNull(json.email));
                $("#birthday").val(ReplaceNull(json.birthday));
                $("#handPhone").val(ReplaceNull(json.handPhone));
                $("#homePhone").val(ReplaceNull(json.homePhone));
                $("#address").val(ReplaceNull(json.address));
                $("#note").val(ReplaceNull(json.note));
                //$("#departmentId").val(json.departmentId.split(","));

                $("#departmentId").multiselect('uncheckAll');
                var selectedOptions = json.departmentId.split(",");
                for (var i in selectedOptions) {
                    var optionVal = selectedOptions[i];
                    $("#departmentId").find("option[value=" + optionVal + "]").prop("selected", "selected");
                }
                $("#departmentId").multiselect('refresh');

                $("#userOldImage").val(ReplaceNull(json.image));
                $("#permission").val(ReplaceNull(json.permission));
                $("#password").val(ReplaceNull(json.password));
                if (json.image != null && json.image != "") {
                    $("#show_image").html("<img src='" + root + "/Images/Items/" + json.image + "' width='120' height='100' />");
                } else {
                    $("#show_image").html("<img src='" + root + "/Content/Images/NoPhotoIcon.jpg' width='120' height='100' />");
                }
                $(obj).attr("class", 'active');
                $("body,html").animate({ scrollTop: 0 }, 400);
            }
        });
    }
}

// Function processing when click add new in view
function AddButtonDriver() {
    
    AddButton('userForm');
    $('#userId').attr('readonly', true);
    $('#userId').val('');
    isOn = true;
    action = "add";
    if ($("#mode").val() == 'add') {
        $("select#departmentId").multiselect('enable');
        $("#departmentId").multiselect('uncheckAll');
        $("#departmentId").multiselect('refresh');
    }
}

// Function processing when click edit in view
function EditButtonDriver() {
    if ($("#userId").val() != "") {
        EditButton('userForm');
        $('#userId').attr('readonly', true);        
        isOn = true;
        action = "edit";
        if ($("#mode").val() == 'edit') {
            $("select#departmentId").multiselect('enable');            
        }
    }
    else {
        alert("Please choose an item!");
    }
}

// Function processing when click cancel in view
function CancelButtonUser() {
    $('select#departmentId').multiselect('disable'); 
    $("#departmentId").multiselect('uncheckAll');
    $("#departmentId").multiselect('refresh');
}


// Function get user in form
function getUser() {

    var userid = $("#userId").val();
    var username = $.trim($("#userName").val());
    var fullname = $.trim($("#fullName").val());
    var email = $.trim($("#email").val());
    var birthday = $.trim($("#birthday").val());
    var handphone = $.trim($("#handPhone").val());
    var homephone = $.trim($("#homePhone").val());
    var address = $.trim($("#address").val());
    var note = $.trim($("#note").val());
    var departmentid = $.trim($("#departmentId").val());
    var image = $("#userOldImage").val();
    var permission = $.trim($("#permission").val());
    var password = $("#password").val();

    var users = { userId: userid, userName: username, fullName: fullname, email: email, birthday: birthday, handPhone: handphone, homePhone: homephone, address: address, note: note, image: image, departmentId: departmentid, permission: permission, password: password };
    return users;
}