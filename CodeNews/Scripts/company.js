﻿$(document).ready(function () {

    // Set read only for companyForm
    $('#companyForm').readOnlyForm(true);
    ChangeButtonStatus(false);

    // Set background for row target when mouse hover
    $("#tableId tbody tr").live("mouseenter", function () {
        $(this).css("background-color", "#AED5E1");
    });

    // Remove background of row target when mouse leave
    $("#tableId tbody tr").live("mouseleave", function () {
        $(this).removeAttr('style');
    });

    // Ajax get list company
    $.ajax({
        type: 'POST',
        url: root + '/Company/CompanyList',
        dataType: 'html',
        success: function (data) {
            $('#listContent').html(data);
            $("#tableId tbody tr:odd").attr("class", 'odd');
        }
    });

    // Function processing when click on button delete
    $("#btnDelete").click(function () {
        if ($("#companyId").val() == "") {
            alert("Please choose an item!");
        }
        else if ($("#companyId").val() == "1") {
            alert("Master data cannot be deleted!");
        }
        else {
            var action = confirm("Are you sure to delete data?");
            if (action) {
                $.ajax(
                {
                    url: root + '/Company/Delete',
                    type: "POST",
                    dataType: "html",
                    data: (getCompany()),
                    success: function (data) {
                        $('#listContent').html(data);
                        $("#tableId tbody tr:odd").attr("class", 'odd');
                        if (message != "") {                            
                            alert(message);
                            DeleteButton('companyForm');                   
                        }
                    }
                });
            }
        }
    });

    // Function processing when click on button save
    $("#btnSave").click(function () {
        if ($("#companyForm").validationEngine('validate')) {
            $.ajax(
            {
                url: root + '/Company/Save',
                type: "POST",
                dataType: "html",
                data: (getCompany()),
                success: function (data) {
                    $('#listContent').html(data);
                    $("#tableId tbody tr:odd").attr("class", 'odd');
                    SaveButton('companyForm');
                    if (message != "") {
                        alert(message);
                    }
                }
            });
        }
    });
});

//
function newPage(page, sortField, sortDirection) {
    $.ajax({
        type: 'GET',
        url: root + '/Company/CompanyList?page=' + page + '&sortField=' + sortField + '&sortDirection=' + sortDirection,
        dataType: 'html',
        success: function (data) {
            $('#listContent').html(data);
            $("#tableId tbody tr:odd").attr("class", 'odd');
        }
    });
}

// Function sort list
function sort(page, sortField, sortDirection, newSortField) {
    if (sortField == newSortField) {
        if (sortDirection == "asc") {
            sortDirection = "desc";
        } else if (sortDirection == "desc") {
            sortField = "companyId";
        } else {
            sortDirection = "asc";
        }
        
    } else {
        sortField = newSortField;
        sortDirection = "asc";
    }

    $.ajax({
        type: 'GET',
        url: root + '/Company/CompanyList?page=' + page + '&sortField=' + sortField + '&sortDirection=' + sortDirection,
        dataType: 'html',
        success: function (data) {
            $('#listContent').html(data);
            $("#tableId tbody tr:odd").attr("class", 'odd');
        }
    });
}

// Function select company
function SelectCompany(obj, companyId) {
    if ($("#mode").val() == '') {
        $.ajax({
            type: 'POST',
            url: root + '/Company/GetCompanyById',
            data: { id: companyId },
            dataType: 'json',
            success: function (json) {
                $('.active').attr("class", '');
                $("#tableId tbody tr:odd").attr("class", 'odd');
                $("#companyId").val(json.companyId);
                $("#companyName").val(json.companyName);
                $("#email").val(json.email);
                $("#phone").val(json.phone);
                $("#fax").val(json.fax);
                $("#address").val(json.address);
                $("#note").val(json.note);
                $("#website").val(json.website);
                $("#companyOldImage").val(json.image);
                if (json.image != null && json.image != "") {
                    $("#show_image").html("<img src='" + root + "/Images/Items/" + json.image + "' width='120' height='100' />");
                } else {
                    $("#show_image").html("<img src='" + root + "/Content/Images/NoPhotoIcon.jpg' width='120' height='100' />");
                }
                $("#otherEmail").val(json.otherEmail);
                $("#otherPhone").val(json.otherPhone);
                $("#nameContact").val(json.nameContact);
                $("#phoneContact").val(json.phoneContact);
                $("#emailContact").val(json.emailContact);
                $(obj).attr("class", 'active');
                $("body,html").animate({ scrollTop: 0 }, 400);
            }
        });
    }
}

// Function processing when click on button add
function AddButtonCompany() {
    AddButton('companyForm');
    $('#carId').attr('readonly', true);
}

// Function processing when click on button edit
function EditButtonCompany() {
    if ($("#companyId").val() != "") {
        EditButton('companyForm');
        $('#companyId').attr('readonly', true);
    }
    else {
        alert("Please choose an item!");
    }
}

// Function get company
function getCompany() {
    var companyId = $("#companyId").val();
    var companyName = $.trim($("#companyName").val());
    var email = $.trim($("#email").val());
    var phone = $("#phone").val();
    var fax = $("#fax").val();
    var address = $.trim($("#address").val());
    var note = $.trim($("#note").val());
    var website = $.trim($("#website").val());
    var otherEmail = $.trim($("#otherEmail").val());
    var otherPhone = $("#otherPhone").val();
    var nameContact = $.trim($("#nameContact").val());
    var phoneContact = $("#phoneContact").val();
    var emailContact = $.trim($("#emailContact").val());
    var image = $("#companyOldImage").val();

    var companies = { companyId: companyId, companyName: companyName, email: email, phone: phone, fax: fax, address: address, note: note, website: website, image: image, otherEmail: otherEmail, otherPhone: otherPhone, nameContact: nameContact, phoneContact: phoneContact, emailContact: emailContact };
    return companies;
}