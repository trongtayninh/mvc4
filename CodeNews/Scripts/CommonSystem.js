﻿
// Set root of site

var permission = "";
var block = "0";
var blockKey = false;
// Function clear
$.fn.clearForm = function () {
    $("a.upload-image").show();
    return this.each(function () {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
            return $(':input', this).clearForm();
        if (tag == 'select' || type == 'checkbox')
            $('#' + this.id).attr("disabled", false);
        if (type == 'text' || type == 'password' || tag == 'textarea') {
            $('#' + this.id).attr('readonly', false);
            this.value = '';
        }
        else if (type == 'checkbox' || type == 'radio')
            this.checked = false;
        else if (tag == 'select') {
            this.selectedIndex = 0;
        }
    });
};

// Function set read only for form
$.fn.readOnlyForm = function (flag) {
    if (flag) {
        $("a.upload-image").hide();
        return this.each(function () {
            var type = this.type, tag = this.tagName.toLowerCase();
            if (tag == 'form')
                return $(':input', this).readOnlyForm(flag);
            if (tag == 'select' || type == 'checkbox')
                $('#' + this.id).attr("disabled", true);
            //            if (tag == 'select')
            //                $('#' + this.id).val('');
            if (type == 'text' || type == 'password' || tag == 'textarea') {
                $('#' + this.id).attr('readonly', 'readonly');
            }
            //        else if (type == 'checkbox' || type == 'radio')
            //            this.checked = false;
            if (tag == 'select') {
                //this.selectedIndex = 0;
            }
        });
    }
    else {
        $("a.upload-image").show();
        return this.each(function () {
            var type = this.type, tag = this.tagName.toLowerCase();
            if (tag == 'form')
                return $(':input', this).readOnlyForm(flag);
            if (tag == 'select' || type == 'checkbox')
                $('#' + this.id).attr("disabled", false);
            if (type == 'text' || type == 'password' || tag == 'textarea') {
                $('#' + this.id).attr('readonly', false);
            }
            //        else if (type == 'checkbox' || type == 'radio')
            //            this.checked = false;
            //        else if (tag == 'select')
            //            this.selectedIndex = -1;
        });
    }
};

// Function processing when click on Add button
function AddButton(id) {
    var action = confirm("Are you sure add new data?");
    if (action) {
        $('#' + id).clearForm();
        $("#mode").val('add');
        //EnableStatusButton('add');
        //reset image
        $("#show_image>img").attr("src", "");
        ChangeButtonStatus(true);
        $("#" + id).validationEngine();
    }
}

// Function processing when click on Edit button
function EditButton(id) {
    var action = confirm("Are you sure edit info data?");
    if (action) {
        $('#' + id).readOnlyForm(false);
        $("#mode").val('edit');
        //EnableStatusButton('edit');        
        ChangeButtonStatus(true);
        $("#" + id).validationEngine();
    }
}

// Function processing when click on Save button
function SaveButton(id) {
    $('#' + id).readOnlyForm(true);
    $("#mode").val('');
    //EnableStatusButton('save');
    ChangeButtonStatus(false);
    $("#" + id).validationEngine('detach');
}

// Function processing when click on Delete button
function DeleteButton(id) {
    $('#' + id).clearForm();
    $('#' + id).readOnlyForm(true);
    $("#mode").val('');
    //EnableStatusButton('delete');
    //remove image
    $("#show_image>img").attr("src", "");
    ChangeButtonStatus(false);
    isOn = false;
    action = "none";
}

// Function processing when click on Cancel button
function CancelButton(id) {
    $('#' + id).clearForm();
    $('#' + id).readOnlyForm(true);
    $("#mode").val('');
    $("#" + id).validationEngine('hideAll');
    $("#" + id).validationEngine('detach');
    //$(".field-validation-error").empty();
    //clear image
    $("#show_image>img").attr("src", "");
    //EnableStatusButton('cancel');
    ChangeButtonStatus(false);
    isOn = false;
    action = "none";
}

// Function enable status of button
function EnableStatusButton(id) {    
    $('#btnAdd').attr("disabled", true);
    $('#btnEdit').attr("disabled", true);
    $('#btnCancel').attr("disabled", true);
    $('#btnSave').attr("disabled", true);
    $('#btnDelete').attr("disabled", true);  

    switch(id){
        case 'add':
        case 'edit':            
            $('#btnSave').removeAttr("disabled");
            $('#btnCancel').removeAttr("disabled");            
            break;
        case 'save':
        case 'cancel':
        case 'delete':
            $('#btnAdd').removeAttr("disabled");
            $('#btnEdit').removeAttr("disabled");
            $('#btnDelete').removeAttr("disabled");
            break;
    }
}

// Function change status of button
function ChangeButtonStatus(bool) {
    $(".btn_master").attr("disabled", bool);
    $(".btn_approve").attr("disabled", !bool);
    if (!bool) {
        $(".fileupload-buttonbar").css("display", "none");        
    } else {
        $(".fileupload-buttonbar").css("display", "block")
    }
    
}

// Function replace null
function ReplaceNull(value) {
    if (value == null) {
        value = "";
    }
    return value;
}

// Function alert object
function alertObject(o) {
    var out = '';
    for (var p in o) {
        out += p + ': ' + o[p] + '\n';
    }
    alert(out);
}


// Function set css for paging
var topH = 27;
$(document).ready(function () {
    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() < $(document).height() - topH) {
            if ($(".pagination").length > 0) {
                $(".pagination").css("bottom", "0");
            }
        } else {
            $(".pagination").css("bottom", topH + "px");
        }

        //
        $(".popup_max").css("top", $(window).scrollTop() + 50 + "px");
        $(".popup_min").css("top", $(window).scrollTop() + $(window).height() - 50 + "px");
        $("#popupBlock").css("top", $(window).scrollTop() + 10 + "px");

        //
        $("#loadingmessage").css("top", $(window).scrollTop() + "px");
    });
});

// This function to caculator and redrawing pager when ajax complete loading
$(document).ajaxComplete(function (event, request, settings) {

    var docH = $(document).height();
    var divH = $("#container").height();
    var curH = $(window).scrollTop() + $(window).height();

    //alert("doc height=" + docH + " - Div Height=" + divH + " - curH=" + curH);
    if (divH < docH || curH == docH) {
        $(".pagination").css("bottom", docH - divH + topH + "px");
    } else if ((docH - curH) < topH) {
        $(".pagination").css("bottom", docH - curH + 2 + "px");
    } else {
        $(".pagination").css("bottom", "0");
    }

});

// Function to fix css on browser
// Examle: for ie: .ie div, chrome: .chrome div, firefox: .gecko div, safari: .safari div ...
function css_browser_selector(u) { var ua = u.toLowerCase(), is = function (t) { return ua.indexOf(t) > -1 }, g = 'gecko', w = 'webkit', s = 'safari', o = 'opera', m = 'mobile', h = document.documentElement, b = [(!(/opera|webtv/i.test(ua)) && /msie\s(\d)/.test(ua)) ? ('ie ie' + RegExp.$1) : is('firefox/2') ? g + ' ff2' : is('firefox/3.5') ? g + ' ff3 ff3_5' : is('firefox/3.6') ? g + ' ff3 ff3_6' : is('firefox/3') ? g + ' ff3' : is('gecko/') ? g : is('opera') ? o + (/version\/(\d+)/.test(ua) ? ' ' + o + RegExp.$1 : (/opera(\s|\/)(\d+)/.test(ua) ? ' ' + o + RegExp.$2 : '')) : is('konqueror') ? 'konqueror' : is('blackberry') ? m + ' blackberry' : is('android') ? m + ' android' : is('chrome') ? w + ' chrome' : is('iron') ? w + ' iron' : is('applewebkit/') ? w + ' ' + s + (/version\/(\d+)/.test(ua) ? ' ' + s + RegExp.$1 : '') : is('mozilla/') ? g : '', is('j2me') ? m + ' j2me' : is('iphone') ? m + ' iphone' : is('ipod') ? m + ' ipod' : is('ipad') ? m + ' ipad' : is('mac') ? 'mac' : is('darwin') ? 'mac' : is('webtv') ? 'webtv' : is('win') ? 'win' + (is('windows nt 6.0') ? ' vista' : '') : is('freebsd') ? 'freebsd' : (is('x11') || is('linux')) ? 'linux' : '', 'js']; c = b.join(' '); h.className += ' ' + c; return c; }; css_browser_selector(navigator.userAgent);