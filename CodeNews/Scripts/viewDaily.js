﻿
var carId = "-1";
var driverId = "";
var currentDay = "";
var refreshFlg = true;
var myMode = "week";
$(document).ready(function () {
    $("#popup").draggable({ cancel: ".handlerCancelMachine" });
    
    if ($("#currentMode").val().length > 0) {
        myMode = $("#currentMode").val();
    }
    
    // Get current date
    ReloadPage();
   
    // Function processing when click on dropdown list
    $(".filter_view_daily").change(function () {
        NewPage("current");
    });

    $('#getday').Zebra_DatePicker({
        format: "Y/m/d"
    });

    $("#resource_cancel").click(function () {
        $(".close-reveal-modal").click();
    });

    // Block Data function
    $("#blockData").click(function () {
        $("#loadingmessage").show();
        $.ajax({
            type: 'POST',
            url: root + '/CarBookingUsing/GetBlockList',
            data: { bookingDate: $("#currentDate").val() },
            dataType: 'html',
            success: function (jsonData) {
                $('#resource').html(jsonData);
                $("#loadingmessage").hide();
                $("#tbl_block_data").reveal();

                $('#tbl_block_data input:checkbox').bind('change', function () {
                    var year = $(this).attr('name');
                    var month = $(this).attr('value');
                    var label = '';

                    if ($(this).is(':checked')) {
                        label = 'insert';
                    } else {
                        label = 'update';
                    }                    
                    $.ajax({
                        type: 'POST',
                        url: root + '/CarBookingUsing/UpdateBlockData',
                        data: {
                            bookingDate: $("#currentDate").val(),
                            year: year,
                            month: month,
                            label: label
                        },
                        dataType: 'html',
                        success: function (jsonData) {                      
                        }
                    });
                });
            }
        });
    });
    
});

function ReloadPage() {
    RefreshApproveList();
    NewPageReload("current");    
    window.setTimeout(ReloadPage, 60000);
    refreshFlg = false;
}

function checkBlock(date) {
    $.ajax({
        type: 'POST',
        url: root + '/CarBookingUsing/GetMonthBlock',
        data: { currentDate: date },
        dataType: 'json',
        success: function (jsonData) {
            if (jsonData == true) {
                $("#mode").parent().addClass("disabled");                
            } else {
                $("#mode").parent().removeClass("disabled");
            }
        }
    });
}

// Function get list of plan and actual
function NewPageReload(option) {
    //--http://localhost:16845/CarBookingUsing/Index?currentDate=2013%2F08%2F12&mode=week&carFil=1&driFil=1

    var carFil = getParameterByName("carFil");
    var link = "";
    if(carFil == "") {
        carId = $("#carId").val() != "" ? $("#carId").val() : "-1";    
    } else {

        carId = carFil;
    }
    var driFil = getParameterByName("driFil");
    if(driFil == "") {
        driverId = $("#driverId").val();    
    } else {
        driverId = driFil;
    }
    
    
    if (option == "frompicker") {
        $("#currentDate").val($("#getday").val().length < 10 ? $("#getday").val() + "/01" : $("#getday").val());
    }
    if (refreshFlg) {
        $("#loadingmessage").show();
    }
    myMode = $("#currentMode").val();
    $.ajax({
        type: 'POST',
        url: root + '/CarBookingUsing/GetJsonList',
        data: {
            mode: $("#currentMode").val(),
            option: option,
            currentDate: $("#currentDate").val(),
            carId: carId,
            driverId: driverId
        },
        dataType: 'json',
        success: function (jsonData) {
            $("#ganttChart").empty();
            $("#ganttChart").ganttView({
                data: jsonData,
                slideWidth: 720//672
            });
            RefreshRightMenu();
            $("#currentDate").val(jsonData[0].name);
            currentDay = jsonData[0].name;
            checkBlock(currentDay);
            $("#headTitle").text(jsonData[0].name + " ~ " + jsonData[jsonData.length - 1].name);
            $("#loadingmessage").hide();

            if (block == "1") {
                $(".ganttview-vtheader-carplate-name").find("a").css("pointer-events", "none");
            } else {
                $(".ganttview-vtheader-carplate-name").find("a").css("pointer-events", "visible");
            }

            // set mode option:selected
            $(".ganttview-vtheader-carplate-name a").each(function () {
                link = $(this).attr("href");
                link += "&mode=" + myMode + "&carFil=" + $("#carId").val() + "&driFil=" + $("#driverId").val();
                $(this).attr("href", link);

                $(this).click(function () {
                    link = $(this).attr("href");
                    link += "&scroll=" + $(window).scrollTop();
                    $("#scroll").val($(window).scrollTop());
                    window.location = link;
                    return false;
                });
            });

            if (myMode == "week") {
                $(".weekMode").addClass("disabled");
            } else if (myMode == "day") {
                $(".dayMode").addClass("disabled");
            } else {
                $(".monthMode").addClass("disabled");
            }

            var querystring = location.search;
            var parram = querystring.split("&");
            if (parram.length > 0) {
                var parramCount = parram.length;
                var lastParram = parram[parramCount - 1];
                var scroll = lastParram.split("=");
                if (scroll[0] == "scroll") {
                    window.scrollTo(0, scroll[1]);
                }
            }
        }

    });
}

// Function get list of plan and actual
function NewPage(option) {
    var carId = $("#carId").val() != "" ? $("#carId").val() : "-1";
    var driverId = $("#driverId").val();
    if (option == "frompicker") {
        $("#currentDate").val($("#getday").val().length < 10 ? $("#getday").val() + "/01" : $("#getday").val());
    }

    $("#loadingmessage").show();
    myMode = $("#currentMode").val();
    $.ajax({
        type: 'POST',
        url: root + '/CarBookingUsing/GetJsonList',
        data: {
            mode: $("#currentMode").val(),
            option: option,
            currentDate: $("#currentDate").val(),
            carId: carId,
            driverId: driverId
        },
        dataType: 'json',
        success: function (jsonData) {
            $("#ganttChart").empty();
            $("#ganttChart").ganttView({
                data: jsonData,
                slideWidth: 720//672
            });
            RefreshRightMenu();
            $("#currentDate").val(jsonData[0].name);
            currentDay = jsonData[0].name;
            checkBlock(currentDay);
            $("#headTitle").text(jsonData[0].name + " ~ " + jsonData[jsonData.length - 1].name);
            $("#loadingmessage").hide();

            if (block == "1") {
                $(".ganttview-vtheader-carplate-name").find("a").css("pointer-events", "none");
            } else {
                $(".ganttview-vtheader-carplate-name").find("a").css("pointer-events", "visible");
            }

            // set mode
            $(".ganttview-vtheader-carplate-name a").each(function () {
                var link = $(this).attr("href");
                link += "&mode=" + myMode + "&carFil=" + $("#carId").val() + "&driFil=" + $("#driverId").val();
                $(this).attr("href", link);

                $(this).click(function () {
                    link = $(this).attr("href");
                    link += "&scroll=" + $(window).scrollTop();
                    window.location = link;
                    return false;
                });
            });

            if (myMode == "week") {
                $(".weekMode").addClass("disabled");
            } else if (myMode == "day") {
                $(".dayMode").addClass("disabled");
            } else {
                $(".monthMode").addClass("disabled");
            }
        }
    });
}

// Delete function: processing when user delete actual booking plan
function DeleteAction() {    
    var crrDate = $("#currentDate").val();
    var crrMode = $("#mode").attr("class");
    var action = confirm("Are you sure to delete data?");
    if (action) {
        if (crrDate != '' && crrMode != '') {
            $("#loadingmessage").show();
            $.ajax({
                type: 'POST',
                url: root + '/CarBookingUsing/DeleteActual',
                data: {
                    mode: crrMode,
                    currentDate: crrDate
                },
                dataType: 'json',
                success: function (jsonData) {
                    NewPage("current");
                }
            });
        }
    }
}

//Function processing when change mode via click on button Day, Week, Month
function ChangeMode(mode) {
    $(".mode").removeClass("disabled");
    $(".mode").css("pointer-events", "visible");
    $("." + mode + "Mode").addClass("disabled");
    $("." + mode + "Mode").css("pointer-events", "none");
    if (mode == "month") {
        $('#getday').Zebra_DatePicker({
            format: "Y/m"
        });
    } else if (mode == "week") {
        $("#getday").Zebra_DatePicker({
            format: "Y/m/d",
            show_week_number: 'Wk'
        });
    }else{
        $('#getday').Zebra_DatePicker({
            format: "Y/m/d"
        });
    }
    
    $("#currentMode").val(mode);
    $("#currentDate").val(currentDay);
    $("#mode").removeClass();
    $("#mode").addClass(mode);
    myMode = mode;
    NewPage("current");
}

function RefreshRightMenu(newBlock) {
    if (!newBlock) {
        newBlock = $('.ganttview-block');
    }
}

function isExistsDiv(div) {
    if (div.length > 0)
        return true;
    return false;
}

function RefreshApproveList() {
    // Ajax load search WH of driver list
    $.ajax({
        type: 'POST',
        url: root + '/CarBookingUsing/GetApprovedList',
        dataType: 'html',
        success: function (data) {
            $('.popup_content').html(data);
        }
    });
}
// get request query string in javascript
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}