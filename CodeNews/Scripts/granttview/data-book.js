var ganttData = [
	{
		id: 1, name: "6 - March", series: [
			{ id: 1, car_plate: "52Y-6567", car_type: "Innova", name: "Son", plan: [
					{ id: 11, actualId: null, start: 0, end: 3, color: 0 },
					{ id: 12, actualId: null, start: 4, end: 6, color: 0 },
					{ id: 13, actualId: null, start: 8, end: 11, color: 0 },
					{ id: 14, actualId: null, start: 13, end: 17, color: 0 },
					
					{ id: 15, actualId: 11, start: 2, end: 4, color: 1 },
					{ id: 16, actualId: 12, start: 5, end: 7, color: 1 },
					{ id: 17, actualId: 13, start: 8, end: 13, color: 1 },
					{ id: 18, actualId: 14, start: 15, end: 20, color: 1 }
				]
			},
			{ id: 2, car_plate: "52Y-6567", car_type: "Innova", name: "Dong", plan: [
					{ id: 21, actualId: null, start: 1, end: 5, color: 0 },
					
					{ id: 22, actualId: 21, start: 4, end: 8, color: 1 }
				] },
			{ id: 3, car_plate: "52Y-6567", car_type: "Siena", name: "Hiep", plan: [
					{ id: 31, actualId: null, start: 3, end: 9, color: 0 }
				] },
			{ id: 4, car_plate: "52Y-6567", car_type: "Siena", name: "Hai", plan: [
					{ id: 41, actualId: null, start: 0, end: 25, color: 0 },
					{ id: 42, actualId: 41, start: 10, end: 40, color: 1 }
				] }
				
		]
	}
];