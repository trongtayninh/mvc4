var ganttData = [
	{
		id: 1, name: "6 - March", series: [
			{ id: 1, car_plate: "52Y-6567", car_type: "Innova", name: "Son", plan: [
					{ start: 4, end: 11, color: "lightgreen" },
                    { start: 14, end: 18, color: "lightgreen" }
				]
			}
            ,
			{ id: 2, car_plate: "52Y-6567", car_type: "Camry", name: "Dong", plan: [
					{ start: 6, end: 12, color: "lightgreen" }
				]
		    },
			{ id: 3, car_plate: "52Y-6567", car_type: "Mercedes", name: "Hiep", plan: [
					{ start: 4, end: 16, color: "lightgreen" }
				]
			},
            { id: 4, car_plate: "52Y-6567", car_type: "Audi C8", name: "Tam", plan: [
					{ start: 4, end: 9, color: "lightgreen" },
                    { start: 12.5, end: 17.5, color: "lightgreen" }
				]
            },
            { id: 5, car_plate: "52Y-6567", car_type: "Innova", name: "Hai", plan: [
					{ start: 5, end: 11, color: "lightgreen" }
				]
            },
            { id: 6, car_plate: "52Y-6567", car_type: "Innova", name: "Thuy", plan: [
					{ start: 6, end: 8, color: "lightgreen" }
				]
            },
            { id: 7, car_plate: "52Y-6567", car_type: "Innova", name: "Tuan", plan: [
					{ start: 7, end: 12, color: "lightgreen" }
				]
            },
            { id: 8, car_plate: "52Y-6567", car_type: "Innova", name: "Dung", plan: [
					{ start: 8, end: 12, color: "lightgreen" }
				]
            },
            { id: 9, car_plate: "52Y-6567", car_type: "Innova", name: "Hoang", plan: [
					{ start: 0, end: 4, color: "lightgreen" }
				]
            }
            
		]
	}
];