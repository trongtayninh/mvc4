﻿/*
jQuery.ganttView v.0.8.8
Copyright (c) 2010 JC Grubbs - jc.grubbs@devmynd.com
MIT License Applies
*/

/*
Options
-----------------
showWeekends: boolean
data: object
cellWidth: number
cellHeight: number
slideWidth: number
dataUrl: string
behavior: {
clickable: boolean,
draggable: boolean,
resizable: boolean,
onClick: function,
onDrag: function,
onResize: function
}
*/


/**
NOTE
Sửa lại phần header nhỏ lại, để các cell ở dưới sẽ dễ nhìn hơn
fix size của header hours là 36
các cell con là 18
....
load dữ liệu
*/

(function (jQuery) {
    var colorArr = ["lightgreen", "lightyellow", "lightpink"];
    /*var print = function(o){
    var str='';

    for(var p in o){
    if(typeof o[p] == 'string'){
    str+= p + ': ' + o[p]+'; </br>';
    }else{
    str+= p + ': { </br>' + print(o[p]) + '}';
    }
    }
    return str;
    }*/
    jQuery.fn.ganttView = function () {
        //$('body').append( print(arguments));
        var args = Array.prototype.slice.call(arguments);

        if (args.length == 1 && typeof (args[0]) == "object") {
            build.call(this, args[0]);
        }

        if (args.length == 2 && typeof (args[0]) == "string") {
            handleMethod.call(this, args[0], args[1]);
        }
    };



    var hours = 24;
    var devide = 2;
    var hours_width = 37;

    function build(options) {

        var els = this;
        var defaults = {
            showWeekends: false,
            cellWidth: 14,
            cellHeight: 31,
            slideWidth: 400,
            vHeaderWidth: 100,


            behavior: {
                clickable: true,
                draggable: true,
                resizable: true,
                selectable: true
            }
        };

        var opts = jQuery.extend(true, defaults, options);

        if (opts.data) {
            build();
        } else if (opts.dataUrl) {
            jQuery.getJSON(opts.dataUrl, function (data) { opts.data = data; build(); });
        }

        function build() {
            var minDays = Math.floor((opts.slideWidth / opts.cellWidth) + 5);

            //var startEnd = DateUtils.getBoundaryDatesFromData(opts.data, minDays);			

            opts.start = 0;
            opts.end = 48;



            els.each(function () {

                var container = jQuery(this);
                var div = jQuery("<div>", { "class": "ganttview" });
                new Chart(div, opts).render();
                container.append(div);

                var w = jQuery("div.ganttview-vtheader", container).outerWidth() +
					jQuery("div.ganttview-slide-container", container).outerWidth();
                container.css("width", (w + 2) + "px");
                //new Behavior(container, opts).apply();
            });
        }
    }

    function handleMethod(method, value) {

        if (method == "setSlideWidth") {
            var div = $("div.ganttview", this);
            div.each(function () {
                var vtWidth = $("div.ganttview-vtheader", div).outerWidth();
                $(div).width(vtWidth + value + 1);
                $("div.ganttview-slide-container", this).width(value);
            });
        }
    }

    var Chart = function (div, opts) {

        function render() {
            addVtHeader(div, opts.data, opts.cellHeight);
            var slideDiv = jQuery("<div>", {
                "class": "ganttview-slide-container",
                "css": { "width": opts.slideWidth + "px", "overflow": "hidden" }
            });

            addHzHeader(slideDiv, opts.cellWidth);
            addGrid(slideDiv, opts.data, opts.cellWidth, opts.showWeekends);
            addBlockContainers(slideDiv, opts.data);
            addBlocks(slideDiv, opts.data, opts.cellWidth, opts.start);
            div.append(slideDiv);
            applyLastClass(div.parent());
        }

        function addVtHeader(div, data, cellHeight) {
            var headerDiv = jQuery("<div>", { "class": "ganttview-vtheader" });
            //print header left
            var itemDiv = jQuery("<div>", { "class": "ganttview-vtheader-item" });

            //add title
            var group = jQuery("<div>", {
                "class": "ganttview-vtheader-item-group",
                "css": {
                    "padding-left": "4px",
                    "overflow": "hidden",
                    "position": "relative"
                }
            });

            //add title date
            group.append(jQuery("<div>", {
                "class": "ganttview-vtheader-item-title",
                "css": { "height": cellHeight + "px",
                    "boder-right": "1px solid #ccc",
                    "float": "left"
                }
            }).append("DATE"));
            //add title car plate
            group.append(jQuery("<div>", {
                "class": "ganttview-vtheader-carplate-title",
                "css": { "height": cellHeight + "px",
                    "boder-right": "1px solid #ccc",
                    "float": "left"
                }
            }).append("CAR PLATE"));
            //add title car type
            group.append(jQuery("<div>", {
                "class": "ganttview-vtheader-cartype-title",
                "css": { "height": cellHeight + "px",
                    "boder-right": "1px solid #ccc",
                    "float": "left"
                }
            }).append("CAR NAME"));
            //add title driver
            group.append(jQuery("<div>", {
                "class": "ganttview-vtheader-series-title",
                "css": {
                    "float": "left"
                }
            }).append("DRIVER"));

            itemDiv.append(group);
            for (var i = 0; i < data.length; i++) {

                // Set background color when day is satuday or sunday
                var cusColor = "#fff none";
                var temp = new Date(data[i].name);
                var cus_day = temp.getDay();
                if (cus_day == 6) {
                    cusColor = "#8CB0E9 none";
                } else if (cus_day == 0) {
                    cusColor = "#F8BABA none";
                }
                var btmHeight = 0;
                //alert(data[i].series.length);
                if (data[i].series.length > 1) {
                    btmHeight = data[i].series.length - 1;
                }
                //alert(data[i].series.length);
                if (data[i].series.length > 0) {
                    //add value date
                    
                    itemDiv.append(jQuery("<div>", {
                        "class": "ganttview-vtheader-item-name",
                        "css": { "height": ((data[i].series.length * cellHeight) / 2 + 8) + "px",
                            "padding-top": ((data[i].series.length * cellHeight) / 2 - 8) + "px",
                            "padding-bottom": btmHeight + "px",
                            "background": cusColor
                        }
                    }).append(data[i].name.substring(5)));

                    //add value car plate				
                    var carplateDiv = jQuery("<div>", {
                        "class": "ganttview-vtheader-carplate",
                        "css": {
                            "border-left": "1px solid #ccc",
                            "margin-left": "-1px"
                        }
                    });

                    for (var j = 0; j < data[i].series.length; j++) {
                        var shortName = data[i].series[j].car_plate;
                        if (shortName.length > 9) {
                            shortName = shortName.substring(0, 7) + "...";
                        }
                        var link = shortName;
                        if (permission != "") {
                            link = '<a href="' + root + "/CarBooking/Index?date=" + data[i].name + "&carId=" + data[i].series[j].id + "&driverName=" + data[i].series[j].name + '">' + shortName + '</a>';
                        }
                        carplateDiv.append(jQuery("<div>", {
                            "class": "ganttview-vtheader-carplate-name",
                            "css": { "padding-top": "7px",
                                "margin-bottom": "-7px",
                                "background": cusColor
                            },
                            "title": data[i].series[j].car_plate
                        })
						.append(link));
                    }
                    itemDiv.append(carplateDiv);

                    //add value company
                    var cartypeDiv = jQuery("<div>", {
                        "class": "ganttview-vtheader-cartype",
                        "css": {
                            "border-left": "1px solid #ccc",
                            "margin-left": "-1px"
                        }

                    });
                    for (var j = 0; j < data[i].series.length; j++) {

                        cartypeDiv.append(jQuery("<div>", {
                            "class": "ganttview-vtheader-cartype-name",
                            "css": { "padding-top": "7px",
                                "margin-bottom": "-7px",
                                "background": cusColor
                            },
                            "title": data[i].series[j].car_type
                        })
						.append(ShortName(data[i].series[j].car_type)));
                    }
                    itemDiv.append(cartypeDiv);
                    //add value driver
                    var seriesDiv = jQuery("<div>", {
                        "class": "ganttview-vtheader-series",
                        "css": {
                            "border-left": "1px solid #ccc",
                            "margin-left": "-1px"
                        }
                    });
                    for (var j = 0; j < data[i].series.length; j++) {

                        seriesDiv.append(jQuery("<div>", {
                            "class": "ganttview-vtheader-series-name",
                            "css": { "padding-top": "7px",
                                "margin-bottom": "-7px",
                                "background": cusColor

                            },
                            "title": data[i].series[j].name,
                            "number": i + "/" + j
                        })
						.append(ShortName(data[i].series[j].name)));
                    }
                }
                itemDiv.append(seriesDiv);
                headerDiv.append(itemDiv);
            }
            div.append(headerDiv);
        }

        //init header
        function addHzHeader(div, cellWidth) {
            var headerDiv = jQuery("<div>", { "class": "ganttview-hzheader" });
            var monthsDiv = jQuery("<div>", { "class": "ganttview-hzheader-months" });
            var daysDiv = jQuery("<div>", { "class": "ganttview-hzheader-days" });
            var totalW = 0;

            var hours = 24;
            var divide = 2;
            for (var i = 0; i < hours; i++) {
                var w = i * cellWidth;
                totalW = totalW + w;

                monthsDiv.append(jQuery("<div>", {
                    "class": "ganttview-hzheader-month"
                }).append(i));

                for (var d = 0; d < divide; d++) {
                    if (d == 0) {
                        daysDiv.append(jQuery("<div>", {
                            "class": "ganttview-hzheader-day",
                            "css": {
                                "background-color": "blue"
                            }
                        }));
                    } else {
                        daysDiv.append(jQuery("<div>", {
                            "class": "ganttview-hzheader-day",
                            "css": {
                                "background-color": "yellow"
                            }
                        }));
                    }

                }

            }

            monthsDiv.css("width", (hours * hours_width + hours) + "px");
            daysDiv.css("width", (hours * hours_width + hours) + "px");
            //daysDiv.css("width", totalW + "px");
            headerDiv.append(monthsDiv).append(daysDiv);
            div.append(headerDiv);
        }

        function addGrid(div, data, cellWidth, showWeekends) {
            var gridDiv = jQuery("<div>", { "class": "ganttview-grid" });
            var rowDiv = jQuery("<div>", { "class": "ganttview-grid-row", "css": { "float": "left"} });

            for (var i = 0; i < hours * devide; i++) {
                var cellDiv = jQuery("<div>", { "class": "ganttview-grid-row-cell" });
                rowDiv.append(cellDiv);
            }

            //alert(totalW);
            var w = jQuery("div.ganttview-grid-row-cell", rowDiv).length * cellWidth;
            rowDiv.css("width", (hours * hours_width + hours) + "px");
            gridDiv.css("width", (hours * hours_width + hours) + "px");
            //rowDiv.css("width", w + "px");
            //gridDiv.css("width", w + "px");
            for (var i = 0; i < data.length; i++) {
                for (var j = 0; j < data[i].series.length; j++) {
                    rowDiv.attr("number", i + "/" + j);
                    gridDiv.append(rowDiv.clone());
                }
            }
            div.append(gridDiv);
        }

        function addBlockContainers(div, data) {
            var blocksDiv = jQuery("<div>", { "class": "ganttview-blocks" });
            for (var i = 0; i < data.length; i++) {
                for (var j = 0; j < data[i].series.length; j++) {
                    blocksDiv.append(jQuery("<div>", { "class": "ganttview-block-container", "number": i + "/" + j }));
                }
            }
            div.append(blocksDiv);
        }

        function addBlocks(div, data, cellWidth, start) {
            var rows = jQuery("div.ganttview-blocks div.ganttview-block-container", div);
            var rowIdx = 0;
            for (var i = 0; i < data.length; i++) {
                for (var j = 0; j < data[i].series.length; j++) {


                    for (var k = 0; k < data[i].series[j].plan.length; k++) {

                        var plan = data[i].series[j].plan[k];
                        var size = plan.end - plan.start;
                        var offset = plan.start * (cellWidth + 1);

                        var block = jQuery("<div>", {
                            "class": "ganttview-block layer" + plan.layer,
                            "title": data[i].series[j].name + ", " + size / 2 + " hours",
                            "css": {
                                "width": ((size * cellWidth) + (size - 3)) + "px",
                                "margin-left": (offset) + "px",
                                "position": "absolute",
                                "z-index": plan.color
                            },
                            "start": plan.start,
                            "end": plan.end,
                            "startpoint": plan.startPoint,
                            "finishpoint": plan.endPoint,
                            "booker": plan.booker,
                            "user": plan.user,
                            "size": size,
                            "layer": plan.color
                        });

                        if (plan.planId) {
                            var actual;
                            $(data[i].series[j].plan).each(function () {
                                if (this.id == plan.planId && this.planId == null) {
                                    actual = this;
                                    return;
                                }
                            });
                            
                            block.poshytip();
                            var overtime;
                            if (typeof(actual) != 'undefined') {
                                overtime = plan.end - actual.end;
                                if (overtime < 0) {
                                    overtime = 0;
                                }
                            }
                            //alert(overtime * (cellWidth + 1) - 2);
                            block.append(jQuery("<div>", { "class": "ganttview-block-over",
                                "css": {
                                    "width": overtime * (cellWidth + 1) - 2,
                                    "height": "100%",
                                    "background-color": colorArr[2],
                                    "float": "right"
                                },
                                "overtime": overtime
                            }));
                        }

                        addBlockData(block, plan);
                        if (data[i].series[j].plan[k].color > -1) {
                            block.css("background-color", colorArr[data[i].series[j].plan[k].color]);
                        }
                        block.append(jQuery("<div>", { "class": "ganttview-block-text" }).text(size / 2 + ' h'));
                        jQuery(rows[rowIdx]).append(block);

                    }
                    rowIdx = rowIdx + 1;
                }
            }
        }

        function addBlockData(block, plan) {
            // This allows custom attributes to be added to the series data objects
            // and makes them available to the 'data' argument of click, resize, and drag handlers
            block.data("block-data", plan);
        }

        function applyLastClass(div) {
            jQuery("div.ganttview-grid-row div.ganttview-grid-row-cell:last-child", div).addClass("last");
            jQuery("div.ganttview-hzheader-days div.ganttview-hzheader-day:last-child", div).addClass("last");
            jQuery("div.ganttview-hzheader-months div.ganttview-hzheader-month:last-child", div).addClass("last");
        }

        return {
            render: render
        };
    }
    function ShortName(value) {
        if (value.length > 7) {
            value = value.substring(0, 7) + "...";
        }
        return value;
    }
})(jQuery);