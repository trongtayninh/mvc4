(function (jQuery) {
    var colorArr = ["lightgreen", "lightyellow", "lightpink"];
    /*var print = function(o){
    var str='';

    for(var p in o){
    if(typeof o[p] == 'string'){
    str+= p + ': ' + o[p]+'; </br>';
    }else{
    str+= p + ': { </br>' + print(o[p]) + '}';
    }
    }
    return str;
    }*/
    jQuery.fn.ganttView = function () {
        //$('body').append( print(arguments));
        var args = Array.prototype.slice.call(arguments);

        if (args.length == 1 && typeof (args[0]) == "object") {
            build.call(this, args[0]);
        }

        if (args.length == 2 && typeof (args[0]) == "string") {
            handleMethod.call(this, args[0], args[1]);
        }
    };



    var hours = 24;
    var devide = 2;
    var hours_width = 37;

    function build(options) {

        var els = this;
        var defaults = {
            showWeekends: false,
            cellWidth: 14,
            cellHeight: 31,
            slideWidth: 400,
            vHeaderWidth: 100,


            behavior: {
                clickable: true,
                draggable: true,
                resizable: true,
                selectable: true
            }
        };

        var opts = jQuery.extend(true, defaults, options);

        if (opts.data) {
            build();
        } else if (opts.dataUrl) {
            jQuery.getJSON(opts.dataUrl, function (data) { opts.data = data; build(); });
        }

        function build() {
            var minDays = Math.floor((opts.slideWidth / opts.cellWidth) + 5);

            //var startEnd = DateUtils.getBoundaryDatesFromData(opts.data, minDays);			

            opts.start = 0;
            opts.end = 48;



            els.each(function () {

                var container = jQuery(this);
                var div = jQuery("<div>", { "class": "ganttview" });
                new Chart(div, opts).render();
                container.append(div);

                var w = jQuery("div.ganttview-vtheader", container).outerWidth() +
					jQuery("div.ganttview-slide-container", container).outerWidth();
                container.css("width", (w + 2) + "px");                
                if (checkBlockKey == false) {
                    new Behavior(container, opts).apply();
                }
            });
        }
    }

    function handleMethod(method, value) {

        if (method == "setSlideWidth") {
            var div = $("div.ganttview", this);
            div.each(function () {
                var vtWidth = $("div.ganttview-vtheader", div).outerWidth();
                $(div).width(vtWidth + value + 1);
                $("div.ganttview-slide-container", this).width(value);
            });
        }
    }

    var Chart = function (div, opts) {

        function render() {
            addVtHeader(div, opts.data, opts.cellHeight);

            var slideDiv = jQuery("<div>", {
                "class": "ganttview-slide-container",
                "css": { "width": opts.slideWidth + "px", "overflow": "hidden" }
            });

            //dates = getDates(opts.start, opts.end);
            addHzHeader(slideDiv, opts.cellWidth);
            addGrid(slideDiv, opts.data, opts.cellWidth, opts.showWeekends);
            addBlockContainers(slideDiv, opts.data);
            addBlocks(slideDiv, opts.data, opts.cellWidth, opts.start);
            div.append(slideDiv);
            applyLastClass(div.parent());
        }

        function addVtHeader(div, data, cellHeight) {
            var headerDiv = jQuery("<div>", { "class": "ganttview-vtheader" });
            //print header left
            var itemDiv = jQuery("<div>", { "class": "ganttview-vtheader-item" });

            //add title
            var group = jQuery("<div>", {
                "class": "ganttview-vtheader-item-group",
                "css": {
                    "padding-left": "4px",
                    "overflow": "hidden",
                    "position": "relative"
                }
            });

            //add title date
            group.append(jQuery("<div>", {
                "class": "ganttview-vtheader-item-title",
                "css": { "height": cellHeight + "px",
                    "boder-right": "1px solid #ccc",
                    "float": "left"
                }
            }).append("DATE"));
            //add title car plate
            group.append(jQuery("<div>", {
                "class": "ganttview-vtheader-carplate-title",
                "css": { "height": cellHeight + "px",
                    "boder-right": "1px solid #ccc",
                    "float": "left"
                }
            }).append("CAR PLATE"));
            //add title car type
            group.append(jQuery("<div>", {
                "class": "ganttview-vtheader-cartype-title",
                "css": { "height": cellHeight + "px",
                    "boder-right": "1px solid #ccc",
                    "float": "left"
                }
            }).append("CAR NAME"));
            //add title driver
            group.append(jQuery("<div>", {
                "class": "ganttview-vtheader-series-title",
                "css": {
                    "float": "left"
                }
            }).append("DRIVER"));

            itemDiv.append(group);
            for (var i = 0; i < data.length; i++) {
                //alert(data[i].name);


                //add value date
                itemDiv.append(jQuery("<div>", {
                    "class": "ganttview-vtheader-item-name",
                    "css": { "height": (data[i].series.length * cellHeight) / 2 + "px",
                        "padding-top": "7px"
                    }
                }).append(data[i].name.substring(5)));
                //add value car plate				
                var carplateDiv = jQuery("<div>", {
                    "class": "ganttview-vtheader-carplate",
                    "css": {
                        "border-left": "1px solid #ccc",
                        "margin-left": "-1px"
                    }
                });
                for (var j = 0; j < data[i].series.length; j++) {

                    carplateDiv.append(jQuery("<div>", {
                        "class": "ganttview-vtheader-carplate-name",
                        "css": { "padding-top": "7px",
                            "margin-bottom": "-7px"
                        },
                        "title": data[i].series[j].car_plate
                    }).append(ShortName(data[i].series[j].car_plate)));
                }
                itemDiv.append(carplateDiv);
                //add value car type
                var cartypeDiv = jQuery("<div>", {
                    "class": "ganttview-vtheader-cartype",
                    "css": {
                        "border-left": "1px solid #ccc",
                        "margin-left": "-1px"
                    }
                });
                for (var j = 0; j < data[i].series.length; j++) {
                    var shortName = data[i].series[j].car_type;
                    if (shortName.length > 7) {
                        shortName = shortName.substring(0, 7) + "...";
                    }
                    cartypeDiv.append(jQuery("<div>", {
                        "class": "ganttview-vtheader-cartype-name",
                        "css": { "padding-top": "7px",
                            "margin-bottom": "-7px"
                        },
                        "title": data[i].series[j].car_type
                    }).append(shortName));
                }
                itemDiv.append(cartypeDiv);
                //add value driver
                var seriesDiv = jQuery("<div>", {
                    "class": "ganttview-vtheader-series",
                    "css": {
                        "border-left": "1px solid #ccc",
                        "margin-left": "-1px"
                    }
                });
                for (var j = 0; j < data[i].series.length; j++) {
                    var shortName = data[i].series[j].name;
                    if (shortName.length > 7) {
                        shortName = shortName.substring(0, 7) + "...";
                    }
                    seriesDiv.append(jQuery("<div>", {
                        "class": "ganttview-vtheader-series-name",
                        "css": { "padding-top": "7px",
                            "margin-bottom": "-7px"

                        },
                        "title": data[i].series[j].name,
                        "date": data[i].id,
                        "carid": data[i].series[j].id,
                        "driverid": data[i].series[j].driverId,
                        "number": i + "/" + j
                    }).append(shortName));
                }
                itemDiv.append(seriesDiv);
                headerDiv.append(itemDiv);
            }
            div.append(headerDiv);
        }

        //init header
        function addHzHeader(div, cellWidth) {
            var headerDiv = jQuery("<div>", { "class": "ganttview-hzheader" });
            var monthsDiv = jQuery("<div>", { "class": "ganttview-hzheader-months" });
            var daysDiv = jQuery("<div>", { "class": "ganttview-hzheader-days" });
            var totalW = 0;

            var hours = 24;
            var divide = 2;
            for (var i = 0; i < hours; i++) {
                var w = i * cellWidth;
                totalW = totalW + w;

                monthsDiv.append(jQuery("<div>", {
                    "class": "ganttview-hzheader-month"
                }).append(i));

                for (var d = 0; d < divide; d++) {
                    if (d == 0) {
                        daysDiv.append(jQuery("<div>", {
                            "class": "ganttview-hzheader-day",
                            "css": {
                                "background-color": "blue"
                            }
                        }));
                    } else {
                        daysDiv.append(jQuery("<div>", {
                            "class": "ganttview-hzheader-day",
                            "css": {
                                "background-color": "yellow"
                            }
                        }));
                    }

                }

            }

            monthsDiv.css("width", (hours * hours_width + hours) + "px");
            daysDiv.css("width", (hours * hours_width + hours) + "px");
            //daysDiv.css("width", totalW + "px");
            headerDiv.append(monthsDiv).append(daysDiv);
            div.append(headerDiv);
        }

        function addGrid(div, data, cellWidth, showWeekends) {
            var gridDiv = jQuery("<div>", { "class": "ganttview-grid" });
            var rowDiv = jQuery("<div>", { "class": "ganttview-grid-row", "css": { "float": "left"} });

            for (var i = 0; i < hours * devide; i++) {
                var cellDiv = jQuery("<div>", { "class": "ganttview-grid-row-cell" });
                rowDiv.append(cellDiv);
            }

            //alert(totalW);
            var w = jQuery("div.ganttview-grid-row-cell", rowDiv).length * cellWidth;
            rowDiv.css("width", (hours * hours_width + hours) + "px");
            gridDiv.css("width", (hours * hours_width + hours) + "px");
            //rowDiv.css("width", w + "px");
            //gridDiv.css("width", w + "px");
            for (var i = 0; i < data.length; i++) {
                for (var j = 0; j < data[i].series.length; j++) {
                    rowDiv.attr("number", i + "/" + j);
                    gridDiv.append(rowDiv.clone());
                }
            }
            div.append(gridDiv);
        }

        function addBlockContainers(div, data) {
            var blocksDiv = jQuery("<div>", { "class": "ganttview-blocks" });
            for (var i = 0; i < data.length; i++) {
                for (var j = 0; j < data[i].series.length; j++) {
                    blocksDiv.append(jQuery("<div>", { "class": "ganttview-block-container", "number": i + "/" + j }));
                }
            }
            div.append(blocksDiv);
        }

        function addBlocks(div, data, cellWidth, start) {
            var rows = jQuery("div.ganttview-blocks div.ganttview-block-container", div);
            var rowIdx = 0;
            for (var i = 0; i < data.length; i++) {
                for (var j = 0; j < data[i].series.length; j++) {
                    for (var k = 0; k < data[i].series[j].plan.length; k++) {
                        var plan = data[i].series[j].plan[k];
                        var size = plan.end - plan.start;
                        var offset = plan.start * (cellWidth + 1);
                        var block = jQuery("<div>", {
                            "id": plan.id,
                            "class": "ganttview-block layer" + plan.layer,
                            "title": data[i].series[j].name + ", " + size / 2 + " hours",
                            "css": {
                                "width": ((size * cellWidth) + (size - 3)) + "px",
                                "margin-left": (offset) + "px",
                                "position": "absolute",
                                "z-index": plan.layer
                            },
                            "start": plan.start,
                            "end": plan.end,
                            "size": size,
                            "layer": plan.color
                        });

                        if (plan.planId) {
                            var actual;
                            $(data[i].series[j].plan).each(function () {
                                if (this.id == plan.planId && this.planId == null) {
                                    actual = this;
                                    return;
                                }
                            });
                            var overtime = plan.end - actual.end;
                            if (overtime < 0) {
                                overtime = 0;
                            }
                            var overtimebf = actual.start - plan.start;
                            if (overtimebf < 0) {
                                overtimebf = 0;
                            }
                            block.append(jQuery("<div>", { "class": "ganttview-block-over",
                                "css": {
                                    "width": overtime * (cellWidth + 1) - 2,
                                    "height": "100%",
                                    "background-color": colorArr[2],
                                    "float": "right"
                                },
                                "overtime": overtime
                            }));
                            block.append(jQuery("<div>", { "class": "ganttview-block-over-before",
                                "css": {
                                    "width": overtimebf * (cellWidth + 1) - 2,
                                    "height": "100%",
                                    "background-color": colorArr[2],
                                    "float": "left"
                                },
                                "overtime": overtimebf
                            }));
                        }

                        addBlockData(block, plan);
                        if (data[i].series[j].plan[k].color > -1) {
                            block.css("background-color", colorArr[data[i].series[j].plan[k].color]);
                        }
                        block.append(jQuery("<div>", { "class": "ganttview-block-text" }).text(size / 2 + ' h'));
                        jQuery(rows[rowIdx]).append(block);

                    }
                    rowIdx = rowIdx + 1;
                }
            }
        }

        function addBlockData(block, plan) {
            // This allows custom attributes to be added to the series data objects
            // and makes them available to the 'data' argument of click, resize, and drag handlers
            block.data("block-data", plan);
        }

        function applyLastClass(div) {
            jQuery("div.ganttview-grid-row div.ganttview-grid-row-cell:last-child", div).addClass("last");
            jQuery("div.ganttview-hzheader-days div.ganttview-hzheader-day:last-child", div).addClass("last");
            jQuery("div.ganttview-hzheader-months div.ganttview-hzheader-month:last-child", div).addClass("last");
        }

        return {
            render: render
        };
    }

    var Behavior = function (div, opts) {
        //printObject(opts);
        function apply() {

            if (opts.behavior.clickable) {
                bindBlockClick(div, opts.behavior.onClick);
            }

            if (opts.behavior.selectable) {
                bindBlockSelect(div, opts.cellWidth, opts.start, opts.behavior.onSelect, opts.behavior.onDrag, opts.behavior.onResize, opts.behavior.onClick);
            }

            if (opts.behavior.resizable) {
                bindBlockResize(div, opts.cellWidth, opts.start, opts.behavior.onResize);
            }

            if (opts.behavior.draggable) {
                bindBlockDrag(div, opts.cellWidth, opts.start, opts.behavior.onDrag);
            }
        }

        function bindBlockClick(div, callback) {
            jQuery("div.ganttview-block", div).live("click", function () {
                if (callback) { callback(jQuery(this).data("block-data")); }
            });

            jQuery("div.ganttview-block", div).live("mousedown", function () {
                var oldBlock = jQuery(this);
                $("#oldBlockLeft").val(oldBlock.offset().left);
                $("#oldBlockRight").val(oldBlock.offset().left + oldBlock.outerWidth());
                $("#oldBlockSize").val(oldBlock.attr("size"));
                $("#oldBlockOver").val(oldBlock.find(".ganttview-block-over").attr("overtime"));
                $("#oldBlockOverBf").val(oldBlock.find(".ganttview-block-over-before").attr("overtime"));
                //var overtimeblock = oldBlock.find(".ganttview-block-over");
                //overtimeblock.css("width", 0);
            });
        }
        var check1 = 0;
        function bindBlockSelect(div, cellWidth, startTime, callbackSelect, callbackDrag, callbackResize, callbackClick) {
            jQuery(".ganttview-grid-row", div).selectable({
                selecting: function (e, ui) {
                },
                stop: function () {
                    var container = jQuery("div.ganttview-slide-container", div);
                    var offset = $(".ui-selected", this).first().offset().left - container.offset().left - 1;
                    var newStart = Math.round(offset / cellWidth);

                    var number = $(".ui-selected", this).parents().attr("number");
                    var rows = $(".ganttview-block-container[number=" + number + "]");
                    var name = $(".ganttview-vtheader-series-name[number=" + number + "]").attr("title").split(",")[0];
                    var start = $(".ui-selected", this).first().index();
                    var end = $(".ui-selected", this).last().index() + 1;
                    var size = $(".ui-selected", this).length;
                    var title = name + ", " + size / 2 + " hours";

                    //check new block is valid
                    var checkValidData = false;
                    var children = rows.find("div.ganttview-block[layer=0]");
                    var overtime = 0;
                    var overtimebf = 0;
                    var i = 0;
                    if (children.length > 1) {
                        var oldendprev = 0;
                        for (i = 0; i < children.length - 1; i++) {
                            /*var oldstart = children.eq(i).attr("start");
                            var oldstartnext = children.eq(i + 1).attr("start");
                            if(start >= oldstart && start <= oldstartnext && end <= oldstartnext && start < oldend) {
                            checkValidData = true;
                            break;
                            }*/
                            var oldstart = children.eq(i).attr("start");
                            var oldend = children.eq(i).attr("end");
                            var oldstartnext = children.eq(i + 1).attr("start");
                            if (i > 0) {
                                var oldendprev = children.eq(i - 1).attr("end");
                            }
                            // if (start >= oldstart && start < oldend && end <= oldstartnext) {
                            if (start >= oldendprev && start < oldend && end > oldstart && end <= oldstartnext) {
                                checkValidData = true;
                                break;
                            }
                        }
                        var oldstart = children.last().attr("start");
                        var oldend = children.last().attr("end");
                        if (children.length > 0) {
                            var oldendprev = children.last().prev().attr("end");
                        }
                        // if (start >= oldstart && start < oldend) {
                        if (start >= oldendprev && end > oldstart && start < oldend) {
                            checkValidData = true;
                        }
                        if (end > parseInt(children.eq(i).attr("end"))) {
                            overtime = end - parseInt(children.eq(i).attr("end"));
                        }
                        if (start < parseInt(children.eq(i).attr("start"))) {
                            overtimebf = parseInt(children.eq(i).attr("start")) - start;
                        }
                    } else if (children.length == 0) {
                        checkValidData = false;
                    } else if ((start >= children.first().attr("start") && start < children.first().attr("end"))
                                || (end > children.first().attr("start") && end <= children.first().attr("end"))
                                || (start <= children.first().attr("start") && end >= children.first().attr("end"))) {
                        if (end > parseInt(children.first().attr("end"))) {
                            overtime = end - parseInt(children.eq(i).attr("end"));
                        }
                        if (start < parseInt(children.first().attr("start"))) {
                            overtimebf = parseInt(children.eq(i).attr("start")) - start;
                        }
                        checkValidData = true;
                    }
                    var planId = children.eq(i).attr("id");

                    rows.find("div.ganttview-block[layer=1]").each(function () {
                        var oldstart = $(this).attr("start");
                        var oldend = $(this).attr("end");
                        //$("#testArea").append("oldstart:" + oldstart + " oldend:" + oldend + " start:" + start + " end:" + end + "<br>");
                        if ((oldstart >= start && oldstart < end) || (oldend > start && oldend <= end) || (start >= oldstart && start < oldend) || (end > oldstart && end <= oldend)) {
                            checkValidData = false;
                        }
                    });

                    if (checkValidData) {
                        if (overtime > size) {
                            overtime = size;
                        }
                        var block = jQuery("<div>", {
                            "class": "ganttview-block layer0",
                            "title": title,
                            "css": {
                                "width": ((size * cellWidth) + (size - 3)) + "px",
                                "margin-left": (offset) + "px",
                                "position": "absolute"
                            },
                            "start": start,
                            "end": end,
                            "size": size,
                            "layer": 1
                        });

                        block.css("background-color", colorArr[1]);

                        block.append(jQuery("<div>", { "class": "ganttview-block-text" }).text(size / 2 + ' h'));
                        block.append(jQuery("<div>", { "class": "ganttview-block-over",
                            "css": {
                                "width": overtime * (cellWidth + 1) - 2,
                                "height": "100%",
                                "background-color": colorArr[2],
                                "float": "right"
                            },
                            "overtime": overtime
                        }));
                        block.append(jQuery("<div>", { "class": "ganttview-block-over-before",
                            "css": {
                                "width": overtimebf * (cellWidth + 1) - 2,
                                "height": "100%",
                                "background-color": colorArr[2],
                                "float": "left"
                            },
                            "overtime": overtimebf
                        }));

                        var rowsData = $(".ganttview-vtheader-series-name[number=" + number + "]");

                        var blockData = { id: "", carId: rowsData.attr("carid"), driverId: rowsData.attr("driverid"), bookingDate: rowsData.attr("date"),
                            start: start, end: end, planId: planId
                        };
                        var series = { car_plate: "52Y-6567", car_type: "Innova", name: "Son", plan: [
								{ start: start, end: end }
							]
                        };
                        jQuery.extend(blockData, series);
                        block.data("block-data", blockData);

                        jQuery(rows).append(block);

                        bindBlockClick(div, cellWidth, startTime, callbackClick);
                        bindBlockResize(div, cellWidth, startTime, callbackResize);
                        bindBlockDrag(div, cellWidth, startTime, callbackDrag);

                        if (callbackSelect) { callbackSelect(block.data("block-data"), block); }
                    }
                }
            });
        }

        function bindBlockResize(div, cellWidth, startTime, callback) {
            var i = 0;
            jQuery("div.ganttview-block", div).resizable({
                grid: cellWidth + 1,
                handles: "e",
                resize: function (event, ui) {
                    var block = jQuery(this);
                    var result = RefreshOverTime(div, block, cellWidth);
                    var overtime = result.overtime;
                    var overtimeblock = block.find(".ganttview-block-over");
                    var overwidth = overtime * (cellWidth + 1);
                    overtimeblock.css("width", overwidth);

                    var overtimebf = result.overtimebf;
                    var overtimebfblock = block.find(".ganttview-block-over-before");
                    var overwidthbf = overtimebf * (cellWidth + 1);
                    overtimebfblock.css("width", overwidthbf);
                },
                stop: function () {
                    var block = jQuery(this);
                    updateDataAndPosition(div, block, cellWidth, startTime, callback);
                }
            });
        }

        function bindBlockDrag(div, cellWidth, startTime, callback) {
            jQuery("div.ganttview-block", div).draggable({
                axis: "x",
                grid: [cellWidth + 1, cellWidth + 1],
                cancel: "div.layer-1",
                stop: function () {
                    var block = jQuery(this);
                    updateDataAndPosition(div, block, cellWidth, startTime, callback);
                }
            });
        }

        function updateDataAndPosition(div, block, cellWidth, startTime, callback) {
            var container = jQuery("div.ganttview-slide-container", div);
            var offset = block.offset().left - container.offset().left - 1;

            // Set new end date
            var width = block.outerWidth() - 2;
            var size = parseInt((width + 3) / (cellWidth + 1)) - 1;
            var checkOutSize = false;
            if (offset < 0) {
                offset = 0;
                checkOutSize = true;
            }
            if (offset + width > opts.slideWidth) {
                offset = opts.slideWidth - width - 2;
                checkOutSize = true;
            }

            var number = block.parents().attr("number");
            var rows = $(".ganttview-block-container[number=" + number + "]");
            var start = parseInt(offset / (cellWidth + 1));
            var end = parseInt((offset + block.outerWidth() + 1) / (cellWidth + 1));
            var backstart = parseInt($("#oldBlockLeft").val());
            var backend = parseInt($("#oldBlockRight").val());
            var backover = parseInt($("#oldBlockOver").val());
            var backoverbf = parseInt($("#oldBlockOverBf").val());
            var oldSize = parseInt($("#oldBlockSize").val()) - 1;

            var result = RefreshOverTime(div, block, cellWidth);
            var checkValidData = result.checkValidData;
            var overtime = result.overtime;
            var overtimebf = result.overtimebf;
            var planId = result.planId;

            var equalCount = 0;

            rows.find("div.ganttview-block[layer=1]").each(function () {
                var oldstart = $(this).attr("start");
                var oldend = $(this).attr("end");
                if (oldstart == start && oldend == end) {
                    equalCount++;
                } else {
                    if (((start >= oldstart && start < oldend) || (end > oldstart && end <= oldend) || (oldstart >= start && oldstart < end) || (oldend > start + 5 && oldend <= end))
							&& $(this).index() != block.index()) {
                        checkValidData = false;
                    }
                }
                if (equalCount > 1 || (checkOutSize && equalCount == 1)) {
                    checkValidData = false;
                }
            });

            if (!checkValidData || checkOutSize) {
                offset = backstart - container.offset().left - 1;
                width = backend - backstart - 1;
                size = oldSize;
                overtime = backover;
                overtimebf = backoverbf;
                planId = "";
            }
            else {
                //block.removeClass("layer0");
                //block.addClass("layer1");
            }
            if (overtime > size) {
                overtime = size + 1;
            }
            if (overtimebf > size) {
                overtimebf = size + 1;
            }
            var oldtitle = block.attr("title").split(",")[0];
            block.attr("title", oldtitle + ", " + (size + 1) / 2 + " hours");
            block.attr("size", size + 1);
            block.attr("start", parseInt(offset / (cellWidth + 1)));
            block.attr("end", parseInt(offset / (cellWidth + 1)) + size + 1);

            var overtimeblock = block.find(".ganttview-block-over");
            var overwidth = overtime * (cellWidth + 1);
            if (overwidth > (size + 1) * cellWidth + size - 1) {
                overwidth = overwidth - 2;
            }
            overtimeblock.css("width", overwidth);
            overtimeblock.attr("overtime", overtime);

            var overtimebfblock = block.find(".ganttview-block-over-before");
            var overwidthbf = overtimebf * (cellWidth + 1);
            if (overwidthbf > (size + 1) * cellWidth + size - 1) {
                overwidthbf = overwidthbf - 2;
            }
            overtimebfblock.css("width", overwidthbf);
            overtimebfblock.attr("overtime", overtimebf);

            // Set new start date
            var newStart = parseInt(offset / (cellWidth + 1));
            block.data("block-data").start = newStart + 1;


            block.data("block-data").end = newStart + size + 1;
            block.data("block-data").planId = planId;
            if (!block.data("block-data").id) {
                block.data("block-data").id = block.attr("id");
            }

            jQuery("div.ganttview-block-text", block).text((size + 1) / 2 + " h");

            // Remove top and left properties to avoid incorrect block positioning,
            // set position to relative to keep blocks relative to scrollbar when scrolling
            block.css("top", "").css("left", "")
				.css("position", "absolute").css("margin-left", offset + "px").css("width", (size + 1) * cellWidth + size - 1);

            if (callback) { callback(block.data("block-data")); }
        }

        function RefreshOverTime(div, block, cellWidth) {
            var container = jQuery("div.ganttview-slide-container", div);
            var offset = block.offset().left - container.offset().left - 1;

            // Set new end date			
            var number = block.parents().attr("number");
            var rows = $(".ganttview-block-container[number=" + number + "]");
            var start = parseInt(offset / (cellWidth + 1));
            var end = parseInt((offset + block.outerWidth() + 1) / (cellWidth + 1));

            //check new block is valid
            var checkValidData = false;
            var children = rows.find("div.ganttview-block[layer=0]");
            //alert(children.length);
            var overtime = 0;
            var overtimebf = 0;
            var i = 0;
            if (children.length > 1) {
                var oldendprev = 0;
                for (i = 0; i < children.length - 1; i++) {
                    /*var oldstart = children.eq(i).attr("start");
                    var oldstartnext = children.eq(i + 1).attr("start");
                    if(start >= oldstart && start <= oldstartnext && end <= oldstartnext) {
                    checkValidData = true;
                    break;
                    }*/
                    var oldstart = children.eq(i).attr("start");
                    var oldend = children.eq(i).attr("end");
                    var oldstartnext = children.eq(i + 1).attr("start");
                    if (i > 0) {
                        oldendprev = children.eq(i - 1).attr("end");
                    }
                    // if (start >= oldstart && start < oldend && end <= oldstartnext) {
                    if (start >= oldendprev && start < oldend && end > oldstart && end <= oldstartnext) {
                        checkValidData = true;
                        break;
                    }
                }
                var oldstart = children.last().attr("start");
                var oldend = children.last().attr("end");
                if (children.length > 0) {
                    oldendprev = children.last().prev().attr("end");
                }
                // if (start >= oldstart && start < oldend) {
                if (start >= oldendprev && end > oldstart && start < oldend) {
                    checkValidData = true;
                }
                if (end > parseInt(children.eq(i).attr("end"))) {
                    overtime = end - parseInt(children.eq(i).attr("end"));
                }
                if (parseInt(children.eq(i).attr("start")) > start) {
                    overtimebf = parseInt(children.eq(i).attr("start")) - start;
                }
            } else if (children.length == 0) {
                checkValidData = false;
            } else if ((start >= children.first().attr("start") && start < children.first().attr("end"))
                                || (end > children.first().attr("start") && end <= children.first().attr("end"))
                                || (start <= children.first().attr("start") && end >= children.first().attr("end"))) {
                if (end > parseInt(children.first().attr("end"))) {
                    overtime = end - parseInt(children.eq(i).attr("end"));
                }
                if (parseInt(children.first().attr("start")) > start) {
                    overtimebf = parseInt(children.eq(i).attr("start")) - start;
                }
                checkValidData = true;
            }
            var planId = children.eq(i).attr("id");
            var result = { checkValidData: checkValidData, overtime: overtime, overtimebf: overtimebf, planId: planId };
            return result;
        }

        return {
            apply: apply
        };
    }

    var ArrayUtils = {

        contains: function (arr, obj) {
            var has = false;
            for (var i = 0; i < arr.length; i++) { if (arr[i] == obj) { has = true; } }
            return has;
        }
    };
    function ShortName(value, lengthValue) {
        if (value.length > 7) {
            value = value.substring(0, 7) + "...";
        }
        return value;
    }
})(jQuery);