/*
jQuery.ganttView v.0.8.8
Copyright (c) 2010 JC Grubbs - jc.grubbs@devmynd.com
MIT License Applies
*/

/*
Options
-----------------
showWeekends: boolean
data: object
cellWidth: number
cellHeight: number
slideWidth: number
dataUrl: string
behavior: {
clickable: boolean,
draggable: boolean,
resizable: boolean,
onClick: function,
onDrag: function,
onResize: function
}
*/


/**
NOTE
Sửa lại phần header nhỏ lại, để các cell ở dưới sẽ dễ nhìn hơn
fix size của header hours là 36
các cell con là 18
....
load dữ liệu
*/

(function (jQuery) {
    var colorArr = ["lightgreen", "lightyellow", "lightpink"];
    /*var print = function(o){
    var str='';

    for(var p in o){
    if(typeof o[p] == 'string'){
    str+= p + ': ' + o[p]+'; </br>';
    }else{
    str+= p + ': { </br>' + print(o[p]) + '}';
    }
    }
    return str;
    }*/
    jQuery.fn.ganttView = function () {
        //$('body').append( print(arguments));
        var args = Array.prototype.slice.call(arguments);

        if (args.length == 1 && typeof (args[0]) == "object") {
            build.call(this, args[0]);
        }

        if (args.length == 2 && typeof (args[0]) == "string") {
            handleMethod.call(this, args[0], args[1]);
        }
    };



    var hours = 24;
    var devide = 2;
    var hours_width = 37;

    function build(options) {

        var els = this;
        var defaults = {
            showWeekends: false,
            cellWidth: 14,
            cellHeight: 31,
            slideWidth: 400,
            vHeaderWidth: 100,


            behavior: {
                clickable: true,
                draggable: true,
                resizable: true,
                selectable: true
            }
        };

        var opts = jQuery.extend(true, defaults, options);

        if (opts.data) {
            build();
        } else if (opts.dataUrl) {
            jQuery.getJSON(opts.dataUrl, function (data) { opts.data = data; build(); });
        }

        function build() {
            var minDays = Math.floor((opts.slideWidth / opts.cellWidth) + 5);

            //var startEnd = DateUtils.getBoundaryDatesFromData(opts.data, minDays);			

            opts.start = 0;
            opts.end = 48;



            els.each(function () {

                var container = jQuery(this);
                var div = jQuery("<div>", { "class": "ganttview" });
                new Chart(div, opts).render();
                container.append(div);

                var w = jQuery("div.ganttview-vtheader", container).outerWidth() +
					jQuery("div.ganttview-slide-container", container).outerWidth();
                container.css("width", (w + 2) + "px");    
                
                //alertObject(opts.data[0].series[0]);     
                
                    new Behavior(container, opts).apply();
                
            });
        }
    }

    function handleMethod(method, value) {

        if (method == "setSlideWidth") {
            var div = $("div.ganttview", this);
            div.each(function () {
                var vtWidth = $("div.ganttview-vtheader", div).outerWidth();
                $(div).width(vtWidth + value + 1);
                $("div.ganttview-slide-container", this).width(value);
            });
        }
    }

    var Chart = function (div, opts) {

        function render() {
            addVtHeader(div, opts.data, opts.cellHeight);

            var slideDiv = jQuery("<div>", {
                "class": "ganttview-slide-container",
                "css": { "width": opts.slideWidth + "px", "overflow": "hidden" }
            });

            //dates = getDates(opts.start, opts.end);
            addHzHeader(slideDiv, opts.cellWidth);
            addGrid(slideDiv, opts.data, opts.cellWidth, opts.showWeekends);
            addBlockContainers(slideDiv, opts.data);
            addBlocks(slideDiv, opts.data, opts.cellWidth, opts.start);
            div.append(slideDiv);
            applyLastClass(div.parent());
        }

        function addVtHeader(div, data, cellHeight) {
            var headerDiv = jQuery("<div>", { "class": "ganttview-vtheader" });
            //print header left
            var itemDiv = jQuery("<div>", { "class": "ganttview-vtheader-item" });

            //add title
            var group = jQuery("<div>", {
                "class": "ganttview-vtheader-item-group",
                "css": {
                    "padding-left": "4px",
                    "overflow": "hidden",
                    "position": "relative"
                }
            });

            //add title date
            group.append(jQuery("<div>", {
                "class": "ganttview-vtheader-item-title",
                "css": { "height": cellHeight + "px",
                    "boder-right": "1px solid #ccc",
                    "float": "left"
                }
            }).append("DATE"));
            //add title car plate
            group.append(jQuery("<div>", {
                "class": "ganttview-vtheader-carplate-title",
                "css": { "height": cellHeight + "px",
                    "boder-right": "1px solid #ccc",
                    "float": "left"
                }
            }).append("CAR PLATE"));
            //add title car type
            group.append(jQuery("<div>", {
                "class": "ganttview-vtheader-cartype-title",
                "css": { "height": cellHeight + "px",
                    "boder-right": "1px solid #ccc",
                    "float": "left"
                }
            }).append("CAR NAME"));
            //add title driver
            group.append(jQuery("<div>", {
                "class": "ganttview-vtheader-series-title",
                "css": {
                    "float": "left"
                }
            }).append("DRIVER"));

            itemDiv.append(group);
            for (var i = 0; i < data.length; i++) {
                //alert(data[i].name);
                // Set background color when day is satuday or sunday
                var cusColor = "#fff none";
                var temp = new Date(data[i].name);
                var cus_day = temp.getDay();
                if (cus_day == 6) {
                    cusColor = "#8CB0E9 none";
                } else if (cus_day == 0) {
                    cusColor = "#F8BABA none";
                }
                var btmHeight = 0;
                //alert(data[i].series.length);
                if (data[i].series.length > 1) {
                    btmHeight = data[i].series.length - 1;
                }

                //add value date
                itemDiv.append(jQuery("<div>", {
                    "class": "ganttview-vtheader-item-name",
                    "css": { "height": (data[i].series.length * cellHeight) / 2 + "px",
                        "padding-top": (data[i].series.length * cellHeight) / 2 + "px",
                        "padding-bottom": btmHeight + "px",
                        "background": cusColor
                    }
                }).append(data[i].name.substring(5)));
                //add value car plate				
                var carplateDiv = jQuery("<div>", {
                    "class": "ganttview-vtheader-carplate",
                    "css": {
                        "border-left": "1px solid #ccc",
                        "margin-left": "-1px",
                    }
                });
                for (var j = 0; j < data[i].series.length; j++) {
                    var shortName = data[i].series[j].car_plate;
                     if (shortName.length > 9) {
                        shortName = shortName.substring(0, 7) + "...";
                     }
                     //alert(permission);
                     if (permission != "") {
                            link = '<a href="' + root + "/CarBooking/Index?date=" + data[i].name + "&carId=" + data[i].series[j].id + "&driverName=" + data[i].series[j].name + "&page=plan" + '">' + shortName + '</a>';
                     }
                   
                    carplateDiv.append(jQuery("<div>", {
                        "class": "ganttview-vtheader-carplate-name",
                        "css": { "padding-top": "7px",
                            "margin-bottom": "-7px",
                            "background": cusColor
                        },
                        "title": data[i].series[j].car_plate
                    })
						.append(link));
                }
                itemDiv.append(carplateDiv);
                //add value car type
                var cartypeDiv = jQuery("<div>", {
                    "class": "ganttview-vtheader-cartype",
                    "css": {
                        "border-left": "1px solid #ccc",
                        "margin-left": "-1px"
                    }
                });
                for (var j = 0; j < data[i].series.length; j++) {
                    var shortName = data[i].series[j].car_type;
                    if (shortName.length > 7) {
                        shortName = shortName.substring(0, 7) + "...";
                    }
                    cartypeDiv.append(jQuery("<div>", {
                        "class": "ganttview-vtheader-cartype-name",
                        "css": { "padding-top": "7px",
                            "margin-bottom": "-7px",
                            "background": cusColor
                        },
                        "title": data[i].series[j].car_type
                    })
						.append(shortName));
                }
                itemDiv.append(cartypeDiv);
                //add value driver
                var seriesDiv = jQuery("<div>", {
                    "class": "ganttview-vtheader-series",
                    "css": {
                        "border-left": "1px solid #ccc",
                        "margin-left": "-1px"
                    }
                });
                for (var j = 0; j < data[i].series.length; j++) {
                    var shortName = data[i].series[j].name;
                    if (shortName.length > 7) {
                        shortName = shortName.substring(0, 7) + "...";
                    }
                    //var driverDdl = jQuery("<div>", { "class": "driverDdl" });
                    var driverLink = jQuery("<a>", {"href": "JavaScript:ChangeDriver(" + data[i].series[j].driverId + "," + i + "," + j + ")" });
                    
                    driverLink.append(shortName);
                    seriesDiv.append(jQuery("<div>", {
                        "class": "ganttview-vtheader-series-name",
                        "css": { "padding-top": "7px",
                            "margin-bottom": "-7px",
                            "background": cusColor

                        },
                        "title": data[i].series[j].name,
                        "date": data[i].id,
                        "carid": data[i].series[j].id,
                        "driverid": data[i].series[j].driverId,
                        "number": i + "/" + j,
                        "group": i
                    })
                    //.append("<input type='hidden' class='driverName' value=" + data[i].series[j].driverId + " />").append(driverDdl));
                        .append(driverLink));

                }
                itemDiv.append(seriesDiv);
                headerDiv.append(itemDiv);
            }
            div.append(headerDiv);
        }

        //init header
        function addHzHeader(div, cellWidth) {
            var headerDiv = jQuery("<div>", { "class": "ganttview-hzheader" });
            var monthsDiv = jQuery("<div>", { "class": "ganttview-hzheader-months" });
            var daysDiv = jQuery("<div>", { "class": "ganttview-hzheader-days" });
            var totalW = 0;

            var hours = 24;
            var divide = 2;
            for (var i = 0; i < hours; i++) {
                var w = i * cellWidth;
                totalW = totalW + w;

                monthsDiv.append(jQuery("<div>", {
                    "class": "ganttview-hzheader-month"
                }).append(i));

                for (var d = 0; d < divide; d++) {
                    if (d == 0) {
                        daysDiv.append(jQuery("<div>", {
                            "class": "ganttview-hzheader-day",
                            "css": {
                                "background-color": "blue"
                            }
                        }));
                    } else {
                        daysDiv.append(jQuery("<div>", {
                            "class": "ganttview-hzheader-day",
                            "css": {
                                "background-color": "yellow"
                            }
                        }));
                    }

                }

            }

            monthsDiv.css("width", (hours * hours_width + hours) + "px");
            daysDiv.css("width", (hours * hours_width + hours) + "px");
            //daysDiv.css("width", totalW + "px");
            headerDiv.append(monthsDiv).append(daysDiv);
            div.append(headerDiv);
        }

        function addGrid(div, data, cellWidth, showWeekends) {
            var gridDiv = jQuery("<div>", { "class": "ganttview-grid" });
            var rowDiv = jQuery("<div>", { "class": "ganttview-grid-row", "css": { "float": "left"} });


            for (var i = 0; i < hours * devide; i++) {
                var cellDiv = jQuery("<div>", { "class": "ganttview-grid-row-cell" });
                rowDiv.append(cellDiv);
            }

            //alert(totalW);
            var w = jQuery("div.ganttview-grid-row-cell", rowDiv).length * cellWidth;
            rowDiv.css("width", (hours * hours_width + hours) + "px");
            gridDiv.css("width", (hours * hours_width + hours) + "px");
            //rowDiv.css("width", w + "px");
            //gridDiv.css("width", w + "px");
            for (var i = 0; i < data.length; i++) {
                for (var j = 0; j < data[i].series.length; j++) {
                    if(data[i].series[0].isBlock == true){
                        rowDiv.addClass("none-select");
                    }
                    rowDiv.attr("number", i + "/" + j);
                    gridDiv.append(rowDiv.clone());
                }
            }
            div.append(gridDiv);
        }

        function addBlockContainers(div, data) {
            var blocksDiv = jQuery("<div>", { "class": "ganttview-blocks" });
            for (var i = 0; i < data.length; i++) {
                for (var j = 0; j < data[i].series.length; j++) {
                    blocksDiv.append(jQuery("<div>", { "class": "ganttview-block-container", "number": i + "/" + j }));
                }
            }
            div.append(blocksDiv);
        }

        function addBlocks(div, data, cellWidth, start) {
            var rows = jQuery("div.ganttview-blocks div.ganttview-block-container", div);
            var rowIdx = 0;
            for (var i = 0; i < data.length; i++) {
                for (var j = 0; j < data[i].series.length; j++) {
                    

                    for (var k = 0; k < data[i].series[j].plan.length; k++) {
                        
                        var plan = data[i].series[j].plan[k];
                        var size = plan.end - plan.start;
                        var offset = plan.start * (cellWidth + 1);                        
                        var block;
                        if(data[i].series[j].isBlock == true){
                            block = jQuery("<div>", {
                                "id": plan.id,
                                "class": "ganttview-block none-edit",
                                "title": data[i].series[j].name + ", " + size / 2 + " hours",
                                "css": {
                                    "width": ((size * cellWidth) + (size - 2)) + "px",
                                    "margin-left": (offset) + "px",
                                    "position": "absolute"
                                },
                                "start": plan.start,
                                "end": plan.end,
                                "size": size
                            });
                        }else{
                            block = jQuery("<div>", {
                                "id": plan.id,
                                "class": "ganttview-block",
                                "title": data[i].series[j].name + ", " + size / 2 + " hours",
                                "css": {
                                    "width": ((size * cellWidth) + (size - 2)) + "px",
                                    "margin-left": (offset) + "px",
                                    "position": "absolute"
                                },
                                "start": plan.start,
                                "end": plan.end,
                                "size": size
                            });
                        }
                        
                        block.poshytip();
                        addBlockData(block, plan);
                        if (data[i].series[j].plan[k].color > -1) {
                            block.css("background-color", colorArr[data[i].series[j].plan[k].color]);
                        }
                        block.append(jQuery("<div>", { "class": "ganttview-block-text" }).text(size / 2 + ' h'));
                        jQuery(rows[rowIdx]).append(block);

                    }
                    rowIdx = rowIdx + 1;
                }
            }
        }

        function addBlockData(block, plan) {
            // This allows custom attributes to be added to the series data objects
            // and makes them available to the 'data' argument of click, resize, and drag handlers
            block.data("block-data", plan);
        }

        function applyLastClass(div) {
            jQuery("div.ganttview-grid-row div.ganttview-grid-row-cell:last-child", div).addClass("last");
            jQuery("div.ganttview-hzheader-days div.ganttview-hzheader-day:last-child", div).addClass("last");
            jQuery("div.ganttview-hzheader-months div.ganttview-hzheader-month:last-child", div).addClass("last");
        }

        return {
            render: render
        };
    }

    var Behavior = function (div, opts) {
        //printObject(opts);
        function apply() {

            if (opts.behavior.clickable) {
                bindBlockClick(div, opts.behavior.onClick);
            }

            if (opts.behavior.selectable) {
                bindBlockSelect(div, opts.cellWidth, opts.start, opts.behavior.onSelect, opts.behavior.onDrag, opts.behavior.onResize, opts.behavior.onClick);
            }

            if (opts.behavior.resizable) {
                bindBlockResize(div, opts.cellWidth, opts.start, opts.behavior.onResize);
            }

            if (opts.behavior.draggable) {
                bindBlockDrag(div, opts.cellWidth, opts.start, opts.behavior.onDrag);
            }
        }

        function bindBlockClick(div, callback) {
            jQuery("div.ganttview-block", div).live("click", function () {
                if (callback) { callback(jQuery(this).data("block-data")); }
            });

            jQuery("div.ganttview-block", div).live("mousedown", function () {
                var oldBlock = jQuery(this);
                $("#oldBlockLeft").val(oldBlock.offset().left);
                $("#oldBlockRight").val(oldBlock.offset().left + oldBlock.outerWidth());
                $("#oldBlockSize").val(oldBlock.attr("size"));
            });
        }
        var check1 = 0;
        function bindBlockSelect(div, cellWidth, startTime, callbackSelect, callbackDrag, callbackResize, callbackClick) {
            jQuery(".ganttview-grid-row", div).selectable({
                selecting: function (e, ui) {
                },
                cancel: "div.none-select",
                stop: function () {
                    var container = jQuery("div.ganttview-slide-container", div);
                    var offset = $(".ui-selected", this).first().offset().left - container.offset().left - 1;
                    var newStart = Math.round(offset / cellWidth);

                    var number = $(".ui-selected", this).parents().attr("number");
                    var rows = $(".ganttview-block-container[number=" + number + "]");
                    var name = $(".ganttview-vtheader-series-name[number=" + number + "]").attr("title").split(",")[0];
                    var start = $(".ui-selected", this).first().index();
                    var end = $(".ui-selected", this).last().index() + 1;
                    var size = $(".ui-selected", this).length;
                    var title = name + ", " + size / 2 + " hours";

                    var checkValidData = true;
                    //$("#testArea").empty();
                    rows.children().each(function () {
                        var oldstart = $(this).attr("start");
                        var oldend = $(this).attr("end");
                        //$("#testArea").append("oldstart:" + oldstart + " oldend:" + oldend + " start:" + start + " end:" + end + "<br>");
                        if ((oldstart >= start && oldstart < end) || (oldend > start && oldend <= end) || (start >= oldstart && start < oldend) || (end > oldstart && end <= oldend)) {
                            checkValidData = false;
                        }
                    });
                    if (checkValidData) {
                        var block = jQuery("<div>", {
                            "class": "ganttview-block",
                            "title": title,
                            "css": {
                                "width": ((size * cellWidth) + (size - 2)) + "px",
                                "margin-left": (offset) + "px",
                                "position": "absolute"
                            },
                            "start": start,
                            "end": end,
                            "size": size
                        });

                        block.css("background-color", colorArr[0]);

                        block.append(jQuery("<div>", { "class": "ganttview-block-text" }).text(size / 2 + ' h'));

                        var rowsData = $(".ganttview-vtheader-series-name[number=" + number + "]");

                        var blockData = { id: "", carId: rowsData.attr("carid"), driverId: rowsData.attr("driverid"), bookingDate: rowsData.attr("date"),
                            start: start, end: end
                        };
                        var series = { car_plate: "52Y-6567", car_type: "Innova", name: "Son", plan: [
								{ start: start, end: end }
							]
                        };
                        jQuery.extend(blockData, series);
                        block.data("block-data", blockData);

                        jQuery(rows).append(block);

                        bindBlockClick(div, cellWidth, startTime, callbackClick);
                        bindBlockResize(div, cellWidth, startTime, callbackResize);
                        bindBlockDrag(div, cellWidth, startTime, callbackDrag);

                        if (callbackSelect) { callbackSelect(block.data("block-data"), block); }
                    }
                }
            });
        }

        function bindBlockResize(div, cellWidth, startTime, callback) {
            jQuery("div.ganttview-block", div).resizable({
                grid: cellWidth + 1,
                handles: "e",
                cancel: "div.none-edit",
                stop: function () {
                    var block = jQuery(this);
                    updateDataAndPosition(div, block, cellWidth, startTime, callback);
                }
            });
        }

        function bindBlockDrag(div, cellWidth, startTime, callback) {
            jQuery("div.ganttview-block", div).draggable({
                axis: "x",
                grid: [cellWidth + 1, cellWidth + 1],
                cancel: "div.none-edit",
                stop: function () {
                    var block = jQuery(this);
                    updateDataAndPosition(div, block, cellWidth, startTime, callback);
                }
            });
        }

        function updateDataAndPosition(div, block, cellWidth, startTime, callback) {
            var container = jQuery("div.ganttview-slide-container", div);
            var offset = block.offset().left - container.offset().left - 1;

            // Set new end date
            var width = block.outerWidth() - 2;
            var size = parseInt((width + 2) / (cellWidth + 1)) - 1;
            var checkOutSize = false;
            if (offset < 0) {
                offset = 0;
                checkOutSize = true;
            }
            if (offset + width > opts.slideWidth) {
                offset = opts.slideWidth - width - 2;
                checkOutSize = true;
            }

            var number = block.parents().attr("number");
            var rows = $(".ganttview-block-container[number=" + number + "]");
            var left = offset + container.offset().left + 1;
            var right = offset + container.offset().left + 1 + block.outerWidth();
            var oldLeft = parseInt($("#oldBlockLeft").val());
            var oldRight = parseInt($("#oldBlockRight").val());
            var oldSize = parseInt($("#oldBlockSize").val()) - 1;

            var checkValidData = true;
            var equalCount = 0;
            //$("#testArea").empty();
            rows.children().each(function () {
                var oldstart = $(this).offset().left;
                var oldend = oldstart + $(this).outerWidth();
                //$("#testArea").append("oldstart:" + oldstart + " oldend:" + oldend + " start:" + left + " end:" + right, + "<br>");
                if (oldstart == left && oldend == right) {
                    equalCount++;
                } else {
                    if (((left > oldstart && left < oldend - 5) || (right > oldstart + 5 && right < oldend) || (oldstart > left && oldstart < right - 5) || (oldend > left + 5 && oldend < right))
							&& (oldstart != left || oldend != right)) {
                        checkValidData = false;
                    }
                }
                if (equalCount > 1 || (checkOutSize && equalCount == 1)) {
                    checkValidData = false;
                }
            });
            if (!checkValidData) {
                offset = oldLeft - container.offset().left - 1;
                width = oldRight - oldLeft - 1;
                size = oldSize;
            }

            var oldtitle = block.attr("title").split(",")[0];
            block.attr("title", oldtitle + ", " + (size + 1) / 2 + " hours");
            block.attr("size", size + 1);
            block.attr("start", parseInt(offset / (cellWidth + 1)));
            block.attr("end", parseInt(offset / (cellWidth + 1)) + size);
            // Set new start date
            var newStart = parseInt(offset / (cellWidth + 1));
            block.data("block-data").start = newStart + 1;

            block.data("block-data").end = newStart + size + 1;

            if (!block.data("block-data").id) {
                block.data("block-data").id = block.attr("id");
            }

            jQuery("div.ganttview-block-text", block).text((size + 1) / 2 + " h");

            // Remove top and left properties to avoid incorrect block positioning,
            // set position to relative to keep blocks relative to scrollbar when scrolling
            block.css("top", "").css("left", "")
				.css("position", "absolute").css("margin-left", offset + "px").css("width", (size + 1) * cellWidth + size - 1);

            if (callback) { callback(block.data("block-data")); }
        }

        return {
            apply: apply
        };
    }

    var ArrayUtils = {

        contains: function (arr, obj) {
            var has = false;
            for (var i = 0; i < arr.length; i++) { if (arr[i] == obj) { has = true; } }
            return has;
        }
    };

})(jQuery);