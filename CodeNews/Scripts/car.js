﻿$(document).ready(function () {

    //  Set read only for carForm
    $('#carForm').readOnlyForm(true);
    ChangeButtonStatus(false);

    // Set background row when mouse hover
    $("#tableId tbody tr").live("mouseenter", function () {
        $(this).css("background-color", "#AED5E1");
    });

    // Remove background when mouse leave
    $("#tableId tbody tr").live("mouseleave", function () {
        $(this).removeAttr('style');
    });

    // Ajax load list car
    $.ajax({
        type: 'POST',
        url: root + '/Car/CarList',
        dataType: 'html',
        success: function (data) {
            $('#listContent').html(data);
            $("#tableId tbody tr:odd").attr("class", 'odd');
        }
    });

    // Function processing when click on button delete 
    $("#btnDelete").click(function () {
        if ($("#carId").val() == "") {
            alert("Please choose an item!");
        }
        else {
            var action = confirm("Are you sure to delete data?");
            if (action) {
                $.ajax(
                {
                    url: root + '/Car/Delete',
                    type: "POST",
                    dataType: "html",
                    data: (getCar()),
                    success: function (data) {
                        $('#listContent').html(data);
                        $("#tableId tbody tr:odd").attr("class", 'odd');
                        if (message != "") {
                            alert(message);
                            DeleteButton('carForm');
                        }                        
                    }
                });
            }
        }
    });

    // Function processing when click on button save 
    $("#btnSave").click(function () {
        //if ($("#carForm").validationEngine('validate')) {
        alert('ss');
        if (1==1) {
            $.ajax(
            {
                url: root + '/YouLost/Save',
                type: "POST",
                dataType: "html",
                data: (getCar()),
                success: function (data) {
                    //$('#listContent').html(data);
                    //$("#tableId tbody tr:odd").attr("class", 'odd');
                    //SaveButton('carForm');
                    alert(data);
                    if (message != "") {
                        alert(message);
                    }
                }
            });
        }
    });
    // Function processing when click on button save 
    $("#btnLoad").click(function () {
        //if ($("#carForm").validationEngine('validate')) {
      
        if (1 == 1) {
            $.ajax(
            {
                url: root + '/YouLost/GetLostById',
                type: "POST",
                dataType: "json",
                data: ({id:"9"}),
                success: function (data) {
                    //$('#listContent').html(data);
                    //$("#tableId tbody tr:odd").attr("class", 'odd');
                    //SaveButton('carForm');
                    alert(data.LienHe);
                    if (message != "") {
                        alert(message);
                    }
                }
            });
        }
    });
    
});

//
function newPage(page, sortField, sortDirection) {
    $.ajax({
        type: 'GET',
        url: root + '/Car/CarList?page=' + page + '&sortField=' + sortField + '&sortDirection=' + sortDirection,
        dataType: 'html',
        success: function (data) {
            $('#listContent').html(data);
            $("#tableId tbody tr:odd").attr("class", 'odd');
        }
    });
}

//Function sort list
function sort(page, sortField, sortDirection, newSortField) {
    if (sortField == newSortField) {
        if (sortDirection == "asc") {
            sortDirection = "desc";
        } else if (sortDirection == "desc") {
            sortField = "carId";
        } else {
            sortDirection = "asc";
        }
        
    } else {
        sortField = newSortField;
        sortDirection = "asc";
    }

    $.ajax({
        type: 'GET',
        url: root + '/Car/CarList?page=' + page + '&sortField=' + sortField + '&sortDirection=' + sortDirection,
        dataType: 'html',
        success: function (data) {
            $('#listContent').html(data);
            $("#tableId tbody tr:odd").attr("class", 'odd');
        }
    });
}

// Function select car
function SelectCar(obj, carId) {
    if ($("#mode").val() == '') {
        $.ajax({
            type: 'POST',
            url: root + '/Car/GetCarById',
            data: { id: carId },
            dataType: 'json',
            success: function (json) {
                $('.active').attr("class", '');
                $("#tableId tbody tr:odd").attr("class", 'odd');
                $("#carId").val(json.carId);
                $("#carName").val(json.carName);
                $("#carPlate").val(json.carPlate);
                $("#driverId").val(json.driverId);
                $("#rentalFee").val(ReplaceNull(json.rentalFee)).formatCurrency({ roundToDecimalPlace: 0 });
                $("#color").val(ReplaceNull(json.color));
                $("#companyId").val(ReplaceNull(json.companyId));
                $("#note").val(ReplaceNull(json.note));
                $("#carOldImage").val(ReplaceNull(json.image));
                if (json.image != null && json.image != "") {
                    $("#show_image").html("<img src='"+ root +"/Images/Items/" + json.image + "' width='120' height='100' />");
                } else {
                    $("#show_image").html("<img src='" + root + "/Content/Images/NoPhotoIcon.jpg' width='120' height='100' />");
                }
                if (json.locked == "Locked") {
                    $("#locked").attr('checked', true)
                }
                else {
                    $("#locked").attr('checked', false);
                }
                $(obj).attr("class", 'active');
                $("body,html").animate({ scrollTop: 0 }, 400);
            }
        });
    }
}

// Function processing when click on button Add
function AddButtonDriver() {
    AddButton('carForm');
    $('#carId').attr('readonly', true);
    $("#driverId").attr("disabled", true);
}

// Function processing when click on button Edit
function EditButtonDriver() {
    if ($("#carId").val() != "") {
        EditButton('carForm');
        $('#carId').attr('readonly', true);
    }
    else {
        alert("Please choose an item!");
    }
}

// Function get car
function getCar() {
    var lostId = $("#LostId").val();
    var desShort = $.trim($("#txtDesShort").val());
    var cars = { Id: lostId, desShort: desShort, DesLong: "DesLong", noiNhat: "noinhat", LienHe: "lien he" };
    //public decimal Id { get; set; }
    //public Nullable<int> MemberId { get; set; }
    //public string DesShort { get; set; }
    //public string DesLong { get; set; }
    //public string NoiNhat { get; set; }
    //public string LienHe { get; set; }
    //public Nullable<System.DateTime> DateUpdate { get; set; }
    //public Nullable<int> islost { get; set; }

    return cars;
}