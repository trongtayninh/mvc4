﻿$(document).ready(function () {
    var key = false;
    $(window).scroll(function () {

        if ($(window).scrollTop() > 240) {
            if (key == false) {
                key = true;
                
                $(".ganttview-vtheader-item-group")
                .css("background", "#FFFFFF none")
                .css("border-bottom", "1px solid #CCCCCC")
                .css("position", "fixed")
                .css("top", "0")
                .css("width", "306px");

                
                $("div.ganttview-hzheader")
                    .css("background", "#FFFFFF none")
                    .css("border-bottom", "1px solid #CCCCCC")
                    .css("position", "fixed")
                    .css("top", "0")                    
                    .css("width", "720px")
                    .css("z-index", "9999");           
            }
        } else {
            if (key == true) {
                key = false;

                $(".ganttview-vtheader-item-group")
                .css("background", "none")
                .css("border-bottom", "none")
                .css("position", "relative")
                .css("top", "0")
                .css("width", "306px");

                $("div.ganttview-hzheader")
                    .css("background", "none")
                    .css("border-bottom", "none")
                    .css("position", "inherit")
                    .css("top", "0")                    
                    .css("width", "720px")
                    .css("z-index", "9999");
            }
        }
    });
});