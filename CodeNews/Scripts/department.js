﻿$(document).ready(function () {
    
    // Set read only for form departmentForm
    $('#departmentForm').readOnlyForm(true);
    ChangeButtonStatus(false);

    // Set background target when mouse hover
    $("#tableId tbody tr").live("mouseenter", function () {
        $(this).css("background-color", "#AED5E1");
    });

    // Remove background target when mouse leave
    $("#tableId tbody tr").live("mouseleave", function () {
        $(this).removeAttr('style');
    });

    // Ajax load department list
    $.ajax({
        type: 'POST',
        url: root + '/Department/DepartmentList',
        dataType: 'html',
        success: function (data) {
            $('#listContent').html(data);
            $("#tableId tbody tr").attr("class", 'odd');
        }
    });

    // Function processing when click on Delete button
    $("#btnDelete").click(function () {
        if ($("#departmentId").val() == "") {
            alert("Please choose an item!");
        }
        else {
            var action = confirm("Are you sure to delete data?");
            if (action) {
                $.ajax(
                {
                    url: root + '/Department/Delete',
                    type: "POST",
                    dataType: "html",
                    data: (getDepartment()),
                    success: function (data) {
                        $('#listContent').html(data);
                        $("#tableId tbody tr").attr("class", 'odd');
                        if (message != "") {
                            alert(message);
                            DeleteButton('departmentForm');
                        }
                    }
                });
            }
        }
    });

    // Function processing when click on Save button
    $("#btnSave").click(function () {
        if ($("#departmentForm").validationEngine('validate')) {
            $.ajax(
            {
                url: root + '/Department/Save',
                type: "POST",
                dataType: "html",
                data: (getDepartment()),
                success: function (data) {
                    $('#listContent').html(data);
                    $("#tableId tbody tr").attr("class", 'odd');
                    $.ajax(
                    {
                        url: "/Department/GetNewParentsDepartment",
                        type: "POST",
                        dataType: "json",
                        success: function (json) {
                            var oldValue = $("#section").val();
                            $("#section").empty();
                            $("#section").append("<option value>--Group--</option>");
                            $(json).each(function () {
                                $("#section").append("<option value=" + this.departmentId + " >" + this.departmentName + "</option>");
                            });
                            $("#section").val(oldValue);
                            SaveButton('departmentForm');
                            if (message != "") {
                                alert(message);
                            }
                        }
                    });
                }
            });
        }
    });


});

//
function newPage(page, sortField, sortDirection) {
    $.ajax({
        type: 'GET',
        url: root + '/Department/DepartmentList?page=' + page + '&sortField=' + sortField + '&sortDirection=' + sortDirection,
        dataType: 'html',
        success: function (data) {
            $('#listContent').html(data);
            $("#tableId tbody tr").attr("class", 'odd');
        }
    });
}

// Function sort page
function sort(page, sortField, sortDirection, newSortField) {
    if (sortField == newSortField) {
        if (sortDirection == "asc") {
            sortDirection = "desc";
        } else if (sortDirection == "desc") {
            sortField = "departmentId";
        } else {
            sortDirection = "asc";
        }
        
    } else {
        sortField = newSortField;
        sortDirection = "asc";
    }

    $.ajax({
        type: 'GET',
        url: root + '/Department/DepartmentList?page=' + page + '&sortField=' + sortField + '&sortDirection=' + sortDirection,
        dataType: 'html',
        success: function (data) {
            $('#listContent').html(data);
            $("#tableId tbody tr").attr("class", 'odd');
        }
    });
}

// Function select department
function SelectDepartment(obj, departmentId) {
    if ($("#mode").val() == '') {
        $.ajax({
            type: 'POST',
            url: root + '/Department/GetDepartmentById',
            data: { id: departmentId },
            dataType: 'json',
            success: function (json) {
                
                $('.active').attr("class", '');
                $("#tableId tbody tr[role='tr_parents']").attr("class", 'odd');
                $("#departmentId").val(json.departmentId);
                $("#departmentName").val(json.departmentName);
                if (json.section == null) {
                    $("#section").val("");
                }
                else {
                    $("#section").val(ReplaceNull(json.sectionId));
                }
                $("#ycCode").val(ReplaceNull(json.ycCode));
                $("#phone").val(ReplaceNull(json.phone));
                $("#note").val(ReplaceNull(json.note));
                $("#sectioncount").val(ReplaceNull(json.sectioncount));
                $(obj).parent().attr("class", 'active');
                $("body,html").animate({ scrollTop: 0 }, 400);
            }
        });
    }
}

// Function processing when click on Add button
function AddButtonDepartment() {
    AddButton('departmentForm');
    $('#departmentId').attr('readonly', true);
}

// Function processing when click on Edit button
function EditButtonDepartment() {
    if ($("#departmentId").val() != "") {
        EditButton('departmentForm');
        $('#departmentId').attr('readonly', true);
        if ($("#sectioncount").val() > 1) {
            $("#section").attr("disabled", true);
        }
    }
    else {
        alert("Please choose an item!");
    }
}

// Function get department
function getDepartment() {
    var departmentId = $("#departmentId").val();
    var departmentName = $.trim($("#departmentName").val());
    var section = $.trim($("#section").val());
    var ycCode = $.trim($("#ycCode").val());
    var phone = $.trim($("#phone").val());
    var note = $.trim($("#note").val());

    var departments = { departmentId: departmentId, departmentName: departmentName, section: section, ycCode: ycCode, phone: phone, note: note };
    return departments;
}

// Function processing show children of department
function showChildren(id, sectioncount) {
    if (sectioncount > 1) {
        var currentRow = $("tr[number='" + id + "']");
        if (currentRow.attr("status") == "close") {
            $.ajax({
                type: 'POST',
                url: root + '/Department/GetChildrenDepartment',
                data: { id: id },
                dataType: 'json',
                success: function (json) {
                    $("tr[subNumber='" + id + "']").remove();
                    $(json).each(function () {
                        currentRow.after("<tr class='children' subNumber='" + id + "' ><td class='first'></td><td onclick='SelectDepartment(this," + this.departmentId + ")' >"
                                    + this.departmentName + "</td><td onclick='SelectDepartment(this," + this.departmentId + ")'>"
                                    + this.section + "</td><td onclick='SelectDepartment(this," + this.departmentId + ")'>"
                                    + this.ycCode + "</td><td onclick='SelectDepartment(this," + this.departmentId + ")'>"
                                    + this.phone + "</td><td onclick='SelectDepartment(this," + this.departmentId + ")'>"
                                    + this.note + "</td></tr>");
                    });
                    currentRow.children().first().text("-");
                    currentRow.attr("status", "open");
                }
            });
        } else {
            $("tr[subNumber='" + id + "']").remove();
            currentRow.children().first().text("+");
            currentRow.attr("status", "close");
        }
    }
}