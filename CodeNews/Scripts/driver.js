﻿$(document).ready(function () {
    // Set read only for form driverForm
    $('#driverForm').readOnlyForm(true);
    ChangeButtonStatus(false);

    // Set background target when mouse hover
    $("#tableId tbody tr").live("mouseenter", function () {
        $(this).css("background-color", "#AED5E1");
    });

    // Remove background target when mouse leave
    $("#tableId tbody tr").live("mouseleave", function () {
        $(this).removeAttr('style');
    });

    // Ajax load driver list
    $.ajax({
        type: 'POST',
        url: root + '/Driver/DriverList',
        dataType: 'html',
        success: function (data) {
            $('#listContent').html(data);
            $("#tableId tbody tr:odd").attr("class", 'odd');
        }
    });

    // Function processing when click on Delete button
    $("#btnDelete").click(function () {
        if ($("#driverId").val() == "") {
            alert("Please choose an item!");
        }
        else {
            var action = confirm("Are you sure to delete data?");
            if (action) {
                $.ajax(
                {
                    url: root + '/Driver/Delete',
                    type: "POST",
                    dataType: "html",
                    data: (getDriver()),
                    success: function (data) {
                        $('#listContent').html(data);
                        $("#tableId tbody tr:odd").attr("class", 'odd');                        
                        if (message != "") {
                            alert(message);
                            DeleteButton('driverForm');
                        }
                    }
                });
            }
        }
    });

    // Function processing when click on Save button
    $("#btnSave").click(function () {
        if ($("#driverForm").validationEngine('validate')) {
            $.ajax(
            {
                url: root + '/Driver/Save',
                type: "POST",
                dataType: "html",
                data: (getDriver()),
                success: function (data) {
                    $('#listContent').html(data);
                    $("#tableId tbody tr:odd").attr("class", 'odd');
                    SaveButton('driverForm');
                    if (message != "") {
                        alert(message);
                    }                    
                }
            });
        }
    });
});

// Function new page
function newPage(page, sortField, sortDirection) {
    $.ajax({
        type: 'GET',
        url: root + '/Driver/DriverList?page=' + page + '&sortField=' + sortField + '&sortDirection=' + sortDirection,
        dataType: 'html',
        success: function (data) {
            $('#listContent').html(data);
            $("#tableId tbody tr:odd").attr("class", 'odd');
        }
    });
}

// Function sort list
function sort(page, sortField, sortDirection, newSortField) {
    if (sortField == newSortField) {
        if (sortDirection == "asc") {
            sortDirection = "desc";
        } else if (sortDirection == "desc") {
            sortField = "driverid";
        } else {
            sortDirection = "asc";
        }
        
    } else {
        sortField = newSortField;
        sortDirection = "asc";
    }

    $.ajax({
        type: 'GET',
        url: root + '/Driver/DriverList?page=' + page + '&sortField=' + sortField + '&sortDirection=' + sortDirection,
        dataType: 'html',
        success: function (data) {
            $('#listContent').html(data);
            $("#tableId tbody tr:odd").attr("class", 'odd');
        }
    });
}

// Function select driver
function SelectDriver(obj, driverId) {
    if ($("#mode").val() == '') {
        $.ajax({
            type: 'POST',
            url: root + '/Driver/GetDriverById',
            data: { id: driverId },
            dataType: 'json',
            success: function (json) {
                $('.active').attr("class", '');
                $("#tableId tbody tr:odd").attr("class", 'odd');
                $("#driverId").val(json.driverid);
                $("#name").val(json.name);
                if (json.gender == "Female") {
                    $("#gender").val('0');
                }
                else {
                    $("#gender").val('1');
                }
                $("#companyId").val(ReplaceNull(json.companyid));
                $("#email").val(ReplaceNull(json.email));
                $("#experience").val(ReplaceNull(json.experience));
                $("#address").val(ReplaceNull(json.address));
                $("#salary").val(ReplaceNull(json.salary)).formatCurrency({ roundToDecimalPlace: 0 });
                $("#note").val(ReplaceNull(json.note));

                $("#birthday").val(ReplaceNull(json.birthday));
                $("#driverLicense").val(ReplaceNull(json.driverlicense));
                $("#handPhone").val(ReplaceNull(json.handphone));
                $("#homePhone").val(ReplaceNull(json.homephone));
                $("#driverOldImage").val(ReplaceNull(json.image));
                if (json.image != null && json.image != "") {
                    $("#show_image").html("<img src='" + root + "/Images/Items/" + json.image + "' width='120' height='100' />");
                } else {
                    $("#show_image").html("<img src='" + root + "/Content/Images/NoPhotoIcon.jpg' width='120' height='100' />");
                }
                if (json.locked == "Locked") {
                    $("#locked").attr('checked', true);
                }
                else {
                    $("#locked").attr('checked', false);
                }
                $(obj).attr("class", 'active');
                $("body,html").animate({ scrollTop: 0 }, 400);
            }
        });
    }
}

// Function processing when click on Add button
function AddButtonDriver() {
    AddButton('driverForm');
    $('#driverId').attr('readonly', true);
}

// Function processing when click on Edit button
function EditButtonDriver() {
    if ($("#driverId").val() != "") {
        EditButton('driverForm');
        $('#driverId').attr('readonly', true);
    }
    else {
        alert("Please choose an item!");
    }
}

// Function get driver
function getDriver() {
    var driverid = $("#driverId").val();
    var name = $.trim($("#name").val());
    var gender = $("#gender").val();
    var companyid = $("#companyId").val();
    var email = $.trim($("#email").val());
    var experience = $("#experience").val();
    var address = $.trim($("#address").val());
    var salary = $("#salary").val().replace(/,/g, '');
    var note = $.trim($("#note").val());
    var birthday = $("#birthday").val();
    var driverlicense = $.trim($("#driverLicense").val());
    var handphone = $("#handPhone").val();
    var homephone = $("#homePhone").val();
    var locked = $('#locked').attr('checked');
    if (locked == "checked") {
        locked = true;
    } else {
        locked = false;
    }
    var image = $("#driverOldImage").val();

    var drivers = { driverid: driverid, name: name, gender: gender, companyid: companyid, email: email, experience: experience, address: address, salary: salary, note: note, birthday: birthday, driverlicense: driverlicense, handphone: handphone, homephone: homephone, image: image, locked: locked };
    return drivers;
}