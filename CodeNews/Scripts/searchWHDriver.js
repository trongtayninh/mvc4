﻿

$(document).ready(function () {

    // Set background target when mouse hover
    $("#tableId tbody tr").live("mouseenter", function () {
        $(this).css("background-color", "#AED5E1");
    });

    // Remove background taget when mouse leave
    $("#tableId tbody tr").live("mouseleave", function () {
        $(this).removeAttr('style');
    });

    // Ajax load search WH of driver list
    $.ajax({
        type: 'POST',
        url: root + '/SearchWHDriver/SearchWHList',
        dataType: 'html',
        success: function (data) {
            $('#listContent').html(data);
            $("#tableId tbody tr:odd").attr("class", 'odd');
        }
    });

    // Function processing when click on button search
    $("#btnSearch").click(function () {

        var searchData = "";
        var key = false;

        var departmentId = $.trim($("#departmentId").val());
        if (departmentId.length > 0) {
            searchData += "departmentId" + "@slash" + departmentId + "@slash" + "equal" + "@slash" + "string";
            key = true;
        }
        var companyId = $.trim($("#companyId").val());
        if (companyId.length > 0) {
            if (key == false) {
                searchData += "companyId" + "@slash" + companyId + "@slash" + "equal" + "@slash" + "string";
                key = true;
            } else {
                searchData += "@item" + "companyId" + "@slash" + companyId + "@slash" + "equal" + "@slash" + "string";
            }
        }
        var carId = $.trim($("#carId").val());
        if (carId.length > 0) {
            if (key == false) {
                searchData += "carId" + "@slash" + carId + "@slash" + "equal" + "@slash" + "string";
                key = true;
            } else {
                searchData += "@item" + "carId" + "@slash" + carId + "@slash" + "equal" + "@slash" + "string";
            }
        }
        var searchBy = $("input[name='searchby']:checked").val();
        var opts01, opts02;

        if (searchBy == "bymonth") {
            opts01 = $("#month").val();
            if (opts01 != "all") {
                if (key == false) {
                    searchData += "bookingDate" + "@slash" + opts01 + "@slash" + "month" + "@slash" + "monthyear";
                    key = true;
                } else {
                    searchData += "@item" + "bookingDate" + "@slash" + opts01 + "@slash" + "month" + "@slash" + "monthyear";
                }
            }
            opts02 = $("#year").val();
            if (opts02 != "all") {
                if (key == false) {
                    searchData += "bookingDate" + "@slash" + opts02 + "@slash" + "year" + "@slash" + "monthyear";
                    key = true;
                } else {
                    searchData += "@item" + "bookingDate" + "@slash" + opts02 + "@slash" + "year" + "@slash" + "monthyear";
                }
            }
        } else {
            opts01 = $("#fromdate").val();
            opts02 = $("#todate").val();
            if (key == false) {
                searchData += "bookingDate" + "@slash" + opts01 + "@slash" + "from" + "@slash" + "datetime";
                key = true;
            } else {
                searchData += "@item" + "bookingDate" + "@slash" + opts01 + "@slash" + "from" + "@slash" + "datetime";
            }

            if (key == false) {
                searchData += "bookingDate" + "@slash" + opts02 + "@slash" + "to" + "@slash" + "datetime";
                key = true;
            } else {
                searchData += "@item" + "bookingDate" + "@slash" + opts02 + "@slash" + "to" + "@slash" + "datetime";
            }
        }

        var user = $.trim($("#user").val());
        if (user.length > 0) {
            if (key == false) {
                searchData += "userName" + "@slash" + user + "@slash" + "like" + "@slash" + "string";
                key = true;
            } else {
                searchData += "@item" + "userName" + "@slash" + user + "@slash" + "like" + "@slash" + "string";
            }
        }
        var driver = $.trim($("#driver").val());
        if (driver.length > 0) {
            if (key == false) {
                searchData += "driverName" + "@slash" + driver + "@slash" + "like" + "@slash" + "string";
                key = true;
            } else {
                searchData += "@item" + "driverName" + "@slash" + driver + "@slash" + "like" + "@slash" + "string";
            }
        }
        var biztrip = $.trim($("#biztrip").val());
        if (biztrip.length > 0) {
            if (key == false) {
                searchData += "biztrip" + "@slash" + biztrip + "@slash" + "like" + "@slash" + "string";
                key = true;
            } else {
                searchData += "@item" + "biztrip" + "@slash" + biztrip + "@slash" + "like" + "@slash" + "string";
            }
        }

        $.ajax({
            type: 'GET',
            url: root + '/SearchWHDriver/SearchWHList?searchData=' + searchData,
            dataType: 'html',
            success: function (data) {
                $('#listContent').html(data);
                $("#tableId tbody tr:odd").attr("class", 'odd');
            }
        });
    });

    // Function processing when click on button clear
    $("#btnClear").click(function () {

        var d = new Date();

        $("#departmentId").val("");

        $("#companyId").val("");
        $("#carId").val("");
        $("#user").val("");
        $("#driver").val("");
        $("#biztrip").val("");
        $("#month").val(d.getMonth() + 1);
        $("#year").val(d.getFullYear());

        $("#fromdate").val(convertDate(d));
        $("#todate").val(convertDate(d));
    });
});

// Function convert date
function convertDate(date) {
    var result = date.getFullYear() + "/";
    
    if ((date.getMonth() + 1) < 10) {
        result += "0" + (date.getMonth() + 1) + "/";
    } else {
        result += (date.getMonth() + 1) + "/";
    }
    
    if (date.getDate() < 10) {
        result += "0" + date.getDate();
    } else {
        result += date.getDate();
    }
    
    return result;
}

// Function new page
function newPage(page, sortField, sortDirection, searchData) {
    $.ajax({
        type: 'GET',
        url: root + '/SearchWHDriver/SearchWHList?page=' + page + '&sortField=' + sortField + '&sortDirection=' + sortDirection + '&searchData=' + searchData,
        dataType: 'html',
        success: function (data) {
            $('#listContent').html(data);
            $("#tableId tbody tr:odd").attr("class", 'odd');
        }
    });
}

// Function sort page
function sort(page, sortField, sortDirection, newSortField) {
    if (sortField == newSortField) {
        if (sortDirection == "asc") {
            sortDirection = "desc";
        } else if (sortDirection == "desc") {
            sortField = "userId";
        } else {
            sortDirection = "asc";
        }
        
    } else {
        sortField = newSortField;
        sortDirection = "asc";
    }

    $.ajax({
        type: 'GET',
        url: root + '/SearchWHDriver/SearchWHList?page=' + page + '&sortField=' + sortField + '&sortDirection=' + sortDirection,
        dataType: 'html',
        success: function (data) {
            $('#listContent').html(data);
            $("#tableId tbody tr:odd").attr("class", 'odd');
        }
    });
}