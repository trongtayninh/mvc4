﻿
// Variable flag to checked userName and email
var isPass = true;
var isEmail = true;
// Variable form flag to check
var isOn = false;

var action = "none";
var uName = "";
var uEmail = "";

$(document).ready(function () {

    // Let's check username is exists or not    
    $("#userName").bind('focusout', function (e) {
        if (isOn) {
            if ($(this).val().length > 0) {
                $.ajax({
                    type: 'POST',
                    url: root + '/Users/CheckUserNameExists',
                    data: { userName: $(this).val() },
                    dataType: 'json',
                    success: function (json) {
                        if (json != null) {
                            if (action == "add") {
                                isPass = false;
                                // Append caption    
                                var caption = '<div class="userNameformError caption parentFormuserForm formError" style="opacity: 0.87; position: absolute; top: 136px; left: 408px; margin-top: -34px;"><div class="formErrorContent">Username has exists<br></div><div class="formErrorArrow"><div class="line10"><!-- --></div><div class="line9"><!-- --></div><div class="line8"><!-- --></div><div class="line7"><!-- --></div><div class="line6"><!-- --></div><div class="line5"><!-- --></div><div class="line4"><!-- --></div><div class="line3"><!-- --></div><div class="line2"><!-- --></div><div class="line1"><!-- --></div></div></div>';
                                $("#userForm").append(caption);
                            } else {
                                if (json.userName != uName) {
                                    isPass = false;
                                    // Append caption
                                    var caption = '<div class="userNameformError caption parentFormuserForm formError" style="opacity: 0.87; position: absolute; top: 136px; left: 408px; margin-top: -34px;"><div class="formErrorContent">Username has exists<br></div><div class="formErrorArrow"><div class="line10"><!-- --></div><div class="line9"><!-- --></div><div class="line8"><!-- --></div><div class="line7"><!-- --></div><div class="line6"><!-- --></div><div class="line5"><!-- --></div><div class="line4"><!-- --></div><div class="line3"><!-- --></div><div class="line2"><!-- --></div><div class="line1"><!-- --></div></div></div>';
                                    $("#userForm").append(caption);
                                } else {
                                    isPass = true;
                                    $(".caption").remove();
                                }
                            }
                        } else {
                            isPass = true;
                            $(".caption").remove();
                        }
                    }
                });
            } else {
                isPass = false;
                $(".caption").remove();
            }
        }
    });

   
    // Let's check email is exists or not    
    $("#email").bind('focusout', function (e) {
        if (isOn) {
            if ($(this).val().length > 0) {
                $.ajax({
                    type: 'POST',
                    url: root + '/Users/CheckEmailExists',
                    data: { email: $(this).val() },
                    dataType: 'json',
                    success: function (json) {
                        if (json != null) {
                            
                            if (action == "add") {
                                isEmail = false;
                                // Append caption    
                                var caption = '<div class="userNameformError emailcaption parentFormuserForm formError" style="opacity: 0.87; position: absolute; top: 168.8px; left: 408px; margin-top: -34px;"><div class="formErrorContent">Email has exists<br></div><div class="formErrorArrow"><div class="line10"><!-- --></div><div class="line9"><!-- --></div><div class="line8"><!-- --></div><div class="line7"><!-- --></div><div class="line6"><!-- --></div><div class="line5"><!-- --></div><div class="line4"><!-- --></div><div class="line3"><!-- --></div><div class="line2"><!-- --></div><div class="line1"><!-- --></div></div></div>';
                                $("#userForm").append(caption);
                            } else {
                                if (json.email != uEmail) {
                                    isEmail = false;
                                    // Append caption    
                                    var caption = '<div class="userNameformError emailcaption parentFormuserForm formError" style="opacity: 0.87; position: absolute; top: 168.8px; left: 408px; margin-top: -34px;"><div class="formErrorContent">Email has exists<br></div><div class="formErrorArrow"><div class="line10"><!-- --></div><div class="line9"><!-- --></div><div class="line8"><!-- --></div><div class="line7"><!-- --></div><div class="line6"><!-- --></div><div class="line5"><!-- --></div><div class="line4"><!-- --></div><div class="line3"><!-- --></div><div class="line2"><!-- --></div><div class="line1"><!-- --></div></div></div>';
                                    $("#userForm").append(caption);
                                } else {
                                    isEmail = true;
                                    $(".emailcaption").remove();
                                }
                            }
                        } else {
                            isEmail = true;
                            $(".emailcaption").remove();
                        }
                    }
                });
            } else {
                isEmail = false;
                $(".emailcaption").remove();
            }
        }
    });
});