﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ThanhNienNews.App_Start;

namespace ThanhNienNews
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("Defaultss",  // Route name
                  "{controller}/{action}/{guid_id}", // URL with parameters
                    new { controller = "Home", action = "Index", guid_id = "" },
                    
                    new { controller = new FromValuesListConstraint("-id-","bao-mat-do.html") }
                );
            // to handle personalize user url
            routes.MapRoute("user", "{url}", new { controller = "Home", action = "ViewIt", url = "" });

            
            routes.MapRoute(
 "Defauwlt", // Route name
 "Admin/{action}/{guid_id}", // URL with parameters
 defaults: new { controller = "Admin", action = "Edit" } // Parameter defaults
);
            routes.MapRoute(
                name: "Default",
                url: "tin-the-gioi/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

         

//            routes.MapRoute(
//    "Product",
//    "{productName}",
//    new { controller = "Store", action = "Index" },
//    new { productName = UrlParameter.Optional }
//);

            routes.MapRoute(
    name: "aaaawa",
    url: "{id}",
    defaults: new { controller = "Store", action = "Index" },
    constraints: new { id = @"\d{1,3}" }
);

            routes.MapRoute(
    name: "aaaaa",
    url: "{id}",
    defaults: new { controller = "Store", action = "Index" }
);
//            routes.MapRoute(
//"articlename", // Route name
//"", // URL with parameters
//new { action="Index",controller="Store", id="articleID"}, // parameter defaults 
//new[] { "ThanhNienNews.Controllers" } // controller namespaces
//);

            routes.MapRoute(
    name: "BlogPost",
    url: "{postId}",
    defaults: new
    {
        controller = "Store",
        action = "Index"
    }
);
            routes.MapRoute(
            "ChiTietSanPham", // chỉ là tên của Route dùng để phân biệt Route này vơi Route kia thôi, bạn muốn đặt sao cũng được
            "san-pham/{id}/{text}", // Link sẽ hiển thị trên address bar, tương tự như 'san-pham/12/ten-san-pham '
            new { controller = "Store", action = "ChiTiet", id = UrlParameter.Optional }
            );
        }
    }
}