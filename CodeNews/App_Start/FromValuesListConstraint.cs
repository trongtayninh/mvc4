﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace ThanhNienNews.App_Start
{
    public class FromValuesListConstraint : IRouteConstraint
    {
        private string[] _values;

        public FromValuesListConstraint(params string[] values)
        {
            this._values = values;
        }

        public bool Match(HttpContextBase httpContext, Route route, string parameterName,
        RouteValueDictionary values, RouteDirection routeDirection)
        {
            // Get the value called "parameterName" from the
            // RouteValueDictionary called "value"

            string value = values[parameterName].ToString();

            // Return true is the list of allowed values contains
            // this value.

            for (int i = 0; i < _values.Length; i++)
                if (SContains(_values[i], value, StringComparison.OrdinalIgnoreCase))
                    return false;

            return true;
        }

        public bool SContains(string source, string toCheck, StringComparison comp)
        {
            return toCheck.IndexOf(source, comp) >= 0;
        }
    }
}