﻿//******************************************************************************
// Copyright (c) 2013 Mitsui Co.,Ltd. All Right Reserved.                     
//                                                                            
//                                                                            
// system name  : CarBooking                                                  
// project name : ThanhNienNews                                            
// file name    : EmailHelper.cs                                          
// remarks      :                                                             
//                                                                            
// create       : 2013/04/05 Tien Van                                        
//******************************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using System.IO;
using log4net;

namespace ThanhNienNews.Helpers
{
    /// <summary>
    /// Helper send mail and read template mail
    /// </summary>
    public class EmailHelper
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(EmailHelper));
        /// <summary>
        /// Default email
        /// </summary>
        /// <param name="mailSubject"></param>
        /// <param name="mailBody"></param>
        /// <param name="mailTo"></param>        
        public static void SendApplyMail(string mailSubject, string mailBody, string mailTo, string pathWriteFile)
        {
            logger.Info("BEGIN - SendApplyMail");
            try
            {
                string numberRandom = DateTime.Now.ToString(Constants.FORMAT_FULL_DATE_YYYYMMDD)+ "-" + Guid.NewGuid();
                string pathFile = pathWriteFile;
                // Send email dll outlook
                //Helpers.OutlookMail oMail = new Helpers.OutlookMail();
                //oMail.addToOutBox(mailTo, mailSubject, mailBody);
                // The using statement automatically closes the stream and calls  
                // IDisposable.Dispose on the stream object. 
                pathFile +=  numberRandom + ".txt";
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(pathFile))
                {
                    file.WriteLine(mailTo);
                    file.WriteLine(mailSubject);
                    file.Write(mailBody);                    
                }
                           
                
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["EmailServer"]);
                
                mailMessage.To.Add(mailTo);
                mailMessage.Subject = mailSubject;
                mailMessage.Body = mailBody;

                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);
                smtpClient.EnableSsl = true;
                System.Net.NetworkCredential myCreds = new System.Net.NetworkCredential("nguyenminh.itvn11@gmail.com", "hoangmjnh", "");
                smtpClient.Credentials = myCreds;
                try
                {
                    smtpClient.Send(mailMessage);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                
                logger.Info("END - SendApplyMail : " + numberRandom);
            }
            catch (Exception ex)
            {
                logger.Error("ERROR - SendApplyMail: ", ex);                
            }
        }

        /// <summary>
        /// Method reading file
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string FileReading(string filePath)
        {
            logger.Info("BEGIN - FileReading");
            string contents = string.Empty;
            try
            {
                using (var reader = new StreamReader(filePath))
                {
                    contents = reader.ReadToEnd();
                }
                logger.Info("END - FileReading");
            }
            catch (Exception ex)
            {
                contents = string.Empty;
                logger.Error("ERROR - FileReading: ", ex);                
            }
            return contents;
        }
    }
}