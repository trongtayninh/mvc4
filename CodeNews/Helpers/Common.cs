﻿//******************************************************************************
// Copyright (c) 2013 Mitsui Co.,Ltd. All Right Reserved.                     
//                                                                            
//                                                                            
// system name  : CarBooking                                                  
// project name : ThanhNienNews                                            
// file name    : Common.cs                                          
// remarks      :                                                             
//                                                                            
// create       : 2013/04/02 Nguyen Thanh                                        
//******************************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using jQueryFileUploadMVC3.Models;
using System.Security.Cryptography;
using System.Text;
using System.Data.SqlClient;
using System.Collections;
using System.Data;

namespace ThanhNienNews.Helpers
{
    /// <summary>
    /// Common class contains common functions
    /// </summary>
    public static class Common
    {
        /// <summary>
        /// Function to get a property from a string
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public static string GetReflectedPropertyValue(this object subject, string field)
        {
            object reflectedValue = subject.GetType().GetProperty(field).GetValue(subject, null);
            return reflectedValue != null ? reflectedValue.ToString() : string.Empty;
        }

        /// <summary>
        /// Function to upload image
        /// </summary>
        /// <param name="Request"></param>
        /// <param name="Server"></param>
        /// <param name="Session"></param>
        /// <returns></returns>
        public static string UploadImages(HttpRequestBase Request, HttpServerUtilityBase Server, HttpSessionStateBase Session)
        {
            var r = new List<ViewDataUploadFilesResult>();
            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                string newFileName = DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + hpf.FileName.Split(new string[] { "\\" }, 
                    StringSplitOptions.None).Last();
                if (hpf.ContentLength == 0)
                    continue;
                string savedFileName = Path.Combine(Server.MapPath("~/Images/Temp"), Path.GetFileName(newFileName));
                hpf.SaveAs(savedFileName);

                r.Add(new ViewDataUploadFilesResult()
                {
                    Name = newFileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType
                });
                Session["CarBookingImage"] = newFileName;
            }
            
            return "{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}";
        }

        /// <summary>
        /// Function to get uploaded image
        /// </summary>
        /// <param name="Server"></param>
        /// <param name="Session"></param>
        /// <returns></returns>
        public static string GetImage(HttpServerUtilityBase Server, HttpSessionStateBase Session)
        {
            string newFileName = string.Empty;
            if (Session["CarBookingImage"] != null)
            {
                string fileName = Session["CarBookingImage"].ToString();
                var filePath = new FileInfo(Server.MapPath("~/Images/Temp/") + fileName);
                filePath.MoveTo(Server.MapPath("~/Images/Items/") + fileName);
                newFileName = fileName;
            }
            Session["CarBookingImage"] = null;

            string[] filePaths = Directory.GetFiles(Server.MapPath("~/Images/Temp/"));
            foreach (string file in filePaths)
            {
                if (Common.compareDate(file.Split('\\').Last()))
                {
                    var filePath = new FileInfo(file);
                    filePath.Delete();
                }
            }

            return newFileName;
        }

        /// <summary>
        /// Function to check files are old
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static bool compareDate(string filename)
        {
            try
            {
                return DateTime.Now.Subtract(DateTime.ParseExact(filename.Substring(0, 12), "yyyyMMddHHmm", System.Globalization.CultureInfo.InvariantCulture)).TotalMinutes > 5;
            }
            catch
            {
                return true;
            }
        }

        /// <summary>
        /// Function to hash code to MD5
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string MD5Hash(string value)
        {
            return Convert.ToBase64String(new MD5CryptoServiceProvider().ComputeHash(new UTF8Encoding().GetBytes(value)));
        }

        /// <summary>
        /// Method to generation random password width length
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string GetUniqueKey(int length)
        {
            string guidResult = string.Empty;
            while (guidResult.Length < length)
            {
                // Get the GUID.
                guidResult += Guid.NewGuid().ToString().GetHashCode().ToString("x");
            }
            // Make sure length is valid.
            if (length <= 0 || length > guidResult.Length)
                throw new ArgumentException("Length must be between 1 and " + guidResult.Length);

            // Return the first length bytes.
            return guidResult.Substring(0, length);
        }

        /// <summary>
        /// Method to generation random password width length
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string GetGuidId()
        {
            //string guidResult = string.Empty;
            //while (guidResult.Length < length)
            //{
            //    // Get the GUID.
            //    guidResult += Guid.NewGuid().ToString().GetHashCode().ToString("x");
            //}
            //// Make sure length is valid.
            //if (length <= 0 || length > guidResult.Length)
            //    throw new ArgumentException("Length must be between 1 and " + guidResult.Length);

            // Return the first length bytes.
            return Guid.NewGuid().ToString();
        }

        public static string getPara(string key)
        {
            if (HttpContext.Current.Request[key] != null)
            {
                return HttpContext.Current.Request[key].ToString();
            }
            return "";
        }
        public static SqlParameter[] GetParametersText(Hashtable haspara)
        {
            SqlParameter[] paramList = new SqlParameter[haspara.Count];
            int i = 0;
            SqlParameterCollection parameters = new SqlCommand().Parameters;
            foreach (DictionaryEntry item in haspara)
            {

                parameters.Add("@" + item.Key, SqlDbType.NVarChar).Value = item.Value.ToString().Trim();

                i++;
                // parameters.Clear();
            }

            parameters.CopyTo(paramList, 0);
            parameters.Clear();
            return paramList;
        }
        /// <summary>
        /// Method to replace char at index with new char
        /// </summary>
        /// <param name="input"></param>
        /// <param name="index"></param>
        /// <param name="newChar"></param>
        /// <returns></returns>
        public static String ReplaceAt(String input, int index, char newChar)
        {
            if (input != null)
            {
                char[] chars = input.ToCharArray();
                chars[index] = newChar;
                return new String(chars);    
            }
            return null;
        }
    }
}