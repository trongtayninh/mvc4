﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThanhNienNews.Helpers
{
    public class Constants
    {
        //pagination
        public const int PageRange = 10;
        public const int PageMenuCount = 10;

        ///////////////////////////////message////////////////////////////////////////

        #region /***    Message    ***/
        /// <summary>
        /// Insert successful!
        /// </summary>
        public const string MSG001 = "Insert successful!";

        /// <summary>
        /// Insert fail!
        /// </summary>
        public const string MSG002 = "Insert fail!";

        /// <summary>
        /// Update successful!
        /// </summary>
        public const string MSG003 = "Update successful!";

        /// <summary>
        /// Update fail!
        /// </summary>
        public const string MSG004 = "Update fail!";

        /// <summary>
        /// Delete successful!
        /// </summary>
        public const string MSG005 = "Delete successful!";

        /// <summary>
        /// Delete fail!
        /// </summary>
        public const string MSG006 = "Delete fail!";

        /// <summary>
        /// Unsuccessful! It contains elements.
        /// </summary>
        public const string MSG007 = "Unsuccessful! It contains elements.";

        /// <summary>
        /// Unsuccessful! It contains users.
        /// </summary>
        public const string MSG008 = "Unsuccessful! It contains users.";

        /// <summary>
        /// User has exists, please chose other!
        /// </summary>
        public const string MSG009 = "User has exists, please chose other!";

        /// <summary>
        /// User has exists, please chose other!
        /// </summary>
        public const string MSG010 = "Please enter correctly your old password";

        /// <summary>
        /// User and password message
        /// </summary>
        public const string MSG011 = "User name or password was invalid!";

        /// <summary>
        /// No data message
        /// </summary>
        public const string MSG012 = "There is no data in system.";
        #endregion

        #region /***    Controller    ***/
        /// <summary>
        /// Controller CarBookingUsing
        /// </summary>
        public const string CTRL_CAR_BOOKING_USING = "CarBookingUsing";

        /// <summary>
        /// Controller Users
        /// </summary>
        public const string CTRL_USERS = "Users";

        /// <summary>
        /// Controller Booking plan
        /// </summary>
        public const string CTRL_BOOKING_PLAN = "BookingPlan";

        #endregion

        #region /***    Action    ***/
        /// <summary>
        /// Action get approved list
        /// </summary>
        public const string ACTS_GET_APPROVED_LIST = "GetApprovedList";

        /// <summary>
        /// Action view daily
        /// </summary>
        public const string ACTS_VIEW_DAILY = "ViewDaily";
        /// <summary>
        /// Action index
        /// </summary>
        public const string ACTS_INDEX = "Index";

        /// <summary>
        /// Action department list
        /// </summary>
        public const string ACTS_DEPARTMENT_LIST = "DepartmentList";

        /// <summary>
        /// Action User List
        /// </summary>
        public const string ACTS_USER_LIST = "UserList";

        /// <summary>
        /// Action Request Password Success
        /// </summary>
        public const string ACTS_REQUEST_PASSWORD_SUCCESS = "RequestPasswordSuccess";

        /// <summary>
        /// Action Request Password Fail
        /// </summary>
        public const string ACTS_REQUEST_PASSWORD_FAIL = "RequestPasswordFail";

        /// <summary>
        /// Action change password
        /// </summary>
        public const string ACTS_CHANGE_PASSWORD = "ChangePassword";

        /// <summary>
        /// Action Driver List
        /// </summary>
        public const string ACTS_DRIVER_LIST = "DriverList";

        /// <summary>
        /// Action Company List
        /// </summary>
        public const string ACTS_COMPANY_LIST = "CompanyList";

        /// <summary>
        /// Action Car List
        /// </summary>
        public const string ACTS_CAR_LIST = "CarList";

        /// <summary>
        /// Action login
        /// </summary>
        public const string ACTS_LOGIN = "LogIn";
        #endregion

        #region /***    Variable    ***/
        /// <summary>
        /// Search condition format string
        /// </summary>
        public const string SEARCH_CONDITION_FORMAT_STRING = "string";

        /// <summary>
        /// Search condition format Month Year
        /// </summary>
        public const string SEARCH_CONDITION_FORMAT_MONTH_YEAR = "monthyear";

        /// <summary>
        /// Search condition format Datetime
        /// </summary>
        public const string SEARCH_CONDITION_FORMAT_DATETIME = "datetime";

        /// <summary>
        /// Search option like
        /// </summary>
        public const string SEARCH_OPTION_LIKE = "like";

        /// <summary>
        /// Search option month
        /// </summary>
        public const string SEARCH_OPTION_MONTH = "month";

        /// <summary>
        /// Search option year
        /// </summary>
        public const string SEARCH_OPTION_YEAR = "year";

        /// <summary>
        /// Search option from
        /// </summary>
        public const string SEARCH_OPTION_FROM = "from";

        /// <summary>
        /// Search option to
        /// </summary>
        public const string SEARCH_OPTION_TO = "to";

        /// <summary>
        /// Format date
        /// </summary>
        public const string FORMAT_DATE_YYYYMMDD = "yyyy/MM/dd";
        public const string FORMAT_FULL_DATE_YYYYMMDD = "yyyyMMdd-HHmmss-ff";

        public const string IMAGES_PATH = "~/Images/Items/";

        public const string EMAIL_SYSTEM = "~/EmailSystem/";

        /// <summary>
        /// Email pattern path
        /// </summary>        
        public const string SEND_TO_ADMIN = "..\\Content\\Document\\EmailPattern\\sendToAdmin.txt";

        /// <summary>
        /// Email pattern path
        /// </summary>
        public const string CREATE_ACCOUNT = "..\\Content\\Document\\EmailPattern\\createAccount.txt";

        /// <summary>
        /// Email pattern path
        /// </summary>
        public const string FORGOT_PASSWORD = "..\\Content\\Document\\EmailPattern\\forgotPassword.txt";

        /// <summary>
        /// Email pattern path
        /// </summary>
        public const string CONFIRM_PASSWORD = "..\\Content\\Document\\EmailPattern\\confirmPassword.txt";

        /// <summary>
        /// Email pattern path
        /// </summary>
        public const string APPROVED_BOOKING = "..\\Content\\Document\\EmailPattern\\approvedBooking.txt";

        /// <summary>
        /// Email pattern path
        /// </summary>
        public const string CANCELED_BOOKING = "..\\Content\\Document\\EmailPattern\\canceledBooking.txt";

        /// <summary>
        /// Email pattern path
        /// </summary>
        public const string EDITED_BOOKING = "..\\Content\\Document\\EmailPattern\\editedBooking.txt";

        /// <summary>
        /// Email pattern path
        /// </summary>
        public const string DELETED_BOOKING = "..\\Content\\Document\\EmailPattern\\deletedBooking.txt";
        
        #endregion
    }
}