﻿//******************************************************************************
// Copyright (c) 2013 Mitsui Co.,Ltd. All Right Reserved.                     
//                                                                            
//                                                                            
// system name  : CarBooking                                                  
// project name : ThanhNienNews                                            
// file name    : CustomRoleProvider.cs                                          
// remarks      :                                                             
//                                                                            
// create       : 2013/04/05 Tien Van                                        
//******************************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using ThanhNienNews.Models;

namespace ThanhNienNews
{
    /// <summary>
    /// Using custom role providers
    /// </summary>
    public class CustomRoleProvider : RoleProvider
    {
        #region /***    Private Declared    ***/

        #endregion

        #region /***    Property Declared    ***/
        /// <summary>
        /// Gets or sets the name of the application to store and retrieve role information for.
        /// </summary>
        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        #endregion

        #region /***    Public Method Declared    ***/
        /// <summary>
        /// Function execute when call : FormsAuthentication.SetAuthCookie(account, true or false);
        /// </summary>
        /// <param name="username">username</param>
        /// <returns>permission user</returns>
        public override string[] GetRolesForUser(string username)
        {
            // Connect to database get permission of user
            MVC4Entities db = new MVC4Entities();
            User user = db.Users.Single(x => x.userName.Equals(username) && x.deleteFlg.Equals("0"));

            // If find user
            if (user != null)
            {
                // Return permission of user
                return new String[] { user.permission };
            }
            else
            {
                // If not find user then set permission is null
                return new String[] { };
            }
             
        }

        /// <summary>
        /// Check username and role
        /// </summary>
        /// <param name="username"></param>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Adds the specified user names to the specified roles for the configured applicationName.
        /// </summary>
        /// <param name="usernames"></param>
        /// <param name="roleNames"></param>
        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Adds a new role to the data source for the configured applicationName.
        /// </summary>
        /// <param name="roleName"></param>
        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Removes a role from the data source for the configured applicationName.
        /// </summary>
        /// <param name="roleName"></param>
        /// <param name="throwOnPopulatedRole"></param>
        /// <returns></returns>
        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets an array of user names in a role where the user name contains the specified
        /// user name to match.
        /// </summary>
        /// <param name="roleName"></param>
        /// <param name="usernameToMatch"></param>
        /// <returns></returns>
        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a list of all the roles for the configured applicationName.
        /// </summary>
        /// <returns></returns>
        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a list of users in the specified role for the configured applicationName.
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Removes the specified user names from the specified roles for the configured
        /// applicationName.
        /// </summary>
        /// <param name="usernames"></param>
        /// <param name="roleNames"></param>
        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a value indicating whether the specified role name already exists in
        /// the role data source for the configured applicationName.
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region /***    Private Method Declared    ***/

        #endregion

    }
}