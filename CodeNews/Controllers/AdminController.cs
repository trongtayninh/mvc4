﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ThanhNienNews;
using ThanhNienNews.Models;
using ThanhNienNews.Helpers;
using ThanhNienNews.Controllers;
using ThanhNienNews.Controllers.Business;
using System.IO;
using System.Data.Entity.Infrastructure;
using System.Collections;
using System.Data.SqlClient;
namespace ThanhNienNews.Controllers
{
       [Authorize]
    public partial class AdminController : Controller
    {
        MyNewsBusiness myBusiness = new MyNewsBusiness();
        MVC4Entities db = new MVC4Entities();

        CategoryBusiness categoryBusiness = new CategoryBusiness();
        //
        // GET: /Admin/
        #region ---MyNew
       
        public ActionResult Index()
        {
            ViewBag.txtSearch = Common.getPara("txtSearch");
            int pz = 2;
            int pi = 1;
            if (Request["page"] != null)
            {
                pi = int.Parse(Request["page"]);
            }
            string where = "";
            int recFrom = 1;
            int recTo = 1;
            if (pi == 1)
            {
                recFrom = 1;
                recTo = pz;
            }
            else
            {
                recFrom = pi * pz - pz + 1;
                recTo = recFrom + pz - 1;
            }
            // data: "name="+name+"&group_id="+group_id,
            Hashtable hs = new Hashtable();

            string where_ = "where 1=1 ";
           
                if (Common.getPara("txtSearch") != "")
                {
                    hs["Title"] = "%" + Request["txtSearch"] + "%";
                    where_ += " and [Title] like @Title";
                }
            
            string tblname = "mynews";
            string order_by = " DateCreate desc";
            string sqlx = " WITH Ordered1 AS " +
            " (SELECT CateId, DateCreate, DateUpdate , GUID_ID, ShortDesc,Title, ROW_NUMBER() OVER (order by " + order_by + ") as RowNumber " +
            " FROM " + tblname + "  " + where_ + ")" +
            " SELECT CateId, DateCreate, DateUpdate , GUID_ID, ShortDesc,Title " +
            " FROM Ordered1 " +
            " WHERE RowNumber between " + recFrom + " and " + recTo;
            string sqlcount = " select count(GUID_ID) from " + tblname + " " + where_ + " ";
           
            DbRawSqlQuery<MyNewShow> data = db.Database.SqlQuery<MyNewShow>
                                  (sqlx, Common.GetParametersText(hs));
            var reco = db.Database.SqlQuery<int>
                                  (sqlcount, Common.GetParametersText(hs)).ToList();
           string totalRecord = reco.ToList()[0].ToString();
           int totalPage = int.Parse(totalRecord) / pz;

           //add the last page, ugly
           if (int.Parse(totalRecord) % pz != 0) totalPage++;

           PagingNum(totalPage, pi);

           //ViewBag.txtSearch = totalRecord;
            ViewBag.NewsListSearch = data;
            ViewBag.ViewPageNum = quesryxx_;
            return View();
        }
        public string quesryxx_ = "";
        public string link = "";
        public string end_page = "";
        string pagemy = "Admin/Index";
        private void PagingNum(int totalpage, int currentpage)
        {


            end_page = totalpage.ToString();
            string first = "<td align='center' valign='middle'><img src='images/btnFirst.gif' width='21' height='19' /></td>";
            //string first = "<td align='center' valign='middle'>First</td>";
            string prev = "<td align='center' valign='middle'><img src='images/btnPrev.gif' width='21' height='19' /></td>";
            //string prev = "<td align='center' valign='middle'><<Pre</td>";
            string next = "  <td align='center' valign='middle'><img src='images/btnNext.gif' width='21' height='19' /></td>";
            // string next = "  <td align='center' valign='middle'>NEXT</td>";
            string end = " <td align='center' valign='middle'><img src='images/btnEnd.gif' width='21' height='19' /></td>";
            //string end = " <td align='center' valign='middle'>END</td>";

            string inpage = "<td align='center' valign='middle' nowrap='nowrap'>";

            if (totalpage == 1) return;
            if (totalpage >= 2)
            {
                first = "<td align='center' valign='middle'><img src='images/btnFirst.gif' width='21' onclick=\"window.location.href='" + pagemy + "?page=" + (1).ToString() + "'\"  height='19' /></td>";
                end = "<td align='center' valign='middle'><img src='images/btnEnd.gif' width='21' onclick=\"window.location.href='" + pagemy + "?page=" + (totalpage).ToString() + "'\"  height='19' /></td>";
                //totalpage = totalpage - 1;
                link = "";
                for (int i = 1; i <= totalpage; i++)
                {
                    if (i != currentpage)
                    {
                        link = link + "<a href='javascript:GoPage(" + i.ToString() + ")'  class='URL' >" + i.ToString() + "</a>&nbsp;";
                    }
                    else
                        link = link + "<a href='javascript:void(0)' class='URL' >" + i.ToString() + "</a>&nbsp;";

                }
                inpage += link + "</td>";
                if (currentpage != totalpage)//next 
                {
                    // link = link + " <span class='pagenum'><a  onclick='javascript:CallLoad(\"" + (currentpage + 1).ToString() + "\") ;return false;' href='javascript:void(0)'> trang tiếp »</a></span>";
                    next = "  <td align='center' valign='middle'><img src='images/btnNext.gif' width='21' height='19' onclick=\"window.location.href='javascript:GoPage(" + (currentpage + 1).ToString() + ")'\" /></td>";
                }
                if (currentpage != 1)//back
                {
                    //link = " <span class='pagenum'><a onclick='javascript:CallLoad(\"" + (currentpage - 1).ToString() + "\") ;return false;' href='javascript:void(0)'>« trang trước </a></span>&nbsp;" + link;
                    prev = "<td align='center' valign='middle'><img src='images/btnPrev.gif' width='21' height='19'  onclick=\"window.location.href='javascript:GoPage(" + (currentpage - 1).ToString() + ")'\"  /></td>";
                }
            }
            quesryxx_ = first + prev + inpage + next + end;

        }

        public ActionResult Edit(string guid_id)
        {
            //MVC4Entities db = new MVC4Entities();
            //var queryList = from mnews in db.MyNews select mnews;
           
            ViewBag.DataOne = db.MyNews.Where(one => one.GUID_ID == guid_id).SingleOrDefault();
            Session["DataOne"] = ViewBag.DataOne;
            return View();
        }

     
        [HttpPost]
        public ActionResult SaveMyNews(MyNew myNew)
        {
            //MVC4Entities db = new MVC4Entities();
            //var queryList = from mnews in db.MyNews select mnews;
            if (string.IsNullOrEmpty(myNew.GUID_ID )==false )
            {
                MyNew myOld = (MyNew)Session["DataOne"];
                myNew.DateCreate = myOld.DateCreate;
                myNew.DateUpdate = DateTime.Now;

                string newImage = Common.GetImage(Server, Session);
                if (newImage != string.Empty)
                {
                    try
                    {
                        var filePath = new FileInfo(Server.MapPath(Constants.IMAGES_PATH) + myNew.Image1);
                        filePath.Delete();
                    }
                    catch
                    {
                    }
                    myNew.Image1 = newImage;
                }

                myBusiness.Update(myNew);
            }
            else
            {
                myNew.GUID_ID = Common.GetGuidId();
                myNew.DateCreate = DateTime.Now;
                myNew.DateUpdate = DateTime.Now;
                myNew.Image1 = Common.GetImage(Server, Session);
                myBusiness.Insert(myNew);
            }

            return Json(true);
        }
        public ActionResult AddNews()
        {
            ViewBag.DataOne = new MyNew();
            //MVC4Entities db = new MVC4Entities();
            //var queryList = from mnews in db.MyNews select mnews;
    
            
            return View();
        }
        /// <summary>
        /// Method upload file
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ContentResult UploadFiles()
        {
            return Content(Common.UploadImages(Request, Server, Session), "text/plain");
        }
        #endregion

        #region ---Category

        public ActionResult ViewCategory()
        {

            var queryList = (from cate in db.Categories select cate).OrderBy(p => p.STT);
            ViewBag.NewsListSearch = queryList.ToList();
            if (TempData["mess"]!=null)
            {
                ViewBag.mess =  TempData["mess"].ToString();
            }
            
            return View();
        }
        [HttpPost]
        public ActionResult SaveViewCate()
        {
            //MVC4Entities db = new MVC4Entities();
            //var queryList = from mnews in db.MyNews select mnews;
            
                
                for (int i = 0; i < (Request.Form).AllKeys.Length; i++)
			{
			  string myid = (Request.Form).AllKeys[i].Split("_".ToCharArray())[1];
                    string myvalue = Request.Form["CateName_"+myid];
                    int idcom =int.Parse(myid);
                    Category cateUpdate = db.Categories.Where(p => p.ID == idcom).SingleOrDefault();
                    cateUpdate.CateName = myvalue;
                   
                    cateUpdate.STT =  int.Parse( Request.Form["STT_"+myid]);
                    categoryBusiness.Update(cateUpdate);

			}

                TempData["mess"] = "Update thành công!";
                return RedirectToAction("ViewCategory");
        }

        public ActionResult EditCate(int Id)
        {
            //MVC4Entities db = new MVC4Entities();
            //var queryList = from mnews in db.MyNews select mnews;

            ViewBag.DataOne = db.Categories.Where(one => one.ID == Id).SingleOrDefault();
            Session["DataOne"] = ViewBag.DataOne;
            return View();
        }

        public ActionResult AddCate( )
        {
          var cateAdd =   new Category();
          cateAdd.STT = 0;
          cateAdd.DateCreate = DateTime.Now;
          cateAdd.DateUpdate = DateTime.Now;
          categoryBusiness.Insert(cateAdd);
           // TempData["MessageAlert"] ="Đã thêm mơ"
            return RedirectToAction("ViewCategory");
        }
        [HttpPost]
        public ActionResult SaveCate(Category myCate)
        {
            //MVC4Entities db = new MVC4Entities();
            //var queryList = from mnews in db.MyNews select mnews;
            if ((myCate.ID) == 0)
            {
                Category myOld = (Category)Session["DataOne"];
                myCate.DateCreate = myOld.DateCreate;
                myCate.DateUpdate = DateTime.Now;



                categoryBusiness.Update(myCate);
            }
            else
            {

                myCate.DateCreate = DateTime.Now;
                myCate.DateUpdate = DateTime.Now;

                categoryBusiness.Insert(myCate);
            }

            return Json(true);
        }
       
    
        #endregion
    }
}
