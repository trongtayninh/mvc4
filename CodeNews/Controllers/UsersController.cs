﻿//******************************************************************************
// Copyright (c) 2013 Mitsui Co.,Ltd. All Right Reserved.                     
//                                                                            
//                                                                            
// system name  : CarBooking                                                  
// project name : CarBookingSystem                                            
// file name    : UsersController.cs                                          
// remarks      :                                                             
//                                                                            
// create       : 2013/04/02 Manh Tuan                                        
//******************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ThanhNienNews.Models;
using ThanhNienNews.Controllers.Business;
using ThanhNienNews.Helpers;
using System.IO;
using System.Net.Mail;
using System.Configuration;
using System.Web.Routing;


namespace ThanhNienNews.Controllers
{
    /// <summary>
    /// Controller to control user
    /// </summary>    
    public class UsersController : Controller
    {
        #region /***    Private Declared    ***/

        private UserBusiness userBusiness;
        private UserViewBusiness userViewBusiness;
   

        private string message = string.Empty;
        private string root = string.Empty;

        #endregion
        #region /***    Private Method    ***/
        #endregion
        #region /***    Public Method    ***/
        // GET: /Users/
        /// <summary>
        /// Init object
        /// </summary>
        public UsersController()
        {
            userBusiness = new UserBusiness();
            userViewBusiness = new UserViewBusiness();
          
        }

      
        /// <summary>
        /// Action Index
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        public ActionResult Index(string message = "")
        {
            
           

            return View();
        }

       
        
        /// <summary>
        /// Action delete User
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(User user)
        {
            user.deleteFlg = "1";
            user.updateDateTime = DateTime.Now;
            if (userBusiness.Update(user))
            {
                message = Constants.MSG005;
            }
            else
            {
                message = Constants.MSG006;
            }
            return RedirectToAction(Constants.ACTS_USER_LIST, new { viewMessage = message });
        }

        /// <summary>
        /// Action save user info
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Save(User user)
        {
            user.deleteFlg = "0";
            if (user.userId.ToString() == "0")
            {
                var users = userBusiness.GetList();
                foreach (User u in users)
                {
                    if (u.userName == user.userName)
                    {
                        message = Constants.MSG009;
                        return View(Constants.ACTS_INDEX);
                    }
                }

                DateTime nowDate = DateTime.Now;
                user.image = Common.GetImage(Server, Session);
                                
                user.password = Common.MD5Hash(user.userName);
                user.userCreate = user.userUpdate = (Session["loginUser"] as User).userId.ToString();
                user.createDateTime = nowDate;
                user.updateDateTime = nowDate;

                if (userBusiness.Insert(user))
                {
                    //send email to user
                    root = Url.Content("~/");                    
                    if (root.Equals(""))
                    {
                        root = "/";
                    }
                    string link = Request.UrlReferrer.Scheme + "://" + Request.UrlReferrer.Authority + root + "Account/Login";
                    

                    

                    string mailSubject = ConfigurationManager.AppSettings["mailTitle"];
                    string mailBody = EmailHelper.FileReading(Server.MapPath(Constants.CREATE_ACCOUNT));
                    mailBody = mailBody.Replace("<%name%>", user.fullName)
                                       .Replace("<%username%>", user.userName)
                                       .Replace("<%password%>", user.userName)
                                       .Replace("<%link%>", link);
                    string mailTo = user.email;
                    EmailHelper.SendApplyMail(mailSubject, mailBody, mailTo, Server.MapPath(Constants.EMAIL_SYSTEM));
                    message = Constants.MSG001;
                }
                else
                {
                    message = Constants.MSG002;
                }
            }
            else
            {
                string newImage = Common.GetImage(Server, Session);
                if (newImage != string.Empty)
                {
                    try
                    {
                        var filePath = new FileInfo(Server.MapPath(Constants.IMAGES_PATH) + user.image);
                        filePath.Delete();
                    }
                    catch
                    {
                    }
                    user.image = newImage;
                }
                user.updateDateTime = DateTime.Now;
                user.userUpdate = (Session["loginUser"] as User).userId.ToString();
                if (userBusiness.Update(user))
                {
                    message = Constants.MSG003;
                }
                else
                {
                    message = Constants.MSG004;
                }
            }
            return RedirectToAction(Constants.ACTS_USER_LIST, new { viewMessage = message });
        }

    

        /// <summary>
        /// Get user by user name
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public ActionResult CheckUserNameExists(string userName)
        {
            User u = userBusiness.CheckUserNameExists(userName);
            return Json(u);
        }

        /// <summary>
        /// Get user by email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public ActionResult CheckEmailExists(string email)
        {
            User u = userBusiness.CheckEmailExists(email);
            return Json(u);
        }

        /// <summary>
        /// Method upload file
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ContentResult UploadFiles()
        {
            return Content(Common.UploadImages(Request, Server, Session), "text/plain");
        }

       
        
        /// <summary>
        /// Action to show form request password
        /// </summary>
        /// <returns></returns>
        public ActionResult ForgotPassword()
        {
            return View();            
        }

        /// <summary>
        /// Action confirm change password
        /// </summary>
        /// <returns></returns>
        public ActionResult Confirm()
        {
            string id = Request.QueryString["id"];
            string scode = Request.QueryString["scode"];
                        
            User user = userBusiness.FindUserByID(int.Parse(id));
            string oldpass = Common.MD5Hash(user.password).Replace("+", " ");

            if (oldpass == scode)
            {
                string newpass = Common.GetUniqueKey(8);
                user.password = Common.MD5Hash(newpass);
                user.updateDateTime = DateTime.Now;
                if (userBusiness.Update(user))
                {
                    // Send email to user    
                    root = Url.Content("~/");
                    if (root.Equals(""))
                    {
                        root = "/";
                    }
                    string link =  "http://" + Request.Url.Authority + root + "Account/Login";
                  
                    // Email to user with temp password
                    string mailSubject = ConfigurationManager.AppSettings["mailTitle"];                  
                    string mailBody = EmailHelper.FileReading(Server.MapPath(Constants.CONFIRM_PASSWORD));
                    mailBody = mailBody.Replace("<%name%>", user.fullName)
                                       .Replace("<%username%>", user.userName)
                                       .Replace("<%newpass%>", newpass)
                                       .Replace("<%link%>", link);
                    string mailTo = user.email;
                    EmailHelper.SendApplyMail(mailSubject, mailBody, mailTo, Server.MapPath(Constants.EMAIL_SYSTEM));
                }
            }
            return View();
        }

        /// <summary>
        /// Action processing forgot password request 
        /// </summary>
        /// <returns></returns>
        public ActionResult ForgotPasswordProcess()
        {
            string email = Request["email"];
            User u = new User();
            u = userBusiness.CheckEmailExists(email);
            if (u != null)
            {
                string link = string.Empty;
                root = Url.Content("~/");
                if (root.Equals(""))
                {
                    root = "/";
                }
                link = Request.UrlReferrer.Scheme + "://" + Request.UrlReferrer.Authority + root + "Users/Confirm?id=" + u.userId + "&scode=" + Common.MD5Hash(u.password);
                
                string mailSubject = ConfigurationManager.AppSettings["mailTitle"];
                string mailBody = EmailHelper.FileReading(Server.MapPath(Constants.FORGOT_PASSWORD));                
                mailBody = mailBody.Replace("<%name%>", u.fullName)
                                   .Replace("<%email%>", u.email)
                                   .Replace("<%link%>",link);
                string mailTo = u.email;
                EmailHelper.SendApplyMail(mailSubject, mailBody, mailTo, Server.MapPath(Constants.EMAIL_SYSTEM));
                return RedirectToAction(Constants.ACTS_REQUEST_PASSWORD_SUCCESS, Constants.CTRL_USERS);
            }
            return RedirectToAction(Constants.ACTS_REQUEST_PASSWORD_FAIL, Constants.CTRL_USERS);
        }

        /// <summary>
        /// Action to respone when request password successful
        /// </summary>
        /// <returns></returns>
        public ActionResult RequestPasswordSuccess()
        {
            return View();
        }

        /// <summary>
        /// Action to respone when request password fail
        /// </summary>
        /// <returns></returns>
        public ActionResult RequestPasswordFail()
        {
            return View();
        }
       
     
        /// <summary>
        /// Action change password
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
       // [Authorize(Roles = "Admin,User")]
        [Authorize(Roles = "Admin")]
        public ActionResult ChangePassword(string message = "")
        {
            ViewBag.Err = message;
            return View();
        }

        /// <summary>
        /// Action to processing changed password
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult ChangePasswordProcess()
        {
            string oldPass = Common.MD5Hash(Request["old_pass"]);
            string newPass = Common.MD5Hash(Request["new_pass"]);

            userBusiness = new UserBusiness();
            User user = new User();

            user = Session["loginUser"] as User;
            if (user.password != oldPass)
            {
                ViewBag.Err = Constants.MSG010;
                return View(Constants.ACTS_CHANGE_PASSWORD);
            }

            user.password = newPass;
            user.updateDateTime = DateTime.Now;
            if (userBusiness.Update(user))
            {
                return RedirectToAction(Constants.ACTS_INDEX, Constants.CTRL_CAR_BOOKING_USING);
            }
            else
            {
                return RedirectToAction(Constants.ACTS_INDEX, Constants.CTRL_CAR_BOOKING_USING);
            }
        }
        #endregion
    }
}
