﻿//******************************************************************************
// Copyright (c) 2013 Mitsui Co.,Ltd. All Right Reserved.
//
//
// system name  : CarBooking
// project name : ThanhNienNews
// file name    : AccountController.cs
// remarks      : 
//
// create       : 2013/04/02 Nguyen Thanh
//******************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ThanhNienNews.Controllers.Business;
using System.Web.Security;
using ThanhNienNews.Models;
using ThanhNienNews.Helpers;

namespace ThanhNienNews.Controllers
{
    /// <summary>
    /// Controller for page authentication system
    /// </summary>
    public class AccountController : Controller
    {        
        #region /***    Private Declared    ***/ 
        /// <summary>
        /// Define variable user business object
        /// </summary>
        private UserBusiness userBusiness;
        #endregion

        #region /***    Public Method Declared    ***/
        
        /// <summary>
        /// Action login
        /// </summary>
        /// <returns></returns>        
        public ActionResult Login()
        {
            //ViewBag.Err = message;
            return View();
        }

        /// <summary>
        /// Action login process
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LoginProcess()
        {
            // Get value username and password
            string userName = Request["userName"];
            string password = Common.MD5Hash(Request["password"]);
            
            // Get user by username and password
            userBusiness = new UserBusiness();
            var loginUser = userBusiness.CheckUserLogin(userName, password);

            // Check user login system
            if (loginUser != null)
            {
                // Set authentication system
                FormsAuthentication.SetAuthCookie(userName, false);
                Session["loginUser"] = loginUser;
                return RedirectToAction(Constants.ACTS_INDEX,"Admin");
            }
            else
            {
                ViewBag.Err = Constants.MSG011;
            }
            return View(Constants.ACTS_LOGIN);            
        }

        /// <summary>
        /// Action logout system
        /// </summary>
        /// <returns></returns>
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction(Constants.ACTS_LOGIN, "Account");
        }

        /// <summary>
        /// Action view help user manual
        /// </summary>
        /// <returns></returns>
        public ActionResult Help()
        {
            return View();
        }
            
        #endregion

        #region /***    Private Method Declared    ***/

        #endregion        
    }
}
