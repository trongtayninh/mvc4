﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ThanhNienNews.Helpers;
using ThanhNienNews.Models;
using ThanhNienNews.Controllers.Business;
using System.Data.Entity.Infrastructure;
namespace ThanhNienNews.Controllers
{
   
    public class HomeController : Controller
    {
        MyNewsBusiness myBusiness = new MyNewsBusiness();
        MVC4Entities db = new MVC4Entities();
        //
        // GET: /Home/

        public ActionResult Index()
        {
            int pz = 2;
            int pi = 1;
            if (Request["page"] != null)
            {
                pi = int.Parse(Request["page"]);
            }
            string where = "";
            int recFrom = 1;
            int recTo = 1;
            if (pi == 1)
            {
                recFrom = 1;
                recTo = pz;
            }
            else
            {
                recFrom = pi * pz - pz + 1;
                recTo = recFrom + pz - 1;
            }
            // data: "name="+name+"&group_id="+group_id,
           // Hashtable hs = new Hashtable();

            string where_ = "where 1=1 ";
            

            string tblname = "mynews";
            string order_by = " DateCreate desc";
            string sqlx = " WITH Ordered1 AS " +
            " (SELECT *, ROW_NUMBER() OVER (order by " + order_by + ") as RowNumber " +
            " FROM " + tblname + "  " + where_ + ")" +
            " SELECT * " +
            " FROM Ordered1 " +
            " WHERE RowNumber between " + recFrom + " and " + recTo;
            string sqlcount = " select count(Id) from " + tblname + " " + where_ + " ";
           // string reco = MyUtilities.GetDataTable(sqlcount, hs).Rows[0][0].ToString();
            DbRawSqlQuery<MyNew> data = db.Database.SqlQuery<MyNew>
                                  (sqlx);
            //dt = MyUtilities.GetDataTable(sqlx, hs);
           // int totalPage = int.Parse(reco) / pz;

            //add the last page, ugly
           // if (int.Parse(reco) % pz != 0) totalPage++;

          //  PagingNum(totalPage, pi);

            //DbRawSqlQuery<MyNew> data = db.Database.SqlQuery<MyNew>
            //                ("Select * from mynews =@p0", "USA");
            //DbRawSqlQuery<MyNew> data = db.Database.SqlQuery<MyNew>
            //                       ("Select * from mynews ");

            ////var queryList = from mnews in db.MyNews select mnews;
            ////ViewBag.NewsListSearch = queryList.ToList();
            //foreach (var custinfo in data)
            //{
            //    //do something custinfo
            //}
            ViewBag.NewsListSearch = data.ToList();
           
            return View();
        }
        public ActionResult ViewIt(string url)
        {
            if (url.ToString() == "bao-mat-do.html")
            {
               return new MVCTransferResult(new { controller = "YouLost", action = "BaoMatDo" });
                
            }
            string guid_id = url.Split(new string[] { "-id-" }, StringSplitOptions.None)[1];
            //url variable will have apple or microsoft . You may get data from db and return a view now.
            ViewBag.DataOne = db.MyNews.Where(one => one.GUID_ID == guid_id).SingleOrDefault();
            return View();
        }
    }
}
