﻿//******************************************************************************
// Copyright (c) 2013 Mitsui Co.,Ltd. All Right Reserved.
//
//
// system name  : CarBooking
// project name : CarBookingSystem
// file name    : CompanyBusiness.cs
// remarks      : 
//
// create       : 2013/04/02 Nguyen Thanh
//******************************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ThanhNienNews.Models;


namespace ThanhNienNews.Controllers.Business
{
    /// <summary>
    /// User Business Layer maping view in database
    /// </summary>
    public class UserViewBusiness : GenericViewBusiness<User>
    {
        #region /***    Private Declared    ***/

        #endregion

        #region /***    Public Method Declared    ***/

        /// <summary>
        /// Function to find view of a user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public User FindViewUserById(long id)
        {
            return base.CarBookingDB.Users.Where(p => p.userId == id).SingleOrDefault();
        }

        #endregion

        #region /***    Private Method Declared    ***/

        #endregion
    }
}

