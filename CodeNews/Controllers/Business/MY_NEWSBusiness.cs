﻿//******************************************************************************
// Copyright (c) 2013 Mitsui Co.,Ltd. All Right Reserved.
//
//
// system name  : CarBooking
// project name : ThanhNienNews
// file name    : CarViewBusiness.cs
// remarks      : 
//
// create       : 2013/04/02 Nguyen Thanh
//******************************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ThanhNienNews.Models;


namespace ThanhNienNews.Controllers.Business
{
    /// <summary>
    /// Car Business Layer maping view in database
    /// </summary>
    public class MyNewsBusiness : GenericBusiness<MyNew>
    {
        #region /***    Private Declared    ***/

        #endregion

        #region /***    Public Method Declared    ***/
        /// <summary>
        /// Function to find view of a car by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public MyNew FindViewCarById(string GUID_ID)
        {
            //var queryOne = from onl in base.CarBookingDB.MyNews.Where(p=>p.GUID_ID==GUID_ID) select onl;
            return base.CarBookingDB.MyNews.Where(p => p.GUID_ID == GUID_ID).SingleOrDefault();
           // return base.CarBookingDB.MyNews.Where
        }

       
        #endregion

        #region /***    Private Method Declared    ***/

        #endregion        
    }
}

