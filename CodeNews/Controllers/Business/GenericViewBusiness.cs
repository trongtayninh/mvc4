﻿//******************************************************************************
// Copyright (c) 2013 Mitsui Co.,Ltd. All Right Reserved.
//
//
// system name  : CarBooking
// project name : ThanhNienNews
// file name    : CarBusiness.cs
// remarks      : 
//
// create       : 2013/04/02 Nguyen Thanh
//******************************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ThanhNienNews.Models;
using ThanhNienNews.Helpers;
using System.Globalization;

namespace ThanhNienNews.Controllers.Business
{
    /// <summary>
    /// Base business class for using in other normal business classes
    /// </summary>
    /// <typeparam name="T">T is instance of BookmanagementEntities entity</typeparam>
    public class GenericViewBusiness<T> where T : class
    {
        #region /***    Private Declared    ***/

        #endregion

        #region /***    Public Method Declared    ***/

        /// <summary>
        /// Book management entities class
        /// </summary>
        public MVC4Entities CarBookingDB { get; set; }

        /// <summary>
        /// Create 
        /// </summary>
        public GenericViewBusiness()
        {
            this.CarBookingDB = new MVC4Entities();
        }
        

        /// <summary>
        /// Get list of entities T
        /// </summary>
        /// <returns></returns>
        public List<T> GetViewList(int page = 1, string sortField = "", string sortDirection = "", List<SearchCondition> searchConditionList = null, bool checkLimit = true)
        {
            int noItems = Constants.PageRange;
            var result = (from c in CarBookingDB.Set<T>()
                          select c).ToList();

            if (searchConditionList != null)
            {
                foreach (SearchCondition searchCondition in searchConditionList)
                {
                    switch (searchCondition.format)
                    {
                        case Constants.SEARCH_CONDITION_FORMAT_STRING:
                            if (searchCondition.searchOption == Constants.SEARCH_OPTION_LIKE)
                            {
                                result = result.Where(item => item.GetReflectedPropertyValue(
                                    searchCondition.searchField).ToLower().Contains(searchCondition.searchValue.ToLower())).ToList();
                            }
                            else
                            {
                                result = result.Where(item => item.GetReflectedPropertyValue(
                                    searchCondition.searchField).ToLower().Equals(searchCondition.searchValue.ToLower())).ToList();
                            }
                            break;
                        case Constants.SEARCH_CONDITION_FORMAT_MONTH_YEAR:
                            if (searchCondition.searchOption == Constants.SEARCH_OPTION_MONTH)
                            {
                                int month = int.Parse(searchCondition.searchValue);
                                result = result.Where(item => DateTime.ParseExact(item.GetReflectedPropertyValue(
                                    searchCondition.searchField).ToString(), 
                                    Constants.FORMAT_DATE_YYYYMMDD, 
                                    CultureInfo.InvariantCulture).Month == month).ToList();
                            }
                            else if (searchCondition.searchOption == Constants.SEARCH_OPTION_YEAR)
                            {
                                int year = int.Parse(searchCondition.searchValue);
                                result = result.Where(item => DateTime.ParseExact(item.GetReflectedPropertyValue(
                                    searchCondition.searchField).ToString(),
                                    Constants.FORMAT_DATE_YYYYMMDD, 
                                    CultureInfo.InvariantCulture).Year == year).ToList();
                            }
                            break;
                        case Constants.SEARCH_CONDITION_FORMAT_DATETIME:
                            if (searchCondition.searchOption == Constants.SEARCH_OPTION_FROM)
                            {
                                DateTime from = DateTime.ParseExact(searchCondition.searchValue, 
                                    Constants.FORMAT_DATE_YYYYMMDD, CultureInfo.InvariantCulture);
                                result = result.Where(item => Convert.ToDateTime(DateTime.ParseExact(
                                    item.GetReflectedPropertyValue(searchCondition.searchField).ToString(),
                                    Constants.FORMAT_DATE_YYYYMMDD, 
                                    CultureInfo.InvariantCulture)) >= from).ToList();
                            }
                            else if (searchCondition.searchOption == Constants.SEARCH_OPTION_TO)
                            {
                                DateTime to = DateTime.ParseExact(searchCondition.searchValue, 
                                    Constants.FORMAT_DATE_YYYYMMDD, CultureInfo.InvariantCulture);
                                result = result.Where(item => Convert.ToDateTime(DateTime.ParseExact(
                                    item.GetReflectedPropertyValue(searchCondition.searchField).ToString(),
                                    Constants.FORMAT_DATE_YYYYMMDD, 
                                    CultureInfo.InvariantCulture)) <= to).ToList();
                            }
                            break;
                    }                    
                }
            }
            if (checkLimit)
            {
                if (sortDirection == "asc")
                {
                    result = result.OrderBy(item => item.GetReflectedPropertyValue(sortField), 
                        new NaturalSortComparer<string>()).Skip(--page * noItems).Take(noItems).ToList();
                }
                else
                {
                    result = result.OrderByDescending(item => item.GetReflectedPropertyValue(sortField), 
                        new NaturalSortComparer<string>()).Skip(--page * noItems).Take(noItems).ToList();
                }
            }
            else
            {
                if (sortDirection == "asc")
                {
                    result = result.OrderBy(item => item.GetReflectedPropertyValue(sortField), 
                        new NaturalSortComparer<string>()).ToList();
                }
                else
                {
                    result = result.OrderByDescending(item => item.GetReflectedPropertyValue(sortField), 
                        new NaturalSortComparer<string>()).ToList();
                }
            }
            return result.ToList();
        }

        /// <summary>
        /// Get pagination data
        /// </summary>
        /// <param name="page"></param>
        /// <param name="sortField"></param>
        /// <param name="sortDirection"></param>
        /// <param name="searchConditionList"></param>
        /// <returns></returns>
        public PagerModel GetPagination(int page = 1, string sortField = "", string sortDirection = "desc", 
            List<SearchCondition> searchConditionList = null)
        {
            var CarBookingPager = new PagerModel();
            int noItems = Constants.PageRange;
            var result = (from c in CarBookingDB.Set<T>()
                          select c).ToList();

            if (searchConditionList != null)
            {
                foreach (SearchCondition searchCondition in searchConditionList)
                {
                    switch (searchCondition.format)
                    {
                        case Constants.SEARCH_CONDITION_FORMAT_STRING:
                            if (searchCondition.searchOption == Constants.SEARCH_OPTION_LIKE)
                            {
                                result = result.Where(item => item.GetReflectedPropertyValue(searchCondition.searchField).
                                    Contains(searchCondition.searchValue)).ToList();
                            }
                            else
                            {
                                result = result.Where(item => item.GetReflectedPropertyValue(searchCondition.searchField).
                                    Equals(searchCondition.searchValue)).ToList();
                            }
                            break;
                        case Constants.SEARCH_CONDITION_FORMAT_MONTH_YEAR:
                            if (searchCondition.searchOption == Constants.SEARCH_OPTION_MONTH)
                            {
                                int month = int.Parse(searchCondition.searchValue);
                                result = result.Where(item => DateTime.ParseExact(
                                    item.GetReflectedPropertyValue(searchCondition.searchField).ToString(),
                                    Constants.FORMAT_DATE_YYYYMMDD,
                                    CultureInfo.InvariantCulture).Month == month).ToList();
                            }
                            else if (searchCondition.searchOption == Constants.SEARCH_OPTION_YEAR)
                            {
                                int year = int.Parse(searchCondition.searchValue);
                                result = result.Where(item => DateTime.ParseExact(
                                    item.GetReflectedPropertyValue(searchCondition.searchField).ToString(),
                                    Constants.FORMAT_DATE_YYYYMMDD,
                                    CultureInfo.InvariantCulture).Year == year).ToList();
                            }
                            break;
                        case Constants.SEARCH_CONDITION_FORMAT_DATETIME:
                            if (searchCondition.searchOption == Constants.SEARCH_OPTION_FROM)
                            {
                                DateTime from = DateTime.ParseExact(searchCondition.searchValue, 
                                    Constants.FORMAT_DATE_YYYYMMDD, CultureInfo.InvariantCulture);
                                result = result.Where(item => Convert.ToDateTime(DateTime.ParseExact(
                                    item.GetReflectedPropertyValue(searchCondition.searchField).ToString(),
                                    Constants.FORMAT_DATE_YYYYMMDD,
                                    CultureInfo.InvariantCulture)) >= from).ToList();
                            }
                            else if (searchCondition.searchOption == Constants.SEARCH_OPTION_TO)
                            {
                                DateTime to = DateTime.ParseExact(searchCondition.searchValue, 
                                    Constants.FORMAT_DATE_YYYYMMDD, CultureInfo.InvariantCulture);
                                result = result.Where(item => Convert.ToDateTime(DateTime.ParseExact(
                                    item.GetReflectedPropertyValue(searchCondition.searchField).ToString(),
                                    Constants.FORMAT_DATE_YYYYMMDD,
                                    CultureInfo.InvariantCulture)) <= to).ToList();
                            }
                            break;
                    }
                }
            }
            //count all items
            int TotalItems = result.Count();
            CarBookingPager.MaxPage = TotalItems / noItems;

            if (TotalItems % noItems != 0)
            {
                CarBookingPager.MaxPage += 1;
            }
            CarBookingPager.HasNext = CarBookingPager.MaxPage > page;
            CarBookingPager.HasPrev = page > 1 && CarBookingPager.MaxPage >= page;
            CarBookingPager.CurrentPage = page;
            CarBookingPager.SortField = sortField;
            CarBookingPager.SortDirection = sortDirection;
            CarBookingPager.SearchConditionList = searchConditionList;

            int countMenu = (int)(Constants.PageMenuCount / 2) + 1;
            CarBookingPager.MinMenu = CarBookingPager.CurrentPage < countMenu ? 1 : CarBookingPager.CurrentPage - countMenu + 1;
            if (CarBookingPager.MinMenu + Constants.PageMenuCount - 1 > CarBookingPager.MaxPage)
            {
                CarBookingPager.MinMenu = CarBookingPager.MaxPage - Constants.PageMenuCount + 1;
                CarBookingPager.MaxMenu = CarBookingPager.MaxPage;
            }
            else
            {
                CarBookingPager.MaxMenu = CarBookingPager.MinMenu + Constants.PageMenuCount - 1;
            }
            if (CarBookingPager.MinMenu < 1)
            {
                CarBookingPager.MinMenu = 1;
            }

            return CarBookingPager;
        }

        #endregion

        #region /***    Private Method Declared    ***/

        #endregion
    }
}