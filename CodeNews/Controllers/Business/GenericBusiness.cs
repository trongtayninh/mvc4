﻿//******************************************************************************
// Copyright (c) 2013 Mitsui Co.,Ltd. All Right Reserved.
//
//
// system name  : CarBooking
// project name : ThanhNienNews
// file name    : CarBusiness.cs
// remarks      : 
//
// create       : 2013/04/02 Nguyen Thanh
//******************************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ThanhNienNews.Models;
using ThanhNienNews.Helpers;
using ThanhNienNews.Models.Validate;
using log4net;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace ThanhNienNews.Controllers.Business
{
    /// <summary>
    /// Base business class for using in other normal business classes
    /// </summary>
    /// <typeparam name="T">T is instance of BookmanagementEntities entity</typeparam>
    public class GenericBusiness<T> where T : class
    {
        #region /***    Private Declared    ***/
        protected static readonly ILog logger = LogManager.GetLogger(typeof(GenericBusiness<T>));
        #endregion

        #region /***    Public Method Declared    ***/

        /// <summary>
        /// Book management entities class
        /// </summary>
        public MVC4Entities CarBookingDB { get; set; }

        /// <summary>
        /// Create 
        /// </summary>
        public GenericBusiness()
        {
            this.CarBookingDB = new MVC4Entities();
        }

        public List<T> GetListNoneDeleteFlag()
        {
            return CarBookingDB.Set<T>().ToList();
        }

        /// <summary>
        /// Get list of entities T
        /// </summary>
        /// <returns></returns>
        public List<T> GetList()
        {
            //return CarBookingDB.Set<T>().ToList().Where(item => 
            //    item.GetReflectedPropertyValue("deleteFlg").Equals("0")).ToList();
            return CarBookingDB.Set<T>().ToList();
        }

        /// <summary>
        /// Get view list of entities T
        /// </summary>
        /// <returns></returns>
        public List<T> GetViewList(int page = 1, string sortField = "", string sortDirection = "")
        {
            int noItems = Constants.PageRange;
            var result = (from c in CarBookingDB.Set<T>()
                          select c).ToList().Where(item => item.GetReflectedPropertyValue("deleteFlg").Equals("0")).ToList();

            if (sortDirection == "asc")
            {
                result = result.OrderBy(item => item.GetReflectedPropertyValue(sortField), 
                    new NaturalSortComparer<string>()).Skip(--page * noItems).Take(noItems).ToList();
            }
            else
            {
                result = result.OrderByDescending(item => item.GetReflectedPropertyValue(sortField), 
                    new NaturalSortComparer<string>()).Skip(--page * noItems).Take(noItems).ToList();
            }
            return result.ToList();
        }

        /// <summary>
        /// Get pagination data
        /// </summary>
        /// <param name="page"></param>
        /// <param name="sortField"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        public PagerModel GetPagination(int page = 1, string sortField = "", string sortDirection = "desc")
        {
            var CarBookingPager = new PagerModel();
            int noItems = Constants.PageRange;

            //count all items
            int TotalItems = CarBookingDB.Set<T>().Local.Where(item => item.GetReflectedPropertyValue("deleteFlg").Equals("0")).Count();
            CarBookingPager.MaxPage = TotalItems / noItems;

            if (TotalItems % noItems != 0)
            {
                CarBookingPager.MaxPage += 1;
            }
            CarBookingPager.HasNext = CarBookingPager.MaxPage > page;
            CarBookingPager.HasPrev = page > 1 && CarBookingPager.MaxPage >= page;
            CarBookingPager.CurrentPage = page;
            CarBookingPager.SortField = sortField;
            CarBookingPager.SortDirection = sortDirection;

            int countMenu = (int)(Constants.PageMenuCount / 2) + 1;
            CarBookingPager.MinMenu = CarBookingPager.CurrentPage < countMenu ? 1 : CarBookingPager.CurrentPage - countMenu + 1;
            if (CarBookingPager.MinMenu + Constants.PageMenuCount - 1 > CarBookingPager.MaxPage)
            {
                CarBookingPager.MinMenu = CarBookingPager.MaxPage - Constants.PageMenuCount + 1;
                CarBookingPager.MaxMenu = CarBookingPager.MaxPage;
            }
            else
            {
                CarBookingPager.MaxMenu = CarBookingPager.MinMenu + Constants.PageMenuCount - 1;
            }
            if (CarBookingPager.MinMenu < 1)
            {
                CarBookingPager.MinMenu = 1;
            }

            return CarBookingPager;
        }

        /// <summary>
        /// Insert entity which is instance of T class
        /// </summary>
        /// <param name="entity">entity need to be inserted</param>
        /// <returns></returns>
        public bool Insert(T entity)
        {
            logger.Info("BEGIN - Insert");
            try
            {
                CarBookingDB.Set<T>().Add(entity);
                CarBookingDB.SaveChanges();

                logger.Info("END - Insert");
                return true;                
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                       Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                       
                    }
                }

                logger.Error("ERROR - Insert: ", ex);
                throw;
            }
        }

        /// <summary>
        /// Update entity which is instance of T class
        /// </summary>
        /// <param name="entity">entity need to be updated.</param>
        /// <returns></returns>
        public bool Update(T entity)
        {
            logger.Info("BEGIN - Update");
            try
            {
                
                CarBookingDB.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                 CarBookingDB.SaveChanges();

                 logger.Info("END - Update");
                 return true;
            }
            catch (Exception ex)
            {
                logger.Error("ERROR - Update: ", ex);
                throw;
            }
        }
        
        /// <summary>
        /// Delete entity which is instance of T class
        /// </summary>
        /// <param name="entity">entity need to be deleted</param>
        /// <returns></returns>
        public bool Delete(T entity)
        {
            logger.Info("BEGIN - Delete");
            try
            {
                //CarBookingDB.Set<T>().Remove(entity);
                CarBookingDB.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
                CarBookingDB.SaveChanges();

                logger.Info("END - Delete");
                return true;
            }
            catch (Exception ex)
            {
                logger.Error("ERROR - Delete: ", ex);
                throw;
            }
        }

        #endregion

        #region /***    Private Method Declared    ***/

        #endregion

    }
}