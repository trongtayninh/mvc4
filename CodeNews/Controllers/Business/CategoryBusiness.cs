﻿//******************************************************************************
// Copyright (c) 2013 Mitsui Co.,Ltd. All Right Reserved.
//
//
// system name  : CarBooking
// project name : ThanhNienNews
// file name    : CarViewBusiness.cs
// remarks      : 
//
// create       : 2013/04/02 Nguyen Thanh
//******************************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ThanhNienNews.Models;


namespace ThanhNienNews.Controllers.Business
{
    /// <summary>
    /// Car Business Layer maping view in database
    /// </summary>
    public class CategoryBusiness : GenericBusiness<Category>
    {
        #region /***    Private Declared    ***/

        #endregion

        #region /***    Public Method Declared    ***/
  

       
        #endregion

        #region /***    Private Method Declared    ***/

        #endregion        
    }
}

