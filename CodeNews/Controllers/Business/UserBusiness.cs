﻿//******************************************************************************
// Copyright (c) 2013 Mitsui Co.,Ltd. All Right Reserved.
//
//
// system name  : CarBooking
// project name : CarBookingSystem
// file name    : CompanyBusiness.cs
// remarks      : 
//
// create       : 2013/04/02 Nguyen Thanh
//******************************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ThanhNienNews.Models;

namespace ThanhNienNews.Controllers.Business
{
    /// <summary>
    /// User Business Layer maping table in database
    /// </summary>
    public class UserBusiness : GenericBusiness<User>
    {
        #region /***    Private Declared    ***/

        #endregion

        #region /***    Public Method Declared    ***/

        /// <summary>
        /// Function to find a user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public User FindUserByID(int id)
        {
            return base.GetList().Where(p => p.userId == id && p.deleteFlg == "0").SingleOrDefault();
        }

        /// <summary>
        /// Function to check user login successfully or not
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public User CheckUserLogin(string userName, string password)
        {
            User result = base.CarBookingDB.Users.Where(p => p.userName == userName && 
                p.password == password && p.deleteFlg == "0").SingleOrDefault();
            return result;
        }

        /// <summary>
        /// Method get user by user name
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public User CheckUserNameExists(string userName)
        {           
            return base.GetList().Where(p => p.userName == userName && p.deleteFlg == "0").SingleOrDefault();        
        }

        /// <summary>
        /// Method get user by email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public User CheckEmailExists(string email)
        {
            return base.GetList().Where(p => p.email == email).SingleOrDefault();
        }

        /// <summary>
        /// Get user list by department
        /// </summary>
        /// <param name="id"></param>
        /// <param name="option"></param>
        /// <returns></returns>
        public List<User> GetUserByDepartment(string id, int option = 0)
        {
            List<User> result;
            if (id == string.Empty)
            {
                result = base.CarBookingDB.Users.ToList();
            }
            else //if (option == 0)
            {
                result = base.CarBookingDB.Users.Where(p => p.departmentId.Equals(id)).ToList();
            }
            //else
            //{
            //   // result = base.CarBookingDB.Users.Where(p => p.section.Equals(id)).ToList();
            //}
            return result;
        }

        public List<User> getAdminList()
        {
            List<User> result = new List<User>();
            result = base.GetList().Where(p => p.permission == "Admin" && p.deleteFlg.Equals("0")).ToList();
            return result;
        }
        #endregion

        #region /***    Private Method Declared    ***/

        #endregion
    }
}

