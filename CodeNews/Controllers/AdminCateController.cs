﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ThanhNienNews;
using ThanhNienNews.Models;
using ThanhNienNews.Helpers;
using ThanhNienNews.Controllers;
using ThanhNienNews.Controllers.Business;
using System.IO;
namespace ThanhNienNews.Controllers
{
    
    public partial class AdminController : Controller
    {
        //MyNewsBusiness myBusiness = new MyNewsBusiness();
        //MVC4Entities db = new MVC4Entities();

        //CategoryBusiness categoryBusiness = new CategoryBusiness();
        ////
        // GET: /Admin/
        #region ---MyNew
       
        public ActionResult IndexCate()
        {
           
           
            return View();
        }
        /// <summary>
        /// Action view help user manual
        /// </summary>
        /// <returns></returns>
        public ActionResult ChangePassword()
        {
            return View("../Account/ChangePassword");
        }
        [HttpPost]
        public ActionResult ChangePasswordProcess()
        {
               UserBusiness userBusiness = new UserBusiness();
            string oldPass = Common.MD5Hash(Request["old_pass"]);
            string newPass = Common.MD5Hash(Request["new_pass"]);

            userBusiness = new UserBusiness();
            User user = new User();

            user = Session["loginUser"] as User;
            if (user.password != oldPass)
            {
               
                TempData["Message"] = Constants.MSG010;
                return View("../Account/ChangePassword");
            }

            user.password = newPass;
            user.updateDateTime = DateTime.Now;
            if (userBusiness.Update(user))
            {
                TempData["Message"] = "Đã thay đổi thành công";
                return View("../Account/ChangePassword");
            }
            else
            {
               
                return View("../Account/ChangePassword");
            }
        }
        #endregion

   
    }
}
