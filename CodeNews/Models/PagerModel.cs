﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ThanhNienNews.Helpers;

namespace ThanhNienNews.Models
{
    public class PagerModel
    {
        public Boolean HasPrev = false;
        public Boolean HasNext = false;
        public int CurrentPage = 1;
        public int MaxPage = Constants.PageMenuCount;
        public int MinMenu = 1;
        public int MaxMenu = Constants.PageMenuCount;
        public string SortField;
        public string SortDirection = "desc";
        public List<SearchCondition> SearchConditionList = new List<SearchCondition>();
    }

    public class SearchCondition
    {
        public string searchField { get; set; }
        public string searchValue { get; set; }
        public string searchOption { get; set; }
        public string format { get; set; }
    }
}