﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ThanhNienNews.Models.Validate
{
    public class Cars
    {
        public long carId { get; set; }

        [Required(ErrorMessage = "Please choose driver")]
        public string driverId { get; set; }

        [Required(ErrorMessage = "Please enter carPlate")]
        public string carPlate { get; set; }

        public string image { get; set; }

        [Required(ErrorMessage = "Please enter car name")]
        [StringLength(50, ErrorMessage = "Less than 50 characters!")]
        public string carName { get; set; }

        public string marker { get; set; }

        public string color { get; set; }

        public Nullable<decimal> rentalFee { get; set; }

        public string note { get; set; }

        [Required(ErrorMessage = "Please choose type")]
        public string companyId { get; set; }

        public string deleteFlg { get; set; }
        public Nullable<System.DateTime> createDateTime { get; set; }
        public Nullable<System.DateTime> updateDateTime { get; set; }
        public bool locked { get; set; }
    }
}