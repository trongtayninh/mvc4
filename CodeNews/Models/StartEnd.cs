﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThanhNienNews.Models
{
    public class StartEnd
    {
        public int start { get; set; }
        public int end { get; set; }
        public bool isPlan { get; set; }
    }
}