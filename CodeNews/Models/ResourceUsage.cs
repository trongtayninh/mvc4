﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThanhNienNews.Models
{
    public class ResourceUsage
    { 
        public string carId { get; set; }
        public string carPlate { get; set; }
        public string driverId { get; set; }
        public string driverName { get; set; }
        public string bookingDate { get; set; }
        public int time { get; set; }
        public string companyId { get; set; }
    }
}