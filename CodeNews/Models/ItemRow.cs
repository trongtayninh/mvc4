﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThanhNienNews.Models
{
    public class ItemRow
    {
        public string name { get; set; }
        public string car { get; set; }
        public float total { get; set; }
        public List<ItemIndex> days { get; set; }
    }
    public class ItemIndex
    {
        public int index { get; set; }
        public float value { get; set; }
    }
}