﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ThanhNienNews.Helpers;

namespace ThanhNienNews.Models
{
    public class JsonDay
    {
        public string id { get; set; }
        public string name { get; set; }        
        public List<JsonCar> series { get; set; }
    }

    public class JsonCar
    {
        public string id { get; set; }
        public string car_plate { get; set; }
        public string car_type { get; set; }
        public string name { get; set; }
        public string driverId { get; set; }
        public bool isBlock { get; set; }
        public List<JsonBook> plan { get; set; }
    }

    public class JsonBook
    {
        public string id { get; set; }
        public int start { get; set; }
        public int end { get; set; }
        public int color { get; set; }
        public int layer { get; set; }
        public string planId { get; set; }
        public string startPoint { get; set; }
        public string endPoint { get; set; }
        public string booker { get; set; }
        public string user { get; set; }        
    }
}